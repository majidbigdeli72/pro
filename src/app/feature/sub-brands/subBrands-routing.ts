import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from "../sub-brands/add/add.component";
import { ListComponent } from "../sub-brands/list/list.component";
import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
import { EditComponent } from "../sub-brands/edit/edit.component";
const routes: Routes = [
  {
    path: 'subBrand',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'add',
        component: AddComponent,
       },
       {
        path: ':id/edit',
        component: EditComponent,
       }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class subBrandRoutingModule { }
