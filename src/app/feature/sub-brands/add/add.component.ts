import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { NgForm, NgModel } from "@angular/forms";
import { IsubBrand, Ilabel } from "../isubBrands";
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ToastService } from '../../../core/toast/toast.service';
import { Router } from '@angular/router';
import { IKeys } from '../../brands/ibrands';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';


@Component({
  selector: 'basketik-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.less']
})
export class AddComponent implements OnInit, AfterContentInit {

  public model: IsubBrand = new IsubBrand();
  public CountDescription: number;
  public label: IsubBrand = new IsubBrand();
  public showTab: boolean = true;
  public stringFormTitle: string = "اطلاعات پایه";
  public Brands: Array<{ id: number, name: string }>;
  public urlPic: string;

  constructor(
    private dataService:SetTitleNavbarService,
    private router: Router,
    private toast: ToastService,
    private http: HttpServiceService,
    private authService: AuthService,
    private baseUrl: AppConfigService) {
      dataService.updatedDataSelection('افزودن زیربرند جدید');
  }

  ngOnInit() {
    this.label = new Ilabel().getLabel();
    this.http.post("brand/getlist", {
      Title: "",
      OrderByCol: "Title",
      OrderByDir: "ASC",
      PageIndex: 0,
      PageSize: 10000
    }).subscribe(x => {
      if (x.Code == 200) {
        let list = <Array<any>>x.Result.List;
        this.Brands = list.map(val => <IKeys>{
          id: val.Id,
          name: val.Title
        });
      }
    });
  }

  ngAfterContentInit(): void {
    this.CountDescription = 0;
    this.urlPic = this.baseUrl.configuration.PicUrl;
  }

  public getBrands(e) {
    if (this.model.BrandId) {
      return this.Brands.find(x => x.id == e).name;
    }
  }

  public toggleShowTab() {
    this.showTab = !this.showTab;
  }

  public onSubmit(e: NgForm) {

    this.toast.Wait();
    this.model.RegById = this.authService.getUserId();
    if (e.form.valid) {
      this.http.post("subbrand/Register", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/subBrand']);
          this.toast.Success();
        }
        else
          this.toast.Error();
      }, err => { }, () => this.toast.ClearWaiting());
    }
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
