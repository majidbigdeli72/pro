export class IsubBrand {
    constructor(
        Id?: number,
        Title?: string,
        EnTitle?: string,
        Description?: string,
        PicName?: string,
        RegById?: string,
        BrandId?: number) {
        this.Title = Title;
        this.Description = Description;
        this.PicName = PicName;
        this.BrandId = BrandId;
        this.RegById = RegById;
        this.EnTitle = EnTitle;
        this.Id = Id;
    }
    public Id: any;
    public EnTitle: string;
    public Title: string;
    public Description: string;
    public PicName: string;
    public RegById: string;
    public BrandId: number;
    public BrandTitle: string;
}

export class Ilabel {
    public getLabel(): IsubBrand {
        let label: IsubBrand = new IsubBrand();
        label.Id = "شماره";
        label.Title = "عنوان";
        label.EnTitle = "عنوان لاتین";
        label.Description = "توضیحات";
        label.PicName = "تصویر";
        label.BrandTitle = "برند";
        return label;
    }
}

export interface IKeys { id: number, name: string };