import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { subBrandRoutingModule } from './subBrands-routing';
import { FormsModule } from '@angular/forms';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from '../../shared/shared.module';
import { Ng2TableModule } from '../../shared/table/ng-table-module';
import { PaginationModule } from '../../shared/pagination';

@NgModule({
  imports: [
    CommonModule,
    subBrandRoutingModule,
    FormsModule,
    Ng2TableModule,
    SharedModule,
    PaginationModule.forRoot()
  ],
  declarations: [AddComponent, ListComponent, EditComponent]
})
export class SubBrandsModule { }
