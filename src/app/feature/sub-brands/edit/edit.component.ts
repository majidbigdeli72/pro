import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, NgModel } from "@angular/forms";
import { IsubBrand, Ilabel } from "../isubBrands";
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { IKeys } from '../../brands/ibrands';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit {
  id: number;
  private sub: any;
  public label: IsubBrand = new IsubBrand();
  public model: IsubBrand = new IsubBrand();
  public proId: number;
  public showTab: boolean = true;
  public stringFormTitle: string = "اطلاعات پایه";
  public Brands: Array<{ id: number, name: string }>;
  public CountDescription: number;

  constructor(
    private dataService:SetTitleNavbarService,
    private router:Router,
    private toast: ToastService, 
    private route: ActivatedRoute, 
    private authService: AuthService, 
    private http: HttpServiceService, 
    private baseUrl: AppConfigService) { }
  ngAfterContentInit(): void {
    this.CountDescription = 0;
    this.urlPic = this.baseUrl.configuration.PicUrl;
  }
  ngOnInit() {
    this.label = new Ilabel().getLabel();
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.http.post("brand/getlist", {
        Title: "",
        OrderByCol: "RegDate",
        OrderByDir: "DESC",
        PageIndex: 0,
        PageSize: 1000000
      }).subscribe(x => {
        if (x.Code == 200) {
          let list = <Array<any>>x.Result.List;
          this.Brands = list.map(val => <IKeys>{
            id: val.Id,
            name: val.Title
          });

          this.http.post('subbrand/get', {
            Id: this.id
          }).subscribe(x => {
            this.model = x.Result;
            this.dataService.updatedDataSelection(`ویرایش ${this.model.Title}`);
            this.model.Description && (this.CountDescription = this.model.Description.length);
            this.model.PicName = this.model.PicName.replace(new RegExp(this.baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');
          });
        }
      });
    });
  }

  public urlPic: string = this.baseUrl.configuration.PicUrl;
  public toggleShowTab() {
    this.showTab = !this.showTab;
  }
  public getBrands(e) {
    if (this.model.BrandId) {
      return this.Brands.find(x => x.id == e).name;
    }
  }
  public onSubmit(e: NgForm) {
    this.toast.Wait();
    this.model.RegById = this.authService.getUserId();
    if (e.form.valid) {
      this.http.post("subbrand/Update", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/subBrand']);
          this.toast.Success();
        }
        else
          this.toast.Error();
      }, err => { }, () => this.toast.ClearWaiting());
    }
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
