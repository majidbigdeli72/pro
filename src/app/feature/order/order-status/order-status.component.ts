import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { OrderDelivery, DeliveryManModel } from '../iorder';
import { ToastService } from '../../../core/toast/toast.service';

@Component({
  selector: '[basketik-order-status]',
  templateUrl: './order-status.component.html',
  styleUrls: ['./order-status.component.less']
})
export class OrderStatusComponent implements OnInit, OnChanges {

  ngOnChanges(changes: SimpleChanges): void { }

  @Input('OrderStatuses') public _orderStatuses: Array<any>;
  @Input('OrderId') private OrderId: number;
  @Input('SupplierId') private SupplierId: string;
  @Input('SupervisorId') private SupervisorId: string;

  @Output('RefreshTable') private _refreshTable: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output('ShowDeliveryToCustomerModal') private _showDeliveryToCustomerModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output('ShowDeliveryModel') private _showDeliveryModel: EventEmitter<{
    status: boolean,
    items: Array<any>,
    orderId: number
  }> = new EventEmitter<{
    status: boolean,
    items: Array<any>,
    orderId: number
  }>();

  public orderDeleveryMan: OrderDelivery = new OrderDelivery;

  constructor(private toast: ToastService,
    private http: HttpServiceService) { }

  ngOnInit() { }

  public SetDeliveryMan() {
    this.http.post("deliverymanorder/GetSupplierDeliveryManList", {
      Id: this.SupplierId
    }).subscribe(x => {
      if (x.Code === 200) {
        const DeleveryMans = x.Result.List.map(val => <DeliveryManModel>{
          Id: val.Id,
          Name: val.FirstName + ' ' + val.LastName
        });
        this._showDeliveryModel.emit({ status: true, items: DeleveryMans, orderId: this.OrderId });
      }
    });
  }

  public SetDeliveryToCustomer(e) {
    this._showDeliveryToCustomerModal.emit(true);
  }

  public selectStatus(OrderStatusId: Number) {
    this.toast.Wait();
    this.http.post("order/RegisterOrderTracking", {
      OrderId: this.OrderId,
      OrderStatusId: OrderStatusId,
      SupervisorId: this.SupervisorId
    }).subscribe(x => {
      if (x.Code === 200) {
        if (x.Result == -1) {
          this.toast.Warning({
            title:'قبلا ثبت گردیده است'
          })
        } else {
          this.toast.Success();
          document.getElementById(`${this.OrderId}`).className = `button button-tiny case${OrderStatusId}`;
          document.getElementById(`Border${this.OrderId}`).innerText = this._orderStatuses.find(x => x.Id == OrderStatusId).Title;
          //  this._refreshTable.emit(false);
        }
      }
      else
        this.toast.Error()
    }, err => { }, () => this.toast.ClearWaiting());
  };
}
