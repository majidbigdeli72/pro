export class Iorders {
    constructor(
        public Id?: any,
        public UserFullName?: any,
        public PaymentTypeTitle?: any,
        public DeliveryTypeTitle?: any,
        public RegDate?: any,
        public TotalPrice?: any,
        public DiscountAmount?: any,
        public PayableAmount?: any,
        public Phone?: any,
        public PostalCode?: any,
        public State?: any,
        public City?: any,
        public Region?: any,
        public District?: any,
        public ExactAddress?: any,
        public TransactionTypeTitle?: any,
        public SupplierId?: any,
        public SupplierTitle?: any,
        public Mobile?: any,
        public TransferCost?: any


    ) { }
}

export class OrderDelivery {
    constructor(
        public OrderId?: any,
        public DeliveryManId?: any,
        public OrderStatusId?: any,
        public SupervisorId?: any,
        public caption?: any,
        public SupplierId?: any
    ) { }
}

export class Ilabel {
    public getLabel(): Iorders {
        let label: Iorders = new Iorders();
        label.UserFullName = "نام";
        label.PaymentTypeTitle = "نحوه پرداخت";
        label.DeliveryTypeTitle = "نحوه ارسال";
        label.RegDate = "تاریخ ثبت سفارش";
        label.TotalPrice = "مبلغ کل";
        label.DiscountAmount = "مجموع تخفیف";
        label.PayableAmount = "مبلغ پرداختی";
        label.Phone = "تلفن";
        label.PostalCode = "کد پستی";
        label.State = "استان";
        label.City = "شهر";
        label.Region = "منطقه";
        label.District = "ناحیه";
        label.ExactAddress = "توضیحات آدرس";
        label.TransactionTypeTitle = "تراکنش";
        label.Mobile = "موبایل";
        label.TransferCost = "هزینه حمل و نقل"
        return label;
    }
}

export interface inputModel {
    value?: boolean;
    text: string;
    name: string;
}


export interface DeliveryManModel {
    Id: Number;
    Name: string;
}

export class MeasurementCreteria {
    public GetData(): Array<inputModel> {
        return [{
            name: "Cash",
            text: "نقدی",
            value: true
        }, {
            name: "Pos",
            text: "POS",
            value: false
        }];
    }
}