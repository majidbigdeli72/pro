import { Component, OnInit, ViewEncapsulation, HostListener, ElementRef, ViewChild } from '@angular/core';
import { Ilabel, Iorders, OrderDelivery, inputModel, DeliveryManModel, MeasurementCreteria } from '../iorder';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';
import { OrderStatusComponent } from '../order-status/order-status.component';


@Component({
  selector: 'basketik-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit {
  @ViewChild(OrderStatusComponent) private _orderStatusComponent: OrderStatusComponent;
  public deliveryManId: any;
  public Delivery: { status: boolean, items: Array<any> };
  public isPos: boolean = false;
  public MeasurementCreteriaData: Array<inputModel>;
  public MeasurementCreteriaId: inputModel;
  public SupplierId: string;
  public fieldSearch: string = "Id"
  public myClasses: string = "case0";
  public OrderStatusesItems: Array<any>;
  public label: Iorders;
  public url: string = 'order/getlist';
  public columns: Array<any>;
  public config: any = {};
  public SupervisorId: string;
  public Refresh: boolean = true;
  public showModal: boolean = false;

  public body: any = {
    OrderByDir: "DESC",
    OrderByCol: "RegDate",
    PageIndex: 0,
    PageSize: 10
  }
  constructor(
    private dataService: SetTitleNavbarService,
    private authService: AuthService,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private eRef: ElementRef,
    private toast: ToastService) {
    dataService.updatedDataSelection('لیست سفارشات');
    this.SupervisorId = this.authService.getUserId();
    this.Delivery = { status: false, items: [] };
    this.label = new Ilabel().getLabel();
    this.columns = [
      { title: this.label.Id, name: 'Id', sort: true },
      {
        title: this.label.UserFullName,
        name: 'UserFullName',
        sort: false
      },
      { title: this.label.RegDate, name: 'RegDate', sort: false },
      { title: this.label.PaymentTypeTitle, name: 'PaymentTypeTitle', sort: true },
      { title: this.label.DeliveryTypeTitle, name: 'DeliveryTypeTitle', sort: true },
      { title: this.label.SupplierTitle, name: 'SupplierTitle', sort: true },
      { title: this.label.TransactionTypeTitle, name: 'TransactionTypeTitle', sort: true },
      { title: 'عملیات', name: 'Action', sort: false },
    ];
  }

  ngOnInit() {
    this.MeasurementCreteriaData = new MeasurementCreteria().GetData();
    this.http.post("order/GetOrderStatuses", {
    }).subscribe(x => {
      this.OrderStatusesItems = x.Result;
    });
  }
  public text: String;

  public details(c) {
    this.router.navigate([c.Id, 'details'], { relativeTo: this.route });
  };

  public changeMeasurementCreteria(e) {
    if (e) {
      this.isPos = false
    } else {
      this.isPos = true
    }
  };

  public cancelDelevery() {
    this.Delivery.status = false;
    this.Delivery.items = [];
  };

  public okDelevery(orderId) {
    this.toast.Wait();
    this.http.post("deliverymanorder/Register", {
      OrderId: orderId,
      DeliveryManId: this.deliveryManId,
      SupervisorId: this.authService.getUserId()
    }).subscribe(x => {
      if (x.Code === 200) {
        if (x.Result == -1) {
          this.toast.Warning({
            title: 'قبلا ثبت گردیده است'
          });
        }
        else {
          this.toast.Success();
          this._orderStatusComponent.selectStatus(3);
        }
        this.cancelDelevery();
      } else {
        this.toast.Error();
      }
    }, err => { }, () => this.toast.ClearWaiting());
  };

  public DeliveryShowModal(e) {
    this.Delivery = e;
  }

  public modelCahnged(e) {
    this.deliveryManId = e;
  }

  public RefreshTable(e) {
    this.Refresh = !this.Refresh;
    setTimeout(() => this.Refresh = !this.Refresh, 10);
  }

  public ShowDeliveryToCustomerModal(e) {
    this.showModal = !this.showModal;
  }

  public ConfirmDeliveryToCustomer(e) {
    this.toast.Wait();//order/RegisterOrderDeliveryTracking
    this.http.post("", {
      DeliveryManId: "{{user_id}}",
      OrderId: 1368,
      DeliveryStatusId: 1,
      PosReferenceNumber: "",
      IsPayedByCash: true,
      Description: ""
    }).subscribe(x => {
      if (x.Code === 200) {
        this.CancelDeliveryToCustomer();
        this.toast.Success();
        this._orderStatusComponent.selectStatus(4);
      } else {
        this.toast.Error();
      }
    }, err => { }, () => this.toast.ClearWaiting());
  }

  public CancelDeliveryToCustomer() {
    this.showModal = false;
  }
}


