import { Directive, ElementRef, Renderer, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[basketikShowOrderStatus]'
})
export class ShowOrderStatusDirective {
  constructor(private el: ElementRef,
    private renderer: Renderer) { }
  @HostListener('click') onMouseOver() {
    const isShow = this.el.nativeElement.querySelectorAll('.dropdown-content')[0].style.display || 'none';
    this.el.nativeElement.querySelectorAll('.dropdown-content')[0].style.display = isShow === 'none' ? 'block' : 'none';
  }
}
