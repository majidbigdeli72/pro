import { Component, OnInit } from '@angular/core';
import { Ilabel, Iorders } from '../iorder';
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ActivatedRoute } from '@angular/router';
import * as momentJalaali from "moment-jalaali";
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.less']
})
export class DetailsComponent implements OnInit {

  public label: Iorders;
  id: number;
  private sub: any;
  public data: Iorders = new Iorders();
  public showTab: Array<boolean> = [false, true, true, true, true, true];
  stringFormTitle = "اطلاعات پایه"

  constructor(
    private dataService: SetTitleNavbarService,
    private route: ActivatedRoute, 
    private authService: AuthService, 
    private http: HttpServiceService, 
    private baseUrl: AppConfigService) { }

  ngOnInit() {
    this.label = new Ilabel().getLabel();
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.http.post("order/GetOrderDetails", {
        "Id": this.id
      }).subscribe(x => {
        this.data = x.Result;
        this.dataService.updatedDataSelection(`ویرایش ${this.data.UserFullName}`);
        this.data.RegDate = momentJalaali(x.Result.RegDate).format("HH:mm jYYYY/jMM/jDD");
      })
    })
  }

  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
  }

  print() {
    let popupWinindow
    let innerContents = document.getElementById("print-order").innerHTML;
    popupWinindow = window.open('', 'Print-Window');
    popupWinindow.document.open();
    popupWinindow.document.write(`<html><head><link rel="stylesheet" type="text/css" href="assets/css/font.css" /><link rel="stylesheet" type="text/css" href="assets/css/print.css" /></head><body onload="window.print()">${innerContents}</body></html>`);
    popupWinindow.document.close();
  };

}
