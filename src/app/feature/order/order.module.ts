import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderRoutingModule } from './order-routing.module';
import { ListComponent } from './list/list.component';
import { DetailsComponent } from './details/details.component';
import { SharedModule } from '../../shared/shared.module'
import { FormsModule } from "@angular/forms";
import { ShowOrderStatusDirective } from './directive/show-order-status.directive';
import { OrderStatusComponent } from './order-status/order-status.component';

@NgModule({
  imports: [
    CommonModule,
    OrderRoutingModule,
    SharedModule,
    FormsModule
  ],
  declarations: [ListComponent, DetailsComponent, ShowOrderStatusDirective, OrderStatusComponent]
})
export class OrderModule { }
