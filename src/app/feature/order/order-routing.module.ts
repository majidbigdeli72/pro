import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
import { ListComponent } from './list/list.component';
import { DetailsComponent } from './details/details.component';
const routes: Routes = [
  {
    path: 'order',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: ':id/details',
        component: DetailsComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
