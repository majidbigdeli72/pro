import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductCreateComponent } from './product-create/product-create.component';
import { ProductEditComponent } from './product-edit/product-edit.component';

const routes: Routes =  [
  {
    path: 'product',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ProductListComponent
      },
      {
        path: 'create',
        component: ProductCreateComponent
      },
      {
        path: ':hashId/edit',
        component: ProductEditComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
