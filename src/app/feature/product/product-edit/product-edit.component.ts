import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../core/user-service/auth.service';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { ToastService } from '../../../core/toast/toast.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { IKeyedCollection } from '../../../core/dictionary';
import { IProductListCreate, IProductList, ILabel } from '../imodel';
import { CategoryProductComponent } from '../category-product/category-product.component';
import { NgModel, NgForm } from '@angular/forms';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.less']
})
export class ProductEditComponent implements OnInit, AfterContentInit {
  ngAfterContentInit(): void {
    this.CountDescription = 0;
  }
  public label: IKeyedCollection<string>;
  public model: IProductListCreate;
  public showTab: Array<boolean> = [false, true, true];
  public Categories: Array<any>;
  public CategroyList: Array<{ name: string, id: number }> = [];
  public SubCategroyList: Array<{ name: string, id: number }> = [];
  public _baseUrl: string;
  public ProductCategoryTitle: string;
  public ProductSubCategoryTitle: string;
  public stringBaseInformation: string = "";
  public CountDescription: number;

  @ViewChild(CategoryProductComponent) private _category: CategoryProductComponent;

  private id: number;

  constructor(
    private dataService: SetTitleNavbarService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private http: HttpServiceService,
    private toast: ToastService,
    private router: Router,
    private baseUrl: AppConfigService) {
    this.model = new IProductList();
    this.model.FieldGroups = [];
    this.label = new ILabel().getLabel();
    this._baseUrl = baseUrl.configuration.PicUrl;
    this.Categories = [];
  }

  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
    if (index == 2) this.extendModel();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['hashId'];
      this.http.post('product/get', {
        Id: this.id
      }).subscribe(x => {
        if (x.Code == 200) {
          this.model = x.Result;
          this.dataService.updatedDataSelection(`ویرایش ${this.model.Title}`);
          this.model.Description && (this.CountDescription = this.model.Description.length);
          this.model.PicName = this.model.PicName && this.model.PicName.replace(new RegExp(this.baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');
          this.http.post("category/getlist", {
            "Title": "",
            "PageIndex": 0,
            "PageSize": 12000
          }).subscribe((d) => {
            this.Categories = d.Result.List;
            this.CategroyList = d.Result.List.map(c => <{ name: string, id: number }>({
              id: c.Id,
              name: c.Title
            }));
            this.GetProductCategoryTitle(this.model.ProductCategoryId);
            this.changeProductCategory(this.model.ProductCategoryId, true);
          })
          this.http.post('productfield/GetProductFieldListWithGroup', {
            Id: this.model['Id']
          }).subscribe(z => {
            this.model.FieldGroups = z.Result.List;
          })
        }
        else
          this.toast.Error();
      })
    });
  }


  public onSubmit(e: NgForm) {
    this.toast.Wait();

    this.model.FieldGroups = this._category.GetModel();
    let status: boolean = true;
    this.model.FieldGroups.forEach(x => {
      if (!x.Fields || x.Fields.length <= 0) {
        status = false;
        this.toast.Error({
          title: 'خطا در ذخیره اطلاعات',
          msg: `دسته بندی ${x.Title} بدون فیلداست`
        });
      }
      x.Fields.forEach(z => {
        if (!z.Title || !z.FieldTypeId) {
          if (!z.Title)
            this.toast.Error({
              title: 'خطا در ذخیره اطلاعات',
              msg: `دسته بندی ${x.Title} داری فیلد بدون نام است`
            })
          if (!z.FieldTypeId)
            this.toast.Error({
              title: 'خطا در ذخیره اطلاعات',
              msg: `دسته بندی ${x.Title} داری فیلد بدون نوع است`
            })
          status = false;
        }
      })
    })
    if (e.valid && status) {
      let _model = e.form.value;
      _model['FieldGroups'] = this.model.FieldGroups;
      _model['Id'] = this.model.Id;
      this.http.post("product/Update", _model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/product']);
          this.toast.Success();
        }
        else {
          this.toast.Error({
            msg: x.Message,
            title: 'اشکال در عملیات'
          })
        }
      }, err => this.toast.Error(),
        () => this.toast.ClearWaiting());
    }
  }

  private changeSubCategoryId() {
    this.ProductSubCategoryTitle = '...';
    this.model.ProductSubCategoryId = undefined;
    this.changeProductCategory(this.model.ProductCategoryId);
  }

  public changeProductCategory(e, hasValue?: boolean) {
    this.GetProductCategoryTitle(e);
    this.SubCategroyList = this.Categories.find(x => x['Id'] == e).SubCategories.map(c => <{ name: string, id: number }>({
      id: c.Id,
      name: c.Title
    }));
    if (hasValue) this.changeProductSubCategoryTitle(this.model.ProductSubCategoryId);
    else this.ProductSubCategoryTitle = '...';
  }

  public GetProductCategoryTitle(e) {
    this.ProductCategoryTitle = this.CategroyList.find(x => x.id == e).name;
  }

  public changeProductSubCategoryTitle(e) {
    this.ProductSubCategoryTitle = this.SubCategroyList.find(x => x.id == e).name;
  }

  extendModel(): void {
    this.model.FieldGroups = this._category.GetModel();
  }
  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
