import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { IProductList, IProductListCreate, ILabel } from '../imodel';
import { IKeyedCollection } from '../../../core/dictionary';
import { CategoryProductComponent } from '../category-product/category-product.component';
import { NgForm, NgModel } from '@angular/forms';
import { ToastService } from '../../../core/toast/toast.service';
import { Router } from '@angular/router';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.less']
})
export class ProductCreateComponent implements OnInit, AfterContentInit {

  public label: IKeyedCollection<string>;
  public model: IProductListCreate;
  public showTab: Array<boolean> = [false, true, true];
  public CategroyList: Array<{ name: string, id: number }> = [];
  public SubCategroyList: Array<{ name: string, id: number }> = [];
  public baseUrl: string;
  public ProductCategoryTitle: string;
  public ProductSubCategoryTitle: string;
  public stringBaseInformation: string = "افزودن اطلاعات";
  public Categories: Array<any>;
  public CountDescription: number;

  @ViewChild(CategoryProductComponent) private _category: CategoryProductComponent;

  constructor(
    private dataService: SetTitleNavbarService,
    private http: HttpServiceService,
    private router: Router,
    private toast: ToastService,
    private _baseUrl: AppConfigService) {
    dataService.updatedDataSelection('افزودن محصول جدید');
    this.model = new IProductList();
    this.model.FieldGroups = [];
    this.label = new ILabel().getLabel();
    this.baseUrl = _baseUrl.configuration.PicUrl;
  }
  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
    if (index == 2) this.extendModel();
  }
  ngAfterContentInit(): void {
    this.CountDescription = 0;
  }
  ngOnInit() {
    this.http.post("category/getlist", {
      "Title": "",
      "PageIndex": 0,
      "PageSize": 12000
    }).subscribe((x) => {
      this.Categories = x.Result.List;
      this.CategroyList = x.Result.List.map(c => <{ name: string, id: number }>({
        id: c.Id,
        name: c.Title
      }));
    })
  }

  public onSubmit(e: NgForm) {
    this.toast.Wait();
    this.model.FieldGroups = this._category.GetModel();
    let status: boolean = true;
    this.model.FieldGroups.forEach(x => {
      if (x.Fields && x.Fields.length <= 0) {
        status = false;
        this.toast.Error({
          title: 'خطا در ذخیره اطلاعات',
          msg: `دسته بندی ${x.Title} بدون فیلداست`
        });
      }
      x.Fields.forEach(z => {
        if (!z.Title || !z.FieldTypeId) {
          if (!z.Title)
            this.toast.Error({
              title: 'خطا در ذخیره اطلاعات',
              msg: `دسته بندی ${x.Title} داری فیلد بدون نام است`
            })
          if (!z.FieldTypeId)
            this.toast.Error({
              title: 'خطا در ذخیره اطلاعات',
              msg: `دسته بندی ${x.Title} داری فیلد بدون نوع است`
            })
          status = false;
        }
      })
    })
    if (e.valid && status) {
      let _model = e.form.value;
      //product/Register
      _model['FieldGroups'] = this.model.FieldGroups;
      this.http.post("", _model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/product']);
          this.toast.Success();
        }
        else {
          this.toast.Error({
            msg: x.Message,
            title: 'اشکال در عملیات'
          })
        }
      }, err => this.toast.Error(),
        () => this.toast.ClearWaiting());
    }
  }

  public GetSubCategory(e) {
    this.GetProductCategoryTitle(e);
    this.SubCategroyList = this.Categories.find(x => x['Id'] == e).SubCategories.map(c => <{ name: string, id: number }>({
      id: c.Id,
      name: c.Title
    }));
  }

  public changeProductSubCategoryTitle(e) {
    this.GetProductSupCategoryTitle(e);
  }
  public GetProductCategoryTitle(e) {
    this.ProductSubCategoryTitle = '...';
    this.model.ProductSubCategoryId = undefined;
    this.ProductCategoryTitle = this.CategroyList.find(x => x.id == e).name;
  }

  public GetProductSupCategoryTitle(e) {
    this.ProductSubCategoryTitle = this.SubCategroyList.find(x => x.id == e).name;
  }
  extendModel(): void {
    this.model.FieldGroups = this._category.GetModel();
  }



  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
