import { IKeyedCollection, KeyedCollection } from "../../core/dictionary";

export class IProductList {
  constructor(
    public Id?: any,
    public Title?: string,
    public EnTitle?: string,
    public Description?: string,
    public RegDate?: string,
    public StrDate?: string,
    public ProductCategoryTitle?: string,
    public ProductSubCategoryTitle?: String,
    public PicUrl?: string) { }
}

export class ILabel {
  public getLabel(): IKeyedCollection<string> {
    let label = new KeyedCollection<string>();
    label.Add('Description', 'توضیحات');
    label.Add('Id', 'شماره');
    label.Add('ProductCategoryTitle', 'دسته بندی');
    label.Add('ProductSubCategoryTitle', 'زیر دسته بندی');
    label.Add('RegDate', 'تاریخ ثبت');
    label.Add('Title', 'عنوان');
    label.Add('EnTitle', 'عنوان لاتین');
    label.Add('PicUrl', 'تصویر');
    return label;
  }
}

export class IProductListCreate {
  constructor(
    public Id?: any,
    public Title?: string,
    public EnTitle?: string,
    public Description?: string,
    public PicName?: string,
    public ProductCategoryId?: string,
    public ProductSubCategoryId?: String,
    public FieldGroups?: Array<FieldGroups>) { }
}
export class FieldGroups {
  constructor(
    public Title?: string,
    public Description?: string,
    public Fields?: Array<Fields>) { }
}

export class Fields {
  constructor(
    public FieldTypeId?: string,
    public Title?: string,
    public FieldValues?: Array<FieldValues>) { }
}

export class FieldValues {
  constructor(
    public Id?:number,
    public Value?: string) { }
}
