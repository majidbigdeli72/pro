import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { IProductList, ILabel } from '../imodel';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { IKeyedCollection } from '../../../core/dictionary';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.less']
})
export class ProductListComponent implements OnInit {
  public label: IKeyedCollection<string>;
  public url: string = 'product/getlist';
  public columns: Array<any>;
  public config: any = {};
  public refresh: boolean = true;
  public body: any = {
    Title: "",
    Description: "",
    PicName: "",
    RegById: "",
    ProductCategoryId: 0,
    ProductSubCategoryId: 0,
    ProductSuperCategoryId: 0,
    PageIndex: 0,
    PageSize: 10,
    OrderByCol: "Id",
    OrderByDir: "DESC"
  }


  public data: Array<IProductList>;
  constructor(
    private dataService:SetTitleNavbarService,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService) {
      dataService.updatedDataSelection('لیست محصولات ');
    this.label = new ILabel().getLabel();
    this.data = new Array<IProductList>();
    this.columns = [
      { title: this.label.Item('Id'), name: 'Id', sort: true },
      {
        title: this.label.Item('Title'),
        name: 'Title',
        sort: true
      },
      { title: this.label.Item('Description'), name: 'Description', sort: false },
      { title: this.label.Item('RegDate'), name: 'StrDate', sort: false },
      { title: this.label.Item('ProductCategoryTitle'), name: 'ProductCategoryTitle', sort: false },
      { title: this.label.Item('ProductSubCategoryTitle'), name: 'ProductCategoryTitle', sort: false },
      { title: 'عملیات', name: 'Action', sort: false },
    ];
  }

  ngOnInit() { }

  public onRemoveClicked(id) {
    this.toast.Wait();
    this.http.post('product/remove', {
      Id: id
    }).subscribe(status => {
      this.toast.Success({
        msg: 'عملیات با موفقیت انجام شد',
        title: 'حذف'
      });
      this.refresh = false;
      setTimeout(() => this.refresh = true, 1);
    }, errr => { }, () => this.toast.ClearWaiting());
  }

  public onEditClicked(e) {
    this.router.navigate([e.id, 'edit'], { relativeTo: this.route });
  }
}
