import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'basketik-modal-product',
  templateUrl: './modal-product.component.html',
  styleUrls: ['./modal-product.component.less']
})
export class ModalProductComponent implements OnInit {
  constructor() { }
  @Output('ok') ok: EventEmitter<any> = new EventEmitter<any>();
  @Output('close') close: EventEmitter<any> = new EventEmitter<any>();
  public nameCategoryModel: string;
  ngOnInit() {
  }
}
