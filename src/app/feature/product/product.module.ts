import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProductRoutingModule } from './product-routing.module';
import { ProductCreateComponent } from './product-create/product-create.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductListComponent } from './product-list/product-list.component';
import { CategoryProductComponent } from './category-product/category-product.component';
import { ModalProductComponent } from './modal-product/modal-product.component';
import { GroupProductComponent } from './category-product/group-product/group-product.component';
import { ElementGroupProductComponent } from './category-product/group-product/element-group-product/element-group-product.component';
import { ValueGroupComponent } from './category-product/group-product/element-group-product/value-group/value-group.component';
import { SharedModule } from '../../shared/shared.module';
import { Ng2TableModule } from '../../shared/table/ng-table-module';
import { PaginationModule } from '../../shared/pagination';

@NgModule({
  imports: [
    CommonModule,
    ProductRoutingModule,
    FormsModule,
    SharedModule,
    Ng2TableModule,
    PaginationModule.forRoot()
  ],
  declarations: [
    ProductCreateComponent,
    ProductEditComponent,
    ProductListComponent,
    CategoryProductComponent,
    ModalProductComponent,
    GroupProductComponent,
    ElementGroupProductComponent,
    ValueGroupComponent
  ],
  entryComponents: [
    GroupProductComponent,
    ElementGroupProductComponent,
    ValueGroupComponent
  ],
  providers: []
})
export class ProductModule { }
