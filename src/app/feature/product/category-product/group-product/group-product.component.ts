import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentRef, ComponentFactory, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ElementGroupProductComponent } from './element-group-product/element-group-product.component';
import { FieldGroups, Fields } from '../../imodel';
import { ToastService } from '../../../../core/toast/toast.service';
import { HttpServiceService } from '../../../../core/http-service/http-service.service';

@Component({
  selector: 'basketik-group-product',
  templateUrl: './group-product.component.html',
  styleUrls: ['./group-product.component.less']
})
export class GroupProductComponent implements OnInit {

  public id: number;
  public Title: string;
  public contenteditableTitle: boolean = false;
  public Data: FieldGroups;

  private componentsGroup: Array<any> = [];

  @ViewChild('containerElement', { read: ViewContainerRef }) private containerElement: ViewContainerRef;
  @Output() removeGroup: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private toast: ToastService,
    private http: HttpServiceService,
    private resolver: ComponentFactoryResolver) {
    this.Data = new FieldGroups();
    this.Data.Fields = [];
  }

  ngOnInit() {
    this.Data.Title ? this.Data.Fields.forEach(x => this.fieldCreate(x, true)) : this.fieldCreate();
  }

  public groupRemove() {
    this.removeGroup.emit(this.id);
  }

  public GetValueGroup(): FieldGroups {
    this.Data.Fields = [];
    this.componentsGroup.forEach(x => {
      if (x.instance.model)
        this.Data.Fields.push(x.instance.model);
    });

    this.Data.Title = this.Title;
    return this.Data;
  }

  public fieldCreate(e?, isEdit = false) {
    const componentFactory: ComponentFactory<ElementGroupProductComponent> = this.resolver.resolveComponentFactory(ElementGroupProductComponent);
    const componentRef = this.containerElement.createComponent(componentFactory);
    componentRef.instance.id = Math.random();
    componentRef.instance.model = isEdit ? e : new Fields();
    componentRef.instance.isEdit = isEdit;
    componentRef.instance._closePopUpValue.subscribe(x => this.remove(componentRef.instance.id));
    this.componentsGroup.push(componentRef);
  }

  public remove(e) {
    const component = this.componentsGroup.find((component) => component.instance.id == e);
    const componentIndex = this.componentsGroup.indexOf(component);
    const _id = component.instance.model.Id;
    if (component.instance.isEdit && _id) {
      this.toast.Wait();
      this.http.post("productfield/RemoveField", {
        Id: _id
      }).subscribe(x => {
        if (x.Code == 200) {
          this.toast.Success({
            title: 'حذف مقدار',
            msg: 'حذف با موفقیت انجام گردید'
          });
          if (componentIndex !== -1) {
            this.containerElement.remove(this.containerElement.indexOf(component));
            this.componentsGroup.splice(componentIndex, 1);
          }
        }
        else {
          this.toast.Error({
            title: 'حذف مقدار',
            msg: 'مقدار دارای داده وابسته میباشد'
          });
        }
      }, err => {
        this.toast.Error({
          title: 'حذف مقدار',
          msg: 'عملیات در سمت سرور با مشکل مواجه گردید'
        });
      }, () => this.toast.ClearWaiting());
    }
    else {
      if (componentIndex !== -1) {
        this.containerElement.remove(this.containerElement.indexOf(component));
        this.componentsGroup.splice(componentIndex, 1);
      }
    }
  }
}
