import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementGroupProductComponent } from './element-group-product.component';

describe('ElementGroupProductComponent', () => {
  let component: ElementGroupProductComponent;
  let fixture: ComponentFixture<ElementGroupProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementGroupProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementGroupProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
