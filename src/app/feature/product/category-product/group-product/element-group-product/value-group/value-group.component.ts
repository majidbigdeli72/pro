import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FieldValues } from '../../../../imodel';

@Component({
  selector: 'tr[basketik-value-group]',
  templateUrl: './value-group.component.html',
  styleUrls: ['./value-group.component.less']
})
export class ValueGroupComponent implements OnInit {
  constructor() {  }
  @Output() public removeValue: EventEmitter<number> = new EventEmitter<number>();
  @Input() public id: number;
  public model: FieldValues;
  ngOnInit() {  }

  public valueFiledRemove(e) {
    this.removeValue.emit(this.id);
  }
}
