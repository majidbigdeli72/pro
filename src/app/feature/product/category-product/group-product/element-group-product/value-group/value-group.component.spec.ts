import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueGroupComponent } from './value-group.component';

describe('ValueGroupComponent', () => {
  let component: ValueGroupComponent;
  let fixture: ComponentFixture<ValueGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValueGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
