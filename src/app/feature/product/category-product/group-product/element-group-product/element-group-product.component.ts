import { Component, OnInit, ViewChild, TemplateRef, ViewContainerRef, AfterViewInit, ComponentFactory, ComponentFactoryResolver, ComponentRef, OnDestroy, Type, Output, EventEmitter, Input } from '@angular/core';
import { ValueGroupComponent } from './value-group/value-group.component';
import { Fields, FieldValues } from '../../../imodel';
import { HttpServiceService } from '../../../../../core/http-service/http-service.service';
import { ToastService } from '../../../../../core/toast/toast.service';

@Component({
  selector: 'tr[basketik-element-group-product]',
  templateUrl: './element-group-product.component.html',
  styleUrls: ['./element-group-product.component.less']
})
export class ElementGroupProductComponent implements OnInit {
  public TypesEnum: Array<{ id: number, name: string }> = [];
  public disableDetail: boolean = true;
  public showContainer: boolean = false;
  public model: Fields;
  public title: string;
  @Input() id: number;
  @Input() isEdit: boolean = false;
  @Output('close') _closePopUpValue: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('containerValue', { read: ViewContainerRef }) private _container: ViewContainerRef;

  private componentsElement = [];

  constructor(
    private toast: ToastService,
    private http: HttpServiceService,
    private resolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.TypesEnum = [
      {
        id: 1,
        name: 'تک انتخابی'
      },
      {
        id: 2,
        name: 'چند انتخابی'
      },
      {
        id: 3,
        name: 'متنی'
      },
      {
        id: 4,
        name: 'دو حالتی'
      },
    ];
    this.isEdit && this.change(this.model.FieldTypeId);
  }

  public removeElementField() {
    this._closePopUpValue.emit(this.model);
  }

  public change(e) {
    if (e == 3 || e == 4)
      this.disableDetail = true;
    else {
      if (this.isEdit) {
        this.model.FieldValues.forEach(x => this.valueCreate(x));
      }
      this.disableDetail = false;
    }
    this.model.FieldTypeId = e;
  }

  //#region Value Of DropDown Detail
  public addFieldDetails() {
    this.showContainer = true;
  }

  public valueCreate(e?) {
    const componentFactory: ComponentFactory<ValueGroupComponent> = this.resolver.resolveComponentFactory(ValueGroupComponent);
    const componentRef = this._container.createComponent(componentFactory);
    componentRef.instance.id = Math.random();
    componentRef.instance.model = this.isEdit ? e ? e : new FieldValues() : new FieldValues();
    componentRef.instance.removeValue.subscribe(x => this.removeComponent(componentRef.instance.id));
    this.componentsElement.push(componentRef);
  }

  public valueSave() {
    this.model.FieldValues = [];
    this.componentsElement.forEach(x => {
      if (x.instance.model)
        this.model.FieldValues.push({
          Id: x.instance.model.Id,
          Value: x.instance.model.Value
        })
    })
    this.showContainer = false;
  }

  removeComponent(e) {
    const component = this.componentsElement.find((component) => component.instance.id == e);
    const componentIndex = this.componentsElement.indexOf(component);
    const _id = component.instance.model.Id;
    if (this.isEdit && _id) {
      this.toast.Wait();
      this.http.post("productfield/RemoveFieldValue", {
        Id: _id
      }).subscribe(x => {
        if (x.Code == 200) {
          this.toast.Success({
            title: 'حذف مقدار',
            msg: 'حذف با موفقیت انجام گردید'
          });
          if (componentIndex !== -1) {
            this._container.remove(this._container.indexOf(component));
            this.componentsElement.splice(componentIndex, 1);
          }
        }
        else {
          this.toast.Error({
            title: 'حذف مقدار',
            msg: 'مقدار دارای داده وابسته میباشد'
          });
        }
      }, err => {
        this.toast.Error({
          title: 'حذف مقدار',
          msg: 'عملیات در سمت سرور با مشکل مواجه گردید'
        });
      }, () => this.toast.ClearWaiting());
    }
    else {
      if (componentIndex !== -1) {
        this._container.remove(this._container.indexOf(component));
        this.componentsElement.splice(componentIndex, 1);
      }
    }
  }
  //#endregion
}