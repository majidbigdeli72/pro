import { Component, OnInit, ViewChild, ViewRef, ViewContainerRef, ComponentRef, ComponentFactoryResolver, OnDestroy, ComponentFactory, AfterContentInit, Input } from '@angular/core';
import { GroupProductComponent } from './group-product/group-product.component';
import { FieldGroups } from '../imodel';
import { NgModel } from '@angular/forms';
import { ToastService } from '../../../core/toast/toast.service';
import { HttpServiceService } from '../../../core/http-service/http-service.service';

@Component({
  selector: 'basketik-category-product',
  templateUrl: './category-product.component.html',
  styleUrls: ['./category-product.component.less']
})
export class CategoryProductComponent implements OnInit {

  public componentsCategory: Array<any> = [];
  public showModal: boolean = false;
  public model: Array<FieldGroups> = [];

  constructor(
    private toast: ToastService,
    private http: HttpServiceService,
    private resolver: ComponentFactoryResolver) { }

  @Input('Model') public modelFields: Array<FieldGroups>;
  @ViewChild('container', { read: ViewContainerRef }) private container: ViewContainerRef;
  ngOnInit() {
    if (this.modelFields) {
      this.modelFields.forEach(x => this.ConfirmNameCat(x));
    }
  }

  public ConfirmNameCat(event) {
    let _title: string;
    let obj = undefined;
    if (event.Title) {
      obj = event;
      _title = event.Title;
    }
    else
      _title = event;

    this.showModal = false;
    const factory: ComponentFactory<GroupProductComponent> = this.resolver.resolveComponentFactory(GroupProductComponent);
    const componentRef = this.container.createComponent(factory);
    componentRef.instance.id = Math.random();
    componentRef.instance.removeGroup.subscribe(x => this.remove(componentRef.instance.id));
    componentRef.instance.Title = _title;
    componentRef.instance.Data = event.Title ? obj : new FieldGroups();
    this.componentsCategory.push(componentRef);
  }

  public GetModel(): Array<FieldGroups> {
    this.model = [];
    this.componentsCategory.forEach(x => {
      this.model.push(x.instance.GetValueGroup());
    });
    return this.model;
  }

  public remove(e) {
    const component = this.componentsCategory.find((component) => component.instance.id == e);
    const componentIndex = this.componentsCategory.indexOf(component);
    console.log(component);
    const _id = component.instance.Data.Id;
    if (this.modelFields && _id) {
      this.toast.Wait();
      this.http.post("productfield/RemoveFieldGroup", {
        Id: _id
      }).subscribe(x => {
        if (x.Code == 200) {
          this.toast.Success({
            title: 'حذف گروه',
            msg: 'حذف با موفقیت انجام گردید'
          });
          if (componentIndex !== -1) {
            this.container.remove(this.container.indexOf(component));
            this.componentsCategory.splice(componentIndex, 1);
          }
        }
        else {
          this.toast.Error({
            title: 'حذف گروه',
            msg: 'مقدار دارای داده وابسته میباشد'
          });
        }
      }, err => {
        this.toast.Error({
          title: 'حذف گروه',
          msg: 'عملیات در سمت سرور با مشکل مواجه گردید'
        });
      }, () => this.toast.ClearWaiting());
    }
    else {
      if (componentIndex !== -1) {
        this.container.remove(this.container.indexOf(component));
        this.componentsCategory.splice(componentIndex, 1);
      }
    }
  }
}
