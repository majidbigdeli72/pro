export class Idatas {
    constructor(
        public Id?: any,
        public EnTitle?: string,
        public Title?: string,
        public Description?: string,
        public IconName?: string,
        public BackgroundName?: string,
        public RegById?: string,
        public ProductSuperCategoryId?: number,
        public Priority?: any
    ) { }
}

export class Idata {
    public getLabel(): Idatas {
        let data: Idatas = new Idatas();
        data.Id = "شماره";
        data.Title = "عنوان";
        data.EnTitle = "عنوان لاتین";
        data.Description = "توضیحات";
        data.IconName = "آیکن";
        data.BackgroundName = "‍پس زمینه";
        data.Priority = "‍ترتیب نمایش";
        return data;
    }
}