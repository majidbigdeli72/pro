import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, NgModel } from "@angular/forms";
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { Idatas, Idata } from "../icategory";
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ToastService } from '../../../core/toast/toast.service';

import { Router } from '@angular/router';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';


@Component({
  selector: 'basketik-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.less']
})
export class AddComponent implements OnInit {

  public label: Idatas = new Idatas();
  public data: Idatas;
  public showTab: boolean = true;
  public urlPic: string;
  public stringFormTitle: string = "اطلاعات پایه";

  public CountDescription: number;
  constructor(
    private dataService:SetTitleNavbarService,
    private http: HttpServiceService,
    private toast: ToastService,
    private router: Router,
    private authService: AuthService,
    private baseUrl: AppConfigService) {
      dataService.updatedDataSelection('افزودن دسته بندی');
     }

  ngOnInit() {
    this.CountDescription = 0;
    this.label = new Idata().getLabel();

    this.data = new Idatas();
    this.data.RegById = this.authService.getUserId();
    this.urlPic = this.baseUrl.configuration.PicUrl;
  }

  public toggleShowTab() {
    this.showTab = !this.showTab;
  }

  public register(form: NgForm) {
    this.toast.Wait();
    if (form.valid) {
      this.http.post("category/Register", this.data).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/category']);
          this.toast.Success();
        }
        else { }
      }, err => { },
        () => this.toast.ClearWaiting());
      }
  }
  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
