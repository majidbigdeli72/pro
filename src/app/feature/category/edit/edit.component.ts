import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { Idata, Idatas } from "../icategory";
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { NgForm, NgModel } from "@angular/forms";
import { AuthService } from '../../../core/user-service/auth.service';
import { ToastService } from '../../../core/toast/toast.service';

import { retry } from 'rxjs/operator/retry';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit {
  public label: Idatas = new Idatas();
  public data: Idatas = new Idatas();
  public showTab: boolean = true;
  public stringFormTitle: string = "اطلاعات پایه";
  public urlPic: string;
  public CountDescription: number;

  private id: number;


  constructor(
    private dataService:SetTitleNavbarService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private http: HttpServiceService,
    private toast: ToastService,
    private router: Router,
    private baseUrl: AppConfigService) { }

  ngOnInit() {
    this.data.RegById = this.authService.getUserId();
    this.CountDescription = 0;
    this.urlPic = this.baseUrl.configuration.PicUrl;
    this.label = new Idata().getLabel();
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.http.post('/category/get', {
        Id: this.id
      }).subscribe(x => {
        this.data = x.Result;
        this.dataService.updatedDataSelection(`ویرایش ${this.data.Title}`);
        this.data.IconName = this.data.IconName.replace(new RegExp(this.baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');        
        this.data.BackgroundName = this.data.BackgroundName.replace(new RegExp(this.baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');        
        this.CountDescription = this.data.Description ? this.data.Description.length : 0;
      })
    });
  }


  public toggleShowTab() {
    this.showTab = !this.showTab;
  }



  public update(form: NgForm) {
    this.toast.Wait();
    this.data['PicName']='';
    if (form.valid) {
      this.http.post("category/update", this.data).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/category']);
          this.toast.Success();
        }
        else { }
      }, err => { },
        () => this.toast.ClearWaiting());
    }
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}

