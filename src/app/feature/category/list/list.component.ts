import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { Idatas, Idata } from "../icategory";
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { delay } from 'q';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';


@Component({
  selector: 'basketik-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {
  public label: Idatas;
  public url: string = 'category/getlist';
  public columns: Array<any>;
  public config: any = {};
  public refresh: boolean = true;
  public body: any = {
    Title: "",
    OrderByCol: "Id",
    OrderByDir: "DESC",
    PageIndex: 0,
    PageSize: 10
  }
  constructor(
    private dataService:SetTitleNavbarService,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService) {
      dataService.updatedDataSelection('لیست دسته بندی');
    this.label = new Idata().getLabel();
    this.columns = [
      { title: this.label.Id, name: 'Id', sort: true },
      {
        title: this.label.Title,
        name: 'Title',
        sort: true
      },
      { title: this.label.Description, name: 'Description', sort: false },
      { title: this.label.IconName, name: 'IconName', sort: false, pic: true },
      { title: this.label.BackgroundName, name: 'BackgroundName', sort: false, pic: true },
      { title: this.label.Priority, name: 'Priority', sort: true },
      { title: 'عملیات', name: 'Action', sort: false },
    ];
  }
  ngOnInit() { }
  public onRemoveClicked(id) {
    this.toast.Wait();
    this.http.post('category/remove', {
      Id: id
    }).subscribe(status => {
      if (status.Code == 107) {
        this.toast.Error({
          msg: status.Message,
          title: 'پیغام'
        })
      }
      else 
      this.toast.Success({
        msg: 'عملیات با موفقیت انجام شد',
        title: 'حذف'
      });
      this.refresh = false;
      setTimeout(() => this.refresh = true, 1);
    }, errr => { }, () => {
      this.toast.ClearWaiting();
    });
  }
  public onEditClicked(e) {
    this.router.navigate([e.id, 'edit'], { relativeTo: this.route });
  }
}
