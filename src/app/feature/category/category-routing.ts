import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from "../category/add/add.component";
import { ListComponent } from "../category/list/list.component";
import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
import { EditComponent } from "../category/edit/edit.component";
const routes: Routes = [
  {
    path: 'category',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'add',
        component: AddComponent,
      },
      {
        path: ':id/edit',
        component: EditComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
