import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { NgForm, NgModel } from "@angular/forms";
import { IsubCategory, Ilabel } from "../isubCategory";
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { IKeys } from '../../brands/ibrands';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit, AfterContentInit {

  id: number;
  private sub: any;
  public label: IsubCategory = new IsubCategory();
  public model: IsubCategory = new IsubCategory();
  public proId: number;
  public showTab: boolean = true;
  public stringFormTitle: string = "اطلاعات پایه";
  public Categorys: Array<{ id: number, name: string }>;
  public CountDescription: number;
  public urlPic: string;

  constructor(
    private dataService:SetTitleNavbarService,
    private router: Router,
    private toast: ToastService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private http: HttpServiceService,
    private baseUrl: AppConfigService) { }

  ngOnInit() {
    this.label = new Ilabel().getLabel();
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.http.post("category/getlist", {
        Title: "",
        PageIndex: 0,
        PageSize: 12000
      }).subscribe(x => {
        if (x.Code == 200) {
          let list = <Array<any>>x.Result.List;
          this.Categorys = list.map(val => <IKeys>{
            id: val.Id,
            name: val.Title
          });
          this.http.post('subcategory/get', {
            Id: this.id
          }).subscribe(x => {
            this.model = x.Result;
            this.dataService.updatedDataSelection(`ویرایش ${this.model.Title}`);
            this.model.PicName = this.model.PicName.replace(new RegExp(this.baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');
            this.model.Description && (this.CountDescription = this.model.Description.length);
          });
        }
      });
    });
  }


  ngAfterContentInit(): void {
    this.CountDescription = 0;
    this.urlPic = this.baseUrl.configuration.PicUrl;
  }


  public toggleShowTab() {
    this.showTab = !this.showTab;
  }
  public getCategory(e) {
    if (this.model.ProductCategoryId) {
      return this.Categorys.find(x => x.id == e).name;
    }
  }
  public onSubmit(e: NgForm) {
    this.toast.Wait();
    this.model.RegById = this.authService.getUserId();
    if (e.form.valid) {
      this.http.post("subcategory/Update", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/subCategory']);
          this.toast.Success();
        }
        else
          this.toast.Error();
      }, err => { }, () => this.toast.ClearWaiting());
    }
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
