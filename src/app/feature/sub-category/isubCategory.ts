export class IsubCategory {
    constructor(
        Id?: number,
        Title?: string,
        EnTitle?: string,
        Description?: string,
        PicName?: string,
        RegById?: string,
        ProductCategoryId?: number) {
        this.Title = Title;
        this.Description = Description;
        this.PicName = PicName;
        this.ProductCategoryId = ProductCategoryId;
        this.RegById = RegById;
        this.EnTitle = EnTitle;
        this.Id = Id;
    }
    public Id: any;
    public EnTitle: string;
    public Title: string;
    public Description: string;
    public PicName: string;
    public RegById: string;
    public ProductCategoryId: number;
    public ProductCategoryTitle: string;
}

export class Ilabel {
    public getLabel(): IsubCategory {
        let label: IsubCategory = new IsubCategory();
        label.Id = "شماره";
        label.Title = "عنوان";
        label.PicName = "تصویر";
        label.EnTitle = "عنوان لاتین";
        label.Description = "توضیحات";
        label.ProductCategoryTitle = "دسته بندی";
        return label;
    }
}

export interface IKeys { id: number, name: string };