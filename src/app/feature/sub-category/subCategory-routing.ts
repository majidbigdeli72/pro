import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from "../sub-category/add/add.component";
import { ListComponent } from "../sub-category/list/list.component";
import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
import { EditComponent } from "../sub-category/edit/edit.component";
const routes: Routes = [
  {
    path: 'subCategory',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'add',
        component: AddComponent,
      },
      {
        path: ':id/edit',
        component: EditComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class subCategoryRoutingModule { }
