import { Component, OnInit, ViewChild, AfterViewInit, AfterContentInit } from '@angular/core';
import { NgForm, NgModel } from "@angular/forms";
import { IsubCategory, Ilabel } from "../isubCategory";
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ToastService } from '../../../core/toast/toast.service';
import { IKeys } from '../../brands/ibrands';
import { Router } from '@angular/router';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';


@Component({
  selector: 'basketik-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.less']
})
export class AddComponent implements OnInit, AfterContentInit {

  public model: IsubCategory = new IsubCategory();
  public label: IsubCategory = new IsubCategory();
  public showTab: boolean = true;
  public stringFormTitle: string = "اطلاعات پایه";
  public Categorys: Array<{ id: number, name: string }>;
  public CountDescription: number;
  public urlPic: string;

  constructor(
    private dataService:SetTitleNavbarService,
    private router:Router,
    private toast: ToastService,
    private http: HttpServiceService,
    private authService: AuthService,
    private baseUrl: AppConfigService) { 
      dataService.updatedDataSelection('افزودن زیر دسته بندی');
    }

  ngAfterContentInit(): void {
    this.CountDescription = 0;
    this.urlPic = this.baseUrl.configuration.PicUrl;
  }

  ngOnInit() {
    this.label = new Ilabel().getLabel();
    this.model.RegById = this.authService.getUserId();

    this.http.post("category/getlist", {
      Title: "",
      RegById: "",
      PageIndex: 0,
      PageSize: 1000000
    }).subscribe(x => {
      if (x.Code == 200) {
        let list = <Array<any>>x.Result.List;
        this.Categorys = list.map(val => <IKeys>{
          id: val.Id,
          name: val.Title
        });
      }
    });
  }

  public getCategory(e) {
    if (this.model.ProductCategoryId) {
      return this.Categorys.find(x => x.id == e).name;
    }
  }

  public toggleShowTab() {
    this.showTab = !this.showTab;
  }


  public onSubmit(e: NgForm) {
    this.toast.Wait();
    this.model.RegById = this.authService.getUserId();
    if (e.form.valid) {
      this.http.post("subcategory/Register", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/subCategory']);
          this.toast.Success();
        }
        else
          this.toast.Error();
      }, err => { }, () => this.toast.ClearWaiting());
    }
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
