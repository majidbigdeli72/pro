import { Component, OnInit } from '@angular/core';
import { IsubCategory, Ilabel } from "../isubCategory";
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { NgForm } from "@angular/forms";
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {


  public label: IsubCategory;
  public url: string = 'subCategory/getlist';
  public columns: Array<any>;
  public config: any = {};
  public refresh: boolean = true;
  public body: any = {
    Title: "",
    OrderByCol: "RegDate",
    OrderByDir: "DESC",
    PageIndex: 0,
    PageSize: 10
  }
  constructor(
    private dataService:SetTitleNavbarService,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService) {
      dataService.updatedDataSelection('لیست  زیر دسته بندی');
    this.label = new Ilabel().getLabel();
    this.columns = [
      { title: this.label.Id, name: 'Id', sort: true },
      {
        title: this.label.Title,
        name: 'Title',
        sort: true,
      },
      { title: this.label.Description, name: 'Description', sort: false },
      { title: this.label.PicName, name: 'PicName', sort: false, pic: true },
      { title: this.label.ProductCategoryTitle, name: 'ProductCategoryTitle', sort: "" },
      { title: 'عملیات', name: 'Action', sort: false },
    ];
  }
  ngOnInit() { }
  public onRemoveClicked(id) {
    this.toast.Wait();
    this.http.post('subCategory/remove', {
      Id: id
    }).subscribe(status => {
      if (status.Code == 107) {
        this.toast.Error({
          msg: status.Message,
          title: 'پیغام'
        })
      }
      else 
      this.toast.Success({
        msg: 'عملیات با موفقیت انجام شد',
        title: 'حذف'
      });
      this.refresh = false;
      setTimeout(() => this.refresh = true, 1);
    }, errr => {  }, () => this.toast.ClearWaiting());
  }
  public onEditClicked(e) {
    this.router.navigate([e.id, 'edit'], { relativeTo: this.route });
  }
}