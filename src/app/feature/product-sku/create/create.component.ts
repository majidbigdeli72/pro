import { Component, OnInit, ViewChild, TemplateRef, QueryList, ViewChildren, ContentChildren, AfterContentInit } from '@angular/core';
import { IproductSkuCreate, LabelProductSkuCreate, IFieldValue, ISupplierProductSkus, IDiscounts } from '../iproduct-sku';
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { inputModel, selectModel } from '../../../shared/tags/input-model';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { NgForm, NgModel } from '@angular/forms';
import { FieldValueComponent } from '../field-value/field-value.component';
import { DiscountComponent } from '../discount/discount.component';
import { GalleriesComponent } from '../galleries/galleries.component';
import { PricesComponent } from '../prices/prices.component';
import { AuthService } from '../../../core/user-service/auth.service';
import { SharedTableService } from '../shared-table.service';
import { ToastService } from '../../../core/toast/toast.service';
import { Router } from '@angular/router';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.less']
})
export class CreateComponent implements OnInit, AfterContentInit {
  public stringBaseInformation: string = 'اطلاعات پایه';
  public stringExtraInformation: string = "اطلاعات اضافه";
  public stringDiscount: string = "تخفیف";
  public stringImageGallery: string = "گالری تصاویر";
  public showTab: Array<boolean> = [false, true, true, true, true];
  public label: IKeyedCollection<string>;
  public MeasurementCreteriaData: Array<inputModel>;
  public TypeData: Array<inputModel>;
  public productData: Array<selectModel>;
  public brandData: Array<selectModel>;
  public subBrandData: Array<selectModel>;
  public supplierData: Array<selectModel>;
  public model: IproductSkuCreate = new IproductSkuCreate();;
  public hasValueField: boolean = false;
  public listFieldValue: number;
  public MeasurementCreteriaTitle: string;
  public GetTitleProduct: string;
  public selectedValueProduct: String;
  public GetTitleBrand: String;
  public GetTitleSubBrand: String;
  public baseUrl: string;
  public DataOfValueField: Array<any>;
  public CountDescription: number;

  @ViewChild(FieldValueComponent) public _fieldComponent: FieldValueComponent;
  @ViewChild(DiscountComponent) private _discountComponent: DiscountComponent;
  @ViewChild(GalleriesComponent) private _galleriesComponent: GalleriesComponent;
  @ViewChild(PricesComponent) private _pricesComponent: PricesComponent;

  constructor(
    private dataService:SetTitleNavbarService,
    private http: HttpServiceService,
    private auth: AuthService,
    private shared: SharedTableService,
    private router: Router,
    private toast: ToastService,
    private _baseUrl: AppConfigService) {
    this.DataOfValueField = [];
    this.model.ProductSkuPrices = [];
    this.baseUrl = _baseUrl.configuration.PicUrl;
    dataService.updatedDataSelection('افزودن کالای جدید');
  }

  ngOnInit() {
    this.label = new LabelProductSkuCreate().getLabel();

    this.http.post("productsku/GetMeasurementCreteriaList", {}).subscribe(x => {
      let _result: Array<any> = x.Result;
      this.MeasurementCreteriaData = _result.map(val => <inputModel>{
        value: val.Id,
        name: val.Id,
        text: val.Title,
      });
    });

    this.http.post("productsku/GetTypes", {}).subscribe(x => {
      let _result: Array<any> = x.Result;
      this.TypeData = _result.map(val => <inputModel>{
        value: val.Id,
        name: val.Id,
        text: val.Title,
      });
    });

    this.http.post("product/getlist", {
      ProductCategoryId: 0,
      ProductSubCategoryId: 0,
      ProductSuperCategoryId: 0,
      PageIndex: 0,
      PageSize: 45000,
      OrderByCol: "",
      OrderByDir: "",
    }).subscribe(x => {
      let _result: Array<any> = x.Result.List;
      this.productData = _result.map(val => <selectModel>{
        value: val.Id,
        text: val.Title,
      });
    });

    this.http.post("brand/getlist", {
      OrderByCol: "Title",
      OrderByDir: "ASC",
      PageIndex: 0,
      PageSize: 100000
    }).subscribe(x => {
      let _result: Array<any> = x.Result.List;
      this.brandData = _result.map(val => <selectModel>{
        value: val.Id,
        text: val.Title,
      });
    });

    this.http.post("supplier/getlist", {
      CityId: 0,
      StateId: 0,
      RegionId: 0,
      DistrictId: 0,
      OrderByCol: "Title",
      OrderByDir: "ASC",
      PageIndex: 0,
      PageSize: 100000
    }).subscribe(x => {
      let _result: Array<any> = x.Result.List;
      this.supplierData = _result.map(val => <selectModel>{
        value: val.Id,
        text: val.Title,
      });
    });
  }

  ngAfterContentInit(): void {
    this._discountComponent.discountCreate();
    this._galleriesComponent.galleriesCreate();
    this._pricesComponent.pricesCreate();
    this.CountDescription = 0;
  }

  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
    if (index == 4) this.extendModel();
  }

  public changeMeasurementCreteria(id): void {
    this.MeasurementCreteriaTitle = this.MeasurementCreteriaData.find(x => x.value == id).text;
  }

  public GetTitleType(id): String {
    return this.TypeData.find(x => x.value == id).text;
  }

  public GetTitleSupplier(e): string {
    if (e)
      return this.supplierData.find(x => x.value == e).text;
  }

  public selectField(e) {
    this.model.ProductSkuFields = [];
    this.hasValueField = false;
    this.listFieldValue = e;
    window.setTimeout(() => this.hasValueField = true, 1);
    this.GetTitleProduct = this.productData.find(x => x.value == e).text;
    this.selectedValueProduct = e;
    this.model.ProductId = e;
  }

  public selectBrands(e) {
    this.model.BrandId = e;
    this.http.post("subbrand/getlist", {
      OrderByCol: "Title",
      OrderByDir: "ASC",
      PageIndex: 0,
      PageSize: 100000,
      BrandId: e,
    }).subscribe(x => {
      let _result: Array<any> = x.Result.List;
      this.subBrandData = [];
      this.subBrandData = _result.map(val => <selectModel>{
        value: val.Id,
        text: val.Title,
      });
    });

    this.GetTitleBrand = this.brandData.find(x => x.value == e).text;
  }

  public selectSubBrand(e) {
    this.GetTitleSubBrand = this.subBrandData.find(x => x.value == e).text;
    this.model.SubBrandId = e;
  }

  public selectSuplier(e: Array<any>) {
    let _model: ISupplierProductSkus[] = [];
    e.forEach(x => {
      _model.push({
        SupplierId: parseInt(x)
      })
    });
    this.model.SupplierProductSkus = _model;
  }

  public selectType(id): void {
    this.model.ProductSkuInTypes = id;
  }

  public onSubmit(form: NgForm) {
    this.model.RegById = this.auth.getUserId();
    this.model.Stocks = [];
    if (form.valid) {
      this.http.post("productsku/Register", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/product-sku']);
          this.toast.Success();
        }
        else {
          this.toast.Error({
            msg: x.Message,
            title: 'اشکال در عملیات'
          })
        }
      }, err => { },
        () => this.toast.ClearWaiting());
    }
  }

  public changeType(e) {
    this.model.ProductSkuInTypes = e;
  }

  public setManualCode(e) {
    this.shared.ManualCode = e;
  }

  private extendModel() {
    if (this._fieldComponent) {
      this.model.ProductSkuFields = this._fieldComponent.listChildren();
    }
    this.model.Discounts = this._discountComponent.getModel();
    this.model.ProductSkuGalleries = this._galleriesComponent.getModel();
    this.model.ProductSkuPrices = [];
    this.model.ProductSkuPrices.push(this._pricesComponent.getModel());
  }

  public GetFieldValue(e) {
    this.DataOfValueField = e;
  }

  public ReviewFieldValue(e): string {
    let title = '';
    let text = '';
    for (let i of this.DataOfValueField) {
      if (i.FieldId == e.FieldId) {
        title = i.Title;
        switch (i.FieldTypeId) {
          //Textbox
          case 3:
            text = this.GetTextValueOfFieldValue(i.FieldId);
            break;
          //Boolean
          case 4:
            text = this.GetBoolValueOfFieldValue(i.FieldId);
            break;
          //MultiSelect
          case 2:
            text = this.GetMultiValueOfFieldValue(e.FieldSelectedValues, i);
            break;
          //SingleSelect
          case 1:
            text = this.GetSingleValueOfFieldValue(e.FieldValueId, i);
            break;
        }
      }
    }
    let html = `<dl><dt>${title}</dt><dd>${text}</dd></dl>`;
    return html;
  }

  private GetTextValueOfFieldValue(e): string {
    return this.model.ProductSkuFields.find(x => x.FieldId == e).TextValue;
  }

  private GetBoolValueOfFieldValue(e): string {
    const text = this.model.ProductSkuFields.find(x => x.FieldId == e).TextValue;
    let value: string = "&#10004;";
    if (text == "0")
      value = "&#10006;";
    return value;
  }

  private GetSingleValueOfFieldValue(e, obj: Object): string {
    let text: string = '';
    if (e) {
      const values = <Array<any>>obj['FieldValues'];
      text = values.find(x => x.Id == e).Value;
    }
    return text;
  }

  private GetMultiValueOfFieldValue(e: Array<any>, obj): string {
    let value: string = '';
    const values = <Array<any>>obj['FieldValues'];
    if (e)
      e.forEach(x => value += '<br />' + values.find(z => z.Id == x.FieldValueId).Value)
    return value;
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}