import { Component, OnInit, Input, TemplateRef, ViewChild, ViewChildren, ElementRef, Output, EventEmitter } from '@angular/core';
import { inputModel } from '../../../shared/tags/input-model';
import { IFieldValue } from '../iproduct-sku';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
@Component({
  selector: 'basketik-field-value',
  templateUrl: './field-value.component.html',
  styleUrls: ['./field-value.component.less']
})
export class FieldValueComponent implements OnInit {
  @Output() public GetFieldValue: EventEmitter<any> = new EventEmitter<any>();

  @Input('FieldId') Id: number;

  @ViewChild('Boolean', { read: TemplateRef }) boolean: TemplateRef<any>;
  @ViewChild('TextBox', { read: TemplateRef }) textbox: TemplateRef<any>;
  @ViewChild('SingleSelect', { read: TemplateRef }) singleSelect: TemplateRef<any>;
  @ViewChild('MultiSelect', { read: TemplateRef }) multiSelect: TemplateRef<any>;
  @ViewChildren('fieldValueVar', { read: ElementRef }) fieldValueVar: Array<ElementRef>;

  public _model: IKeyedCollection<string>;
  public fieldModel: Array<IFieldValue> = [];
  public listFieldValue: Array<any>;

  constructor(private http: HttpServiceService) { }

  ngOnInit() {
    this.http.post("productfield/GetProductFieldList", {
      ProductId: this.Id,
      OrderByCol: "RegDate",
      OrderByDir: "DESC",
      PageIndex: 0,
      PageSize: 100000
    }).subscribe(x => {
      this.listFieldValue = x.Result.List;
      this.GetFieldValue.emit(this.listFieldValue);
    });
  }

  public listChildren(): Array<IFieldValue> {
    let model: Array<IFieldValue> = [];
    this.fieldValueVar.forEach(x => {
      let _el: any = x.nativeElement;
      let _type: string = _el.getAttribute('data-type');
      let _id: number = _el.getAttribute('data-id');
      switch (_type) {
        case "Checkbox":
          model.push({
            FieldId: _id,
            TextValue: _el.checked ? "1" : "0"
          });
          break;
        case 'SingleSelect':
          model.push({
            FieldId: _id,
            FieldValueId: parseInt(_el.value)
          });
          break;
        case 'MultiSelect':
          let _model: Array<{ ProductSkuFieldId: number, FieldValueId: number }> = [];
          for (var i = 0; i < _el.options.length; i++) {
            if (_el.options[i].selected) {
              _model.push({
                FieldValueId: parseInt(_el.options[i].value),
                ProductSkuFieldId: _id
              });
            }
          }
          model.push({
            FieldId: _id,
            FieldSelectedValues: _model
          });
          break;

        case "Checkbox":
        default:
          model.push({
            FieldId: _id,
            TextValue: _el.value
          });
          break;
      }
    });
    return model;
  }
}
