import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductSkuListComponent } from './product-sku-list/product-sku-list.component';
import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  {
    path: 'product-sku',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ProductSkuListComponent
      },
      {
        path: 'create',
        component: CreateComponent
      },
      {
        path: ':hashId/edit',
        component: EditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductSkuRoutingModule { }
