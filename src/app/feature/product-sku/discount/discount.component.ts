import { Component, OnInit, ViewChild, TemplateRef, ViewContainerRef, ComponentFactoryResolver, ComponentFactory, ComponentRef, ViewChildren, QueryList, ContentChildren, OnDestroy, AfterViewInit, EventEmitter } from '@angular/core';
import { IDiscounts } from '../iproduct-sku';
import { TableComponent } from './table/table.component';
import { SharedTableService } from '../shared-table.service';
import { ToastService } from '../../../core/toast/toast.service';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { Subscriber } from 'rxjs/Subscriber';
@Component({
  selector: 'basketik-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.less']
})
export class DiscountComponent implements OnInit, OnDestroy {
  @ViewChild('container', { read: ViewContainerRef }) private container: ViewContainerRef;
  @ViewChildren(TableComponent) private tableChildren: QueryList<TableComponent>;

  private componentRef: ComponentRef<TableComponent>;
  private _model: IDiscounts;

  public get model(): IDiscounts {
    return this._model;
  }

  public set model(v: IDiscounts) {
    this._model = v;
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private shared: SharedTableService,
    private toast: ToastService,
    private http: HttpServiceService) {
    this.model = new IDiscounts();
  }

  ngOnInit() { }

  public index: number = 0;
  public discountCreate(e?: any, isEditable?: boolean) {
    let _i = this.index++;
    const factory: ComponentFactory<TableComponent> = this.resolver.resolveComponentFactory(TableComponent);
    this.componentRef = this.container.createComponent(factory, _i);
    this.componentRef.instance.index = _i;
    if (!e)
      this.componentRef.instance.Remove.subscribe(event => this.discountRemove());
    if (isEditable) {
      this.componentRef.instance.Remove = new EventEmitter();
      this.componentRef.instance.Remove.subscribe(event => this.discountRemoveAfterAdd());
    }
    this.componentRef.instance.create.subscribe(event => this.discountCreate());
    if (e) {
      this.componentRef.instance.model = e;
      this.componentRef.instance.isEdit = false;
      this.componentRef.instance.Remove.subscribe(event => this.discountRemove(e['Id']));
      this.componentRef.instance.disabled = true;
      this.componentRef.instance.disabledRemove = e.IsRemoved;
    }

  }

  public discountRemoveAfterAdd() {
    this.componentRef.instance.disabled = false;
  }

  public discountRemove(id?) {
    this.toast.Wait();

    this.http.post("ProductSkuDiscount/Remove", {
      "Id": id
    }).subscribe(x => {
      this.toast.Success({
        title: 'عملیات موفق',
        msg: 'حذف تخفیف با موفقیت انجام شد'
      });
      this.create(null, true);
      this.componentRef.instance.disabledRemove = true;
    }, err => this.toast.Error({
      title: 'عملیات ناموفق',
      msg: 'حذف تخفیف با موفقیت انجام نشد'
    }), () => this.toast.ClearWaiting());
  };

  public create(e, is?) {
    this.discountCreate(e, is);
  }

  public getModel(): Array<IDiscounts> {
    return this.shared.getModel();
  }

  ngOnDestroy() {
    if (this.componentRef)
      this.componentRef.destroy();
  }
}
