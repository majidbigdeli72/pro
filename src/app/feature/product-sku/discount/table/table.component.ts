import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IDiscounts } from '../../iproduct-sku';
import { SharedTableService } from '../../shared-table.service';
import { ToastService } from '../../../../core/toast/toast.service';

@Component({
  selector: 'basketik-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.less']
})
export class TableComponent implements OnInit {
  public model: IDiscounts = new IDiscounts();
  public disabled: boolean = false;
  public disabledRemove: boolean = true;
  public isEdit: boolean = false;
  public index: number;
  public hasValue: boolean = false;

  @Output() create: EventEmitter<any> = new EventEmitter<any>();
  @Output() Remove: EventEmitter<any> = new EventEmitter<any>();

  constructor(private shared: SharedTableService) { }

  public DiscountRemove(_index: number) {
    this.disabledRemove = true;
    this.Remove.emit(this.model['Id'])
  }

  public setmodel() {
    this.shared.setModelDiscount(this.model);
    this.disabled = true;
    this.disabledRemove = false;
  }

  ngOnInit() {
    if (this.isEdit) {
      if (this.model['IsRemoved'] || new Date(this.model.ToDateTime) <= new Date(Date.now())) {
        this.disabledRemove = true;
      }
      else
        this.disabled = true;
    }
  }

  public toDateTimeChange(e) {
    this.model.ToDateTime = e;
  }

  public fromDateTimeChange(e) {
    this.model.FromDateTime = e;
  }

  // public CheckHasValue() {
  //   if (this.model.DiscountPercent && this.model.DiscountAmount == null)
  //     this.hasValue = false;
  //   else if((this.model.DiscountPercent  == null && this.model.DiscountAmount))
  //     this.hasValue = true;
  //   console.log(this.model.DiscountPercent, this.hasValue)
  // }
}
