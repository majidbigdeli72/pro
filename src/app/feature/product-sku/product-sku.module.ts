import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProductSkuRoutingModule } from './product-sku-routing.module';
import { ProductSkuListComponent } from './product-sku-list/product-sku-list.component';
import { CreateComponent } from './create/create.component';
import { SharedModule } from '../../shared/shared.module';
import { FieldValueComponent } from './field-value/field-value.component';
import { DiscountComponent } from './discount/discount.component';
import { TableComponent } from './discount/table/table.component';
import { SharedTableService } from './shared-table.service';
import { GalleriesComponent } from './galleries/galleries.component';
import { InputsComponent } from './galleries/inputs/inputs.component';
import { InputsComponentPrices } from './prices/inputs/inputs.component';
import { PricesComponent } from './prices/prices.component';
import { EditComponent } from './edit/edit.component';
import { FieldValueEditComponent } from './edit/field-value-edit/field-value-edit.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProductSkuRoutingModule,
    FormsModule
  ],
  declarations: [
    ProductSkuListComponent,
    CreateComponent,
    FieldValueComponent,
    DiscountComponent,
    TableComponent,
    GalleriesComponent,
    InputsComponent,
    PricesComponent,
    InputsComponentPrices,
    EditComponent,
    FieldValueEditComponent
  ],
  entryComponents:[
    TableComponent,
    InputsComponent,
    InputsComponentPrices
  ],
  providers: [SharedTableService]
})
export class ProductSkuModule { }
