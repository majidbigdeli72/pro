import { Component, OnInit, ViewChild, AfterContentInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { IproductSkuCreate, LabelProductSkuCreate, ISupplierProductSkus, IDiscounts, IProductSkuInTypes, IProductSkuEdit } from '../iproduct-sku';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { selectModel, inputModel } from '../../../shared/tags/input-model';
import { IKeyedCollection } from '../../../core/dictionary';
import { FieldValueComponent } from '../field-value/field-value.component';
import { DiscountComponent } from '../discount/discount.component';
import { PricesComponent } from '../prices/prices.component';
import { GalleriesComponent } from '../galleries/galleries.component';
import { NgForm, NgModel } from '@angular/forms';
import { AuthService } from '../../../core/user-service/auth.service';
import { FieldValueEditComponent } from './field-value-edit/field-value-edit.component';
import { ToastService } from '../../../core/toast/toast.service';
import { ToastOptions } from 'ng2-toasty';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { debug } from 'util';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit, AfterContentInit, OnDestroy {
  public stringBaseInformation: string = 'اطلاعات پایه';
  public stringExtraInformation: string = "اطلاعات اضافه";
  public stringDiscount: string = "تخفیف";
  public stringImageGallery: string = "گالری تصاویر";
  public showTab: Array<boolean> = [false, true, true, true, true];
  public label: IKeyedCollection<string>;
  public MeasurementCreteriaData: Array<inputModel>;
  public TypeData: Array<inputModel>;
  public productData: Array<selectModel>;
  public brandData: Array<selectModel>;
  public subBrandData: Array<selectModel>;
  public supplierData: Array<selectModel>;
  public model: IProductSkuEdit;
  public hasValueField: boolean = true;
  public listFieldValue: number;
  public MeasurementCreteriaTitle: string;
  public GetTitleProduct: string;
  public selectedValueProduct: any;
  public GetTitleBrand: String;
  public GetTitleSubBrand: String;
  public subBrandSelect: boolean = false;
  public HashIdFieldValue: string;
  public newPriceProperty: number;
  public showAddPriceBtn: boolean = true;
  public disabledInputPrice: boolean = false;
  public $supplier: Array<number> = [];
  public $prices: Array<any> = [];
  public OldVaueBarcode: string;
  public OldVaueManualCode: string;
  public _showPrice: boolean = false;
  public DataOfValueField: Array<any>;
  public CountDescription: number;
  public showListPrice: boolean = true;
  public baseUrl: string;
  public showImageReview: boolean = false;
  public moreInformationShow: boolean = false;
  public $oldDiscount: Array<any>;

  private oldProductIdVale: number;
  private id: number;

  @ViewChild(FieldValueComponent) public _fieldComponent: FieldValueComponent;
  @ViewChild(FieldValueEditComponent) private _fieldValueEditComponent: FieldValueEditComponent;
  @ViewChild(DiscountComponent) private _discountComponent: DiscountComponent;
  @ViewChild(GalleriesComponent) private _galleriesComponent: GalleriesComponent;

  constructor(
    private dataService: SetTitleNavbarService,
    private _baseUrl: AppConfigService,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpServiceService,
    private auth: AuthService,
    private toast: ToastService) {
    this.model = new IProductSkuEdit();
    this.model.ProductSkuPrices = [];
    this.model.SupplierProductSkus = [];
    this.DataOfValueField = [];

    this.baseUrl = _baseUrl.configuration.PicUrl;
  }

  ngAfterContentInit(): void {
    this.CountDescription = 0;
  }

  ngOnInit() {
    this.label = new LabelProductSkuCreate().getLabel();
    this.route.params.subscribe(params => {
      this.id = params['hashId'];// x8GkBx
      this.moreInformationShow = false;

      this.http.post("panelspecific/productsku/get", {
        Id: this.id
      }).subscribe(x => {
        const _model = x.Result;
        this.model.Id = _model["Id"];
        this.model.HashId = _model["HashId"];
        this.model.ProductId = _model["ProductId"];
        this.model.RegById = _model["RegById"];
        this.model.Title = _model["Title"];
        this.model.EnTitle = _model["EnTitle"];
        this.model.BarCode = _model["BarCode"];
        this.model.Description = _model["Description"];
        this.model.BuyThresholdCount = _model["BuyThresholdCount"];
        this.model.MeasurementCreteriaId = _model["MeasurementCreteriaId"];
        this.model.BrandId = _model["BrandId"];
        this.model.SubBrandId = _model["SubBrandId"];
        this.model.CategoryId = _model["CategoryId"];
        this.model.SubCategoryId = _model["SubCategoryId"];
        this.model.ManualCode = _model["ManualCode"];
        this.model.ProductSkuInTypes = _model["Types"].map(x => <IProductSkuInTypes>{
          ProductTypeId: x.Id
        });
        this.model.PicName = _model["PicName"].replace(new RegExp(this._baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');
        this.model.ProductSkuGalleries = _model["Galleries"];
        this.$prices = _model["Prices"];
        this.$supplier = _model["Suppliers"].map(x => x.Id);
        this.model.Discounts = _model["Discounts"];

        this.oldProductIdVale = this.model.ProductId;
        this._galleriesComponent.create(null);
        this.model.ProductSkuGalleries.forEach(x => this._galleriesComponent.create(x));
        this.OldVaueBarcode = this.model.BarCode;
        this.OldVaueManualCode = this.model.ManualCode;
        this.CountDescription = this.model.Description.length;
        this.changeMeasurementCreteria(this.model.MeasurementCreteriaId);
        this.dataService.updatedDataSelection(`ویرایش ${this.model.Title}`);

        this.http.post("product/getlist", {
          ProductCategoryId: 0,
          ProductSubCategoryId: 0,
          ProductSuperCategoryId: 0,
          PageIndex: 0,
          PageSize: 45000,
          OrderByCol: "",
          OrderByDir: "",
        }).subscribe(x => {
          let _result: Array<any> = x.Result.List;
          this.productData = _result.map(val => <selectModel>{
            value: val.Id,
            text: val.Title,
          });
          this.selectField(this.model.ProductId);
          this.CheckDiscounts();
        });
      });

      this.http.post("productsku/GetMeasurementCreteriaList", {}).subscribe(x => {
        let _result: Array<any> = x.Result;
        this.MeasurementCreteriaData = _result.map(val => <inputModel>{
          value: val.Id,
          name: val.Id,
          text: val.Title,
        });
      });

      this.http.post("productsku/GetTypes", {}).subscribe(x => {
        let _result: Array<any> = x.Result;
        this.TypeData = _result.map(val => <inputModel>{
          value: val.Id,
          name: val.Id,
          text: val.Title,
        });
      });

      this.http.post("brand/getlist", {
        OrderByCol: "RegDate",
        OrderByDir: "Desc",
        PageIndex: 0,
        PageSize: 100000
      }).subscribe(x => {
        let _result: Array<any> = x.Result.List;
        this.brandData = _result.map(val => <selectModel>{
          value: val.Id,
          text: val.Title,
        });
        this.selectSubBrands(this.model.BrandId);
        this.GetTitleBrand = this.brandData.find(x => x.value == this.model.BrandId).text || '';
      });
      this.http.post("supplier/getlist", {
        CityId: 0,
        StateId: 0,
        RegionId: 0,
        DistrictId: 0,
        OrderByCol: "RegDate",
        OrderByDir: "Desc",
        PageIndex: 0,
        PageSize: 100000
      }).subscribe(x => {
        let _result: Array<any> = x.Result.List;
        this.supplierData = _result.map(val => <selectModel>{
          value: val.Id,
          text: val.Title,
        });
      });
    })
  }

  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
    if (index == 4) this.extendModel();
  }

  public changeMeasurementCreteria(id): void {
    this.MeasurementCreteriaTitle = this.MeasurementCreteriaData.find(x => x.value == id).text;
  }

  public GetTitleType(id): String {
    if (this.TypeData) {
      const v = this.TypeData.find(x => x.value == id);
      return v != null ? v.text : '';
    }
  }

  public GetTitleSupplier(e): string {
    if (e && this.supplierData)
      return this.supplierData.find(x => x.value == e).text;
  }

  public selectField(e) {
    this.model.ProductSkuFields = [];
    this.listFieldValue = e;
    this.HashIdFieldValue = this.model.HashId;
    window.setTimeout(() => this.hasValueField = e == this.oldProductIdVale ? true : false, 1);
    this.selectedValueProduct = e;
    this.model.ProductId = e;
    this.moreInformationShow = true;
  }

  public selectSubBrands(e) {
    this.http.post("subbrand/getlist", {
      OrderByCol: "RegDate",
      OrderByDir: "Desc",
      PageIndex: 0,
      PageSize: 100000,
      BrandId: e,
    }).subscribe(x => {
      let _result: Array<any> = x.Result.List;
      this.subBrandData = [];
      this.subBrandData = _result.map(val => <selectModel>{
        value: val.Id,
        text: val.Title,
      });
      this.subBrandSelect = true;
    });
  }

  public selectSubBrand(e) {
    this.GetTitleSubBrand = this.subBrandData.find(x => x.value == e).text;
    this.model.SubBrandId = e;
  }

  public selectSuplier(e: Array<any>) {
    let _model: ISupplierProductSkus[] = [];
    e.forEach(x => {
      _model.push({
        SupplierId: parseInt(x)
      })
    });
    this.model.SupplierProductSkus = _model;
  }

  public selectType(id): void {
    this.model.ProductSkuInTypes = id;
  }

  public onSubmit(form: NgForm) {
    this.toast.Wait()
    this.model.RegById = this.auth.getUserId();
    this.model.SupplierProductSkus = this.$supplier.map(x => <ISupplierProductSkus>{
      SupplierId: x
    });
    if (form.valid) {
      this.http.post("productsku/Update", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/product-sku']);
          this.toast.Success();
        }
        else {
          this.toast.Error({
            msg: x.Message,
            title: 'اشکال در عملیات'
          })
        }
      }, err => {
        this.toast.Error();
      }, () => this.toast.ClearWaiting())
    }
  }

  public changeType(e) {
    this.model.ProductSkuInTypes = e;
  }

  private extendModel() {
    this.showImageReview = true;
    this.model.ProductSkuFields = this.hasValueField ? this._fieldValueEditComponent.listChildren() : this._fieldComponent.listChildren();
    this.model.Discounts = this._discountComponent.getModel();
    this.model.ProductSkuGalleries = this._galleriesComponent.getModel();
  }

  public newPrice() {
    const _price = {
      Price: this.newPriceProperty,
    };
    this.model.ProductSkuPrices.push(_price);
    this.showAddPriceBtn = false;
    this.disabledInputPrice = true;
  }

  public editPrice() {
    this.showAddPriceBtn = true;
    this.model.ProductSkuPrices.pop();
    this.disabledInputPrice = false;
  }
  public newDiscount() {
    this._discountComponent.create(null);
  }

  public newPic() {
    this._galleriesComponent.create(null);
  }

  public changeStateProductId(e) {
    this.selectField(e.target.value);
  }

  public GetFieldValue(e) {
    this.DataOfValueField = e;
  }

  public ReviewFieldValue(e): string {
    let title: string,
      text: string;
    for (let j of this.DataOfValueField) {
      for (let i of j['Fields']) {
        if (i.FieldId == e.FieldId) {
          title = i.Title;
          switch (i.FieldTypeId) {
            //Textbox
            case 3:
              text = this.GetTextValueOfFieldValue(i.FieldId);
              break;
            //Boolean
            case 4:
              text = this.GetBoolValueOfFieldValue(i.FieldId);
              break;
            //MultiSelect
            case 2:
              text = this.GetMultiValueOfFieldValue(e.FieldSelectedValues, i);
              break;
            //SingleSelect
            case 1:
              text = this.GetSingleValueOfFieldValue(e.FieldValueId, i);
              break;
          }
        }
      }
    }
    console.log('bbbbbbbbbbbbbb')
    let html = `<dl><dt>${title}</dt><dd>${text}</dd></dl>`;
    return html;
  }

  public ReviewFieldValueNew(e): string {
    let title: string,
      text: string;
    for (let i of this.DataOfValueField) {
      if (i.FieldId == e.FieldId) {
        title = i.Title;
        switch (i.FieldTypeId) {
          //Textbox
          case 3:
            text = this.GetTextValueOfFieldValue(i.FieldId);
            break;
          //Boolean
          case 4:
            text = this.GetBoolValueOfFieldValue(i.FieldId);
            break;
          //MultiSelect
          case 2:
            text = this.GetMultiValueOfFieldValue(e.FieldSelectedValues, i);
            break;
          //SingleSelect
          case 1:
            text = this.GetSingleValueOfFieldValue(e.FieldValueId, i);
            break;
        }
      }
    }
    let html = `<dl><dt>${title}</dt><dd>${text}</dd></dl>`;
    return html;
  }
  private GetValue(i, e): { title: String, text: String } {
    let title: string,
      text: string;
    if (i.FieldId == e.FieldId) {
      title = i.Title;
      switch (i.FieldTypeId) {
        //Textbox
        case 3:
          text = this.GetTextValueOfFieldValue(i.FieldId);
          break;
        //Boolean
        case 4:
          text = this.GetBoolValueOfFieldValue(i.FieldId);
          break;
        //MultiSelect
        case 2:
          text = this.GetMultiValueOfFieldValue(e.FieldSelectedValues, i);
          break;
        //SingleSelect
        case 1:
          text = this.GetSingleValueOfFieldValue(e.FieldValueId, i);
          break;
      }
    }
    return {
      title: title,
      text: text
    }
  };


  private GetTextValueOfFieldValue(e): string {
    return this.model.ProductSkuFields.find(x => x.FieldId == e).TextValue;
  }

  private GetBoolValueOfFieldValue(e): string {
    const text = this.model.ProductSkuFields.find(x => x.FieldId == e).TextValue;
    let value: string = "&#10004;";
    if (text == "0")
      value = "&#10006;";
    return value;
  }

  private GetSingleValueOfFieldValue(e, obj: Object): string {
    let text: string = '';
    if (e) {
      const values = <Array<any>>obj['FieldValues'];
      text = values.find(x => x.Id == e).Value;
    }
    return text;
  }

  private GetMultiValueOfFieldValue(e: Array<any>, obj): string {
    let value: string = '';
    const values = <Array<any>>obj['FieldValues'];
    if (e)
      e.forEach(x => {
        if (values.find(z => z.Id == x.FieldValueId))
          return value += '<br />' + values.find(z => z.Id == x.FieldValueId).Value
      });
    return value;
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }

  CheckDiscounts(): any {
    if (this.model.Discounts.length <= 0) {
      this.newDiscount();
      return;
    }

    this.model.Discounts.sort((a, b) => {
      let dateA = new Date(a["RegDate"]).getTime(),
        dateB = new Date(b['RegDate']).getTime();
      return dateB - dateA;
    });

    const topDisocount = this.model.Discounts[0];
    if (topDisocount["IsRemoved"]) {
      this.model.Discounts.forEach(x => this._discountComponent.create(x));
      this.newDiscount();
      return;
    }
    this.model.Discounts.forEach(x => this._discountComponent.create(x));
  }

  ngOnDestroy(): void {
    this._discountComponent.ngOnDestroy();
  }
}
