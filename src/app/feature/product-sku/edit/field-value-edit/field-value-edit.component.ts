import { Component, OnInit, ViewChildren, ViewChild, Input, TemplateRef, ElementRef, EventEmitter, Output } from '@angular/core';
import { IFieldValue } from '../../iproduct-sku';
import { HttpServiceService } from '../../../../core/http-service/http-service.service';
import { IKeyedCollection } from '../../../../core/dictionary';

@Component({
  selector: 'basketik-field-value-edit',
  templateUrl: './field-value-edit.component.html',
  styleUrls: ['./field-value-edit.component.less']
})
export class FieldValueEditComponent implements OnInit {
  @Output() public GetFieldValue: EventEmitter<any> = new EventEmitter<any>();

  @Input('FieldId') Id: number;
  @Input('HashId') HashId: string;

  @ViewChild('Boolean', { read: TemplateRef }) boolean: TemplateRef<any>;
  @ViewChild('TextBox', { read: TemplateRef }) textbox: TemplateRef<any>;
  @ViewChild('SingleSelect', { read: TemplateRef }) singleSelect: TemplateRef<any>;
  @ViewChild('MultiSelect', { read: TemplateRef }) multiSelect: TemplateRef<any>;
  @ViewChildren('fieldValueVar', { read: ElementRef }) fieldValueVar: Array<ElementRef>;
  public _model: IKeyedCollection<string>;
  public fieldModel: Array<IFieldValue> = [];
  public listFieldValue: Array<any>;
  public selectedValueMultiSelect: Array<number> = [];
  public selectedValueSingleSelect: number;
  constructor(private http: HttpServiceService) { }

  ngOnInit() {
    this.http.post("productfield/GetProductSkuFieldList", {
      Title: "",
      Description: "",
      ProductSkuHashId: this.HashId,
      ProductId: this.Id,
      OrderByCol: "RegDate",
      OrderByDir: "DESC",
      PageIndex: 0,
      PageSize: 10000
    }).subscribe(x => {
      this.listFieldValue = x.Result.List;
      this.GetFieldValue.emit(this.listFieldValue);
    });
  }

  public listChildren(): Array<IFieldValue> {
    let model: Array<IFieldValue> = [];
    this.fieldValueVar.forEach(x => {
      let _el: any = x.nativeElement;
      let _type: string = _el.getAttribute('data-type');
      let _id: number = _el.getAttribute('data-id');
      switch (_type) {
        case "Checkbox":
          model.push({
            FieldId: _id,
            TextValue: _el.checked ? "1" : "0"
          });
          break;
        case 'SingleSelect':
          model.push({
            FieldId: _id,
            FieldValueId: parseInt(_el.value)
          });
          break;
        case 'MultiSelect':
          let _model: Array<{ FieldValueId: number }> = [];
          for (var i = 0; i < _el.options.length; i++) {
            if (_el.options[i].selected) {
              _model.push({
                FieldValueId: parseInt(_el.options[i].getAttribute('data-id'))
              });
            }
          }
          model.push({
            FieldId: _id,
            FieldSelectedValues: _model
          });
          break;

        case "Checkbox":
        default:
          model.push({
            FieldId: _id,
            TextValue: _el.value
          });
          break;
      }
    });
    return model;
  }


  public setselectedValueMultiSelect(items: Array<any>) {
    this.selectedValueMultiSelect = items;
  }

  public setselectedValueMultiSelectInit(items: Array<any>) {
    items.forEach(x => {
      if (!this.selectedValueMultiSelect.includes(x.Id)) {
        this.selectedValueMultiSelect.push(x.Id);
      }
    })
  }
  public IsChecked(e): boolean {
    return e === '1';
  }

  private SelectedDropDown(e, values: Array<number>) {
    return values.some(x => x == e);
  }

  public setselectedValueSingleSelectInit(item: any) {
   item && (this.selectedValueSingleSelect = item.Id);
  }
  public SelectedDropDownSingle(e, values: any) {
    return e == values;
  }
}