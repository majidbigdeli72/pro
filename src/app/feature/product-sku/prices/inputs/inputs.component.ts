import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IPrice } from '../../iproduct-sku';
import { SharedTableService } from '../../shared-table.service';

@Component({
  selector: 'basketik-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.less']
})
export class InputsComponentPrices implements OnInit {

  public model: IPrice = new IPrice();
  public disabled: boolean = false;
  public disabledRemove: boolean = true;
  public index: number;
  public isEdit: boolean = false;
  @Output() create: EventEmitter<any> = new EventEmitter<any>();
  @Output() Remove: EventEmitter<any> = new EventEmitter<any>();
  constructor(private shared: SharedTableService) {

  }

  public DiscountRemove(_index: number) {
    this.shared.removePrices(this.model);
    this.disabledRemove = true;
    this.disabled = false;
  }

  public setmodel() {
    this.shared.setModelPrices(this.model);
    this.disabled = true;
    this.disabledRemove = false;
  }

  ngOnInit() {
    console.log(this.model);
    if (this.isEdit)
      this.shared.setModelPrices(this.model);
  }
}
