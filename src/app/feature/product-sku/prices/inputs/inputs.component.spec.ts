import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputsComponentPrices } from './inputs.component';

describe('InputsComponent', () => {
  let component: InputsComponentPrices;
  let fixture: ComponentFixture<InputsComponentPrices>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputsComponentPrices ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputsComponentPrices);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
