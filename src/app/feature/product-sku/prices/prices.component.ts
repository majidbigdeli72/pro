import { Component, OnInit, ViewChild, ViewContainerRef, ViewChildren, QueryList, ComponentRef, ComponentFactoryResolver, ComponentFactory, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IPrice } from '../iproduct-sku';
import { SharedTableService } from '../shared-table.service';
import { InputsComponentPrices } from './inputs/inputs.component';

@Component({
  selector: 'basketik-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.less']
})
export class PricesComponent implements OnInit, OnChanges {

  @ViewChild('containerPrice', { read: ViewContainerRef }) private container: ViewContainerRef;
  @ViewChildren(InputsComponentPrices) private tableChildren: QueryList<InputsComponentPrices>;
  @Input() manualCode: number;
  private componentRef: ComponentRef<InputsComponentPrices>;
  private _model: IPrice;

  public get model(): IPrice {
    return this._model;
  }

  public set model(v: IPrice) {
    this._model = v;
  }

  constructor(private resolver: ComponentFactoryResolver, private shared: SharedTableService) {
    this.model = new IPrice();
  }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.manualCode = changes.manualCode.currentValue;
  }

  public index: number = 0;
  public pricesCreate(e?: any) {
    let _i = this.index++;
    const factory: ComponentFactory<InputsComponentPrices> = this.resolver.resolveComponentFactory(InputsComponentPrices);
    this.componentRef = this.container.createComponent(factory, _i);
    this.componentRef.instance.index = _i;
    this.componentRef.instance.Remove.subscribe(event => this.pricesRemove(_i));
    this.componentRef.instance.create.subscribe(event => this.pricesCreate());
    if (e) {
      this.componentRef.instance.model = e;
      this.componentRef.instance.disabled = true;
      this.componentRef.instance.isEdit = true;
      this.componentRef.instance.disabledRemove = false;
    }
  }

  public pricesRemove(index) {
    this.container.remove(index);
    if (this.container.length < 1)
      this.pricesCreate();
  }

  public create(e) {
    this.pricesCreate(e);
  }

  public getModel(): IPrice {
    return this.shared.getModelPrices();
  }
  ngOnDestroy() {
    if (this.componentRef)
      this.componentRef.destroy();
  }
}
