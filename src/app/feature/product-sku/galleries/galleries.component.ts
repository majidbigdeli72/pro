import { Component, OnInit, OnDestroy, ViewChild, ViewContainerRef, ViewChildren, QueryList, ComponentRef, ComponentFactoryResolver, ComponentFactory, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { InputsComponent } from './inputs/inputs.component';
import { IProductSkuGalleries } from '../iproduct-sku';
import { SharedTableService } from '../shared-table.service';

@Component({
  selector: 'basketik-galleries',
  templateUrl: './galleries.component.html',
  styleUrls: ['./galleries.component.less']
})
export class GalleriesComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('containerGalleries', { read: ViewContainerRef }) private container: ViewContainerRef;
  @ViewChildren(InputsComponent) private tableChildren: QueryList<InputsComponent>;
  private componentRef: ComponentRef<InputsComponent>;
  private _model: IProductSkuGalleries;
  //private factory;

  public get model(): IProductSkuGalleries {
    return this._model;
  }

  public set model(v: IProductSkuGalleries) {
    this._model = v;
  }

  constructor(
    private resolver: ComponentFactoryResolver,
    private shared: SharedTableService) {
    this.model = new IProductSkuGalleries();
  }

  ngOnInit() { }
  ngAfterViewInit(): void {
    
  }
  public index: number = 0;
  public galleriesCreate(e?: any) {
    let _i = this.index++;
    const factory : ComponentFactory<InputsComponent>= this.resolver.resolveComponentFactory(InputsComponent);
    let _componentRef = this.componentRef;
    _componentRef = this.container.createComponent(factory);
    _componentRef.instance.index = _i;
    _componentRef.instance.Remove.subscribe(event => this.galleriesRemove(_i));
    _componentRef.instance.create.subscribe(event => this.galleriesCreate());
    _componentRef.instance.destroy.subscribe(() =>_componentRef.destroy())
    if (e) {
      _componentRef.instance.model = e;
      _componentRef.instance.disabled = true;
      _componentRef.instance.isEdit = true;
      _componentRef.instance.disabledRemove = false;
    }
  }

  public galleriesRemove(index) {
    if (this.container.length < 1)
      this.galleriesCreate();
  }

  public create(e) {
    this.galleriesCreate(e);
  }

  public getModel(): Array<IProductSkuGalleries> {
    return this.shared.getModelGalleries();
  }

  ngOnDestroy() {
    if (this.componentRef)
      this.componentRef.destroy();
    this.shared.clearSharedGallery();
    this.shared.clearSharedDiscount();
    this.shared.clearSharedPrices();
  }
}
