import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IProductSkuGalleries } from '../../iproduct-sku';
import { SharedTableService } from '../../shared-table.service';
import { AppConfigService } from '../../../../core/app-config-setting-bootstrap/app-config-service.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'basketik-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.less']
})
export class InputsComponent implements OnInit {

  public model: IProductSkuGalleries = new IProductSkuGalleries();;
  public disabled: boolean = false;
  public disabledRemove: boolean = true;
  public index: number;
  public isEdit: boolean = false;
  public baseUrl: string;
  @Output() create: EventEmitter<any> = new EventEmitter<any>();
  @Output() Remove: EventEmitter<any> = new EventEmitter<any>();

  constructor(private shared: SharedTableService, private _baseUrl: AppConfigService) {
    this.baseUrl = _baseUrl.configuration.PicUrl;
  }

  public PicRemove(_index: number) {
    this.shared.removeGalleries(this.model);
    this.delete();
    this.Remove.emit(_index);
  }

  public setmodel() {
    this.shared.setModelGalleries(this.model);
    this.disabled = true;
    this.create.emit();
    this.disabledRemove = false;
  }

  ngOnInit() {
    if (this.isEdit) {
      if (this.model.PicName)
        this.model.PicName = this.model.PicName.replace(new RegExp(this._baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');
      this.shared.setModelGalleries(this.model);
    }
  }

  public destroy = new Subject<void>()
  public delete() {
    this.destroy.next(null)
  }
}
