import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSkuListComponent } from './product-sku-list.component';

describe('ProductSkuListComponent', () => {
  let component: ProductSkuListComponent;
  let fixture: ComponentFixture<ProductSkuListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSkuListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSkuListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
