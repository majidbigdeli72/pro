import { Component, OnInit } from '@angular/core';
import { IproductSkuList, Ilabel } from '../iproduct-sku';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { Subject } from 'rxjs/Subject';
import { debounceTime, distinctUntilChanged, flatMap, switchMap } from 'rxjs/operators';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  templateUrl: './product-sku-list.component.html',
  styleUrls: ['./product-sku-list.component.less']
})
export class ProductSkuListComponent implements OnInit {
  public label: IproductSkuList;
  public refresh: boolean = true;
  public fieldSearch: string = "Title";
  public url: string = 'panelspecific/productsku/getlist';
  public columns: Array<any>;
  public config: any = {};
  public body: any = {
    Title: "",
    Description: "",
    PicName: "",
    ManualCode: "",
    RegById: "",
    SubCategoryId: 0,
    CategoryId: 0,
    FromPrice: 0,
    ToPrice: 0,
    ProductId: 0,
    SupplierId: 0,
    TypeIds: [],
    BrandIds: [],
    FieldSearchParams: [],
    OrderByCol: "RegDate",
    OrderByDir: "DESC",
    PageIndex: 0,
    PageSize: 10
  }

  constructor(
    private dataService:SetTitleNavbarService,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService) {
      dataService.updatedDataSelection('لیست کالاها');
    this.label = new Ilabel().getLabel();
    this.columns = [
      { title: this.label.Id, name: 'Id', sort: "" },
      {
        title: this.label.Title,
        name: 'Title',
        sort: '',
        filtering: { filterString: '', placeholder: 'جستجو در عنوان' }
      },
      { title: this.label.ProductTitle, name: 'ProductTitle', sort: false },
      { title: this.label.Description, name: 'Description', sort: false },
      { title: this.label.RegDate, name: 'RegDate', sort: "" },
      { title: this.label.Price, name: 'Price', sort: "" },
      { title: this.label.BarCode, name: 'BarCode', sort: "" },
      { title: this.label.IsShowInMainPage, name: 'IsShowInMainPage', sort: true, icon: true },
      { title: 'عملیات', name: 'Action', sort: false },
    ];
  }

  ngOnInit() {

  }
  public onRemoveClicked(id) {
    this.toast.Wait();
    this.http.post('productsku/remove', {
      Id: id,
      OrderByCol: "Title",
      OrderByDir: "ASC",
      PageIndex: 0,
      PageSize: 20
    }).subscribe(status => {
      this.toast.Success({
        msg: 'عملیات با موفقیت انجام شد',
        title: 'حذف'
      });
    }, errr => { }, () => this.toast.ClearWaiting());
    this.refresh = false;
    setTimeout(() => this.refresh = true, 1)
  }

  public onEditClicked(e) {
    this.router.navigate([e.id, 'edit'], { relativeTo: this.route });
  }

  public ChangeFieldSearch(e:Event) {
    this.fieldSearch = this.fieldSearch == "Title" ? "ManualCode" : "Title";
  }
}
