import { IKeyedCollection, KeyedCollection } from '../../core/dictionary';

export class IproductSkuList {
    constructor(
        public Id?: any,
        public Title?: string,
        public ProductTitle?: string,
        public Description?: string,
        public RegDate?: string,
        public Price?: any,
        public BarCode?: string,
        public IsShowInMainPage?: any
    ) { }
}

export class Ilabel {
    public getLabel(): IproductSkuList {
        let label: IproductSkuList = new IproductSkuList();
        label.Id = 'شماره';
        label.Title = 'عنوان';
        label.ProductTitle = 'عنوان محصول';
        label.Description = 'توضیحات';
        label.RegDate = 'تاریخ ثبت';
        label.Price = 'قیمت';
        label.BarCode = 'بارکد';
        label.IsShowInMainPage = 'ویژه';
        return label;
    }
}

export class IproductSkuCreate {
    constructor(
        Title?: string,
        EnTitle?: string,
        Description?: string,
        ProductId?: number,
        BrandId?: number,
        SubBrandId?: number,
        MeasurementCreteriaId?: number,
        BarCode?: string,
        ManualCode?: string,
        PicName?: string,
        BuyThresholdCount?: string,
        ProductSkuPrices?: Array<IPrice>,
        Discounts?: Array<IDiscounts>,
        SupplierProductSkus?: Array<ISupplierProductSkus>,
        Stocks?: Array<any>,
        ProductSkuInTypes?: Array<IProductSkuInTypes>,
        ProductSkuGalleries?: Array<IProductSkuGalleries>,
        Product?: Array<IFieldValue>,
        RegById?: string,
        Prices?: Array<IPrice>
    ) {
        this.Title = Title;
        this.EnTitle = EnTitle;
        this.Description = Description;
        this.ProductId = ProductId;
        this.BrandId = BrandId;
        this.PicName = PicName;
        this.SubBrandId = SubBrandId;
        this.MeasurementCreteriaId = MeasurementCreteriaId;
        this.BarCode = BarCode;
        this.ManualCode = ManualCode;
        this.BuyThresholdCount = BuyThresholdCount;
        this.ProductSkuPrices = ProductSkuPrices;
        this.Discounts = Discounts;
        this.SupplierProductSkus = SupplierProductSkus;
        this.Stocks = Stocks;
        this.ProductSkuInTypes = ProductSkuInTypes;
        this.ProductSkuGalleries = ProductSkuGalleries;
        this.ProductSkuFields = Product;
        this.RegById = RegById;
        this.Prices = Prices;
    }

    public Title: string;
    public EnTitle?: string;
    public Description?: string;
    public ProductId?: number;
    public BrandId?: number;
    public SubBrandId?: number;
    public MeasurementCreteriaId?: number;
    public BarCode?: string;
    public PicName?: string;
    public ManualCode?: string;
    public BuyThresholdCount?: string;
    public ProductSkuPrices?: Array<IPrice>;
    public Discounts?: Array<IDiscounts>;
    public SupplierProductSkus?: Array<ISupplierProductSkus>;
    public Stocks?: Array<any>;
    public ProductSkuInTypes?: Array<IProductSkuInTypes>;
    public ProductSkuGalleries?: Array<IProductSkuGalleries>;
    public ProductSkuFields?: Array<IFieldValue>;
    public RegById?: string;
    public Prices?: Array<any>;
}

export class LabelProductSkuCreate {
    public getLabel(): IKeyedCollection<string> {
        let label = new KeyedCollection<string>();
        label.Add('Image', 'تصویر');
        label.Add('Type', 'نوع وعده');
        label.Add('Title', 'عنوان');
        label.Add('EnTitle', 'عنوان لاتین');
        label.Add('Description', 'توضیحات');
        label.Add('Product', 'محصول');
        label.Add('PicName', 'عکس اصلی محصول');
        label.Add('Brand', 'برند');
        label.Add('SubBrand', 'زیر برند');
        label.Add('MeasurementCreteriaId', 'معیار اندازه گیری');
        label.Add('BarCode', 'بارکد');
        label.Add('ManualCode', 'کد رفاه');
        label.Add('BuyThresholdCount', 'حداکثر تعداد خرید');
        label.Add('Price', 'قیمت ها');
        label.Add('Supplier', 'تامین کننده');
        label.Add('Discount', 'تخفیف');
        return label;
    }
}

export class IPrice {
    constructor(Price?: number) {
        this.Price = Price;
    }
    public Price: number;
}

export class IDiscounts {
    constructor(
        Description?: string,
        DiscountPercent?: number,
        DiscountAmount?: number,
        FromDateTime?: string,
        ToDateTime?: string,
        IsRemoved?: boolean) {
        this.DiscountAmount = DiscountAmount;
        this.DiscountPercent = DiscountPercent;
        this.FromDateTime = FromDateTime;
        this.ToDateTime = ToDateTime;
        this.Description = Description;
        this.IsRemoved = IsRemoved;
    }
    ///2/7/2018 18:00:00
    public Description: string;
    public DiscountPercent: number;
    public DiscountAmount: number;
    public FromDateTime: string;
    public ToDateTime: string;
    public IsRemoved: boolean;
}

export class ISupplierProductSkus {
    constructor(SupplierId?: any) {
        this.SupplierId = SupplierId;
    }
    public SupplierId: any;
}

export class IProductSkuInTypes {
    constructor(ProductTypeId?: number) {
        this.ProductTypeId = ProductTypeId;
    }
    public ProductTypeId: number;
}

export class IFieldValue {
    constructor(
        FieldId: number,
        TextValue?: string,
        FieldValueId?: number,
        FieldSelectedValues?: Array<{ ProductSkuFieldId: number, FieldValueId: number }>) {
        this.FieldId = FieldId;
        this.TextValue = TextValue;
        this.FieldValueId = FieldValueId;
        this.FieldSelectedValues = FieldSelectedValues;
    }
    public FieldId: number;
    public TextValue?: string;
    public FieldValueId?: number;
    public FieldSelectedValues?: Array<{ ProductSkuFieldId?: number, FieldValueId: number }>;
}

export class IProductSkuGalleries {
    constructor(PicTitle?: string, PicName?: string) {
        this.PicName = PicName;
        this.PicTitle = PicTitle;
    }
    public PicTitle: string;
    public PicName: string;
}

export class IProductSkuEdit {
    constructor(
        public Id?: number,
        public HashId?: string,
        public ProductId?: number,
        public RegById?: string,
        public Title?: string,
        public EnTitle?: string,
        public PicName?: string,
        public BarCode?: string,
        public Description?: string,
        public BuyThresholdCount?: number,
        public MeasurementCreteriaId?: number,
        public BrandId?: number,
        public SubBrandId?: number,
        public CategoryId?: number,
        public SubCategoryId?: number,
        public ManualCode?: string,
        public ProductSkuFields?: Array<IFieldValue>,
        public ProductSkuInTypes?: Array<IProductSkuInTypes>,
        public ProductSkuGalleries?: Array<IProductSkuGalleries>,
        public ProductSkuPrices?: Array<IPrice>,
        public SupplierProductSkus?: Array<ISupplierProductSkus>,
        public Discounts?: Array<IDiscounts>
    ) { }
}