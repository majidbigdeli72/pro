import { TestBed, inject } from '@angular/core/testing';

import { SharedTableService } from './shared-table.service';

describe('SharedTableService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedTableService]
    });
  });

  it('should be created', inject([SharedTableService], (service: SharedTableService) => {
    expect(service).toBeTruthy();
  }));
});
