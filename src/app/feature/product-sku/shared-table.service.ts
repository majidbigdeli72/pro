import { Injectable } from '@angular/core';
import { IDiscounts, IProductSkuGalleries, IPrice } from './iproduct-sku';

@Injectable()
export class SharedTableService {

  private modelDiscount: Array<IDiscounts>;
  private modelGalleries: Array<IProductSkuGalleries>;
  private modelPrices: IPrice;
  private _manualCode: number;
  public get ManualCode(): number {
    return this._manualCode;
  }

  public set ManualCode(val: number) {
    this._manualCode = val;
  }
  constructor() {
    this.modelDiscount = [];
    this.modelGalleries = [];
    this.modelPrices = new IPrice();
  }

  //#region Discount
  public getModel() {
    return this.modelDiscount;
  }

  public setModelDiscount(value: IDiscounts) {
    const _model: IDiscounts = {
      Description: value.Description,
      FromDateTime: value.FromDateTime,
      DiscountPercent: value.DiscountPercent == undefined ? null : value.DiscountPercent,
      ToDateTime: value.ToDateTime,
      DiscountAmount: value.DiscountAmount == undefined ? null : value.DiscountAmount,
      IsRemoved: null
    };
    this.modelDiscount.pop();
    this.modelDiscount.push(_model);
  }

  public removeDiscount(value: IDiscounts) {
    const index = this.modelDiscount.findIndex(x => JSON.stringify(x) == JSON.stringify(value));
    this.modelDiscount.splice(index, 1);
  }

  public clearSharedDiscount() {
    this.modelDiscount = [];
  }
  //#endregion

  //#region Price
  public getModelPrices() {
    return this.modelPrices;
  }

  public setModelPrices(value: IPrice) {
    this.modelPrices = {
      Price: value.Price ? value.Price : value['ProductSkuPrices']
    };
  }

  public removePrices(value: IPrice) {
    this.setModelPrices(value);
  }

  public clearSharedPrices() {
    this.modelPrices = new IPrice();
  }
  //#endregion


  //#region Gallery
  public getModelGalleries() {
    return this.modelGalleries;
  }

  public setModelGalleries(value: IProductSkuGalleries) {
    this.modelGalleries.push({
      PicName: value.PicName,
      PicTitle: value.PicTitle
    });
  }

  public removeGalleries(value: IProductSkuGalleries) {
    this.modelGalleries.forEach((x, i) => {
      if (x.PicName == value.PicName)
        this.modelGalleries.splice(i, 1);
    })
  }

  public clearSharedGallery() {
    this.modelGalleries = [];
  }
  //#endregion
}
