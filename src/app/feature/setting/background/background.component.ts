import { Component, OnInit } from '@angular/core';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { BrowserStorageService } from '../../../core/browser-storage/browser-storage-service.service';

@Component({
  selector: 'basketik-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.less']
})
export class BackgroundComponent implements OnInit {

  public items: Array<string>;
  public src: string
  public showModal: boolean = false
  public baseUrl: string;
  constructor(
    private storage:BrowserStorageService,
    private _baseUrl: AppConfigService,
    private http: HttpServiceService,
    private dataService: SetTitleNavbarService) {
    dataService.updatedDataSelection('انتخاب پس زمینه');
    this.baseUrl = _baseUrl.configuration.PicUrl;
    this.items = [];
  }

  ngOnInit() {
    this.http.post("filemanager/exec", {
      "mode": "getfolder",
      "path": "Background/",
      "thumbnail": false,
    }).subscribe(x => {
      this.items = x.Data.Data.map(z => z.Attributes.Path);
    })
  }
  setImage(src) {
    this.src = src;
    this.showModal = true;
  }
  confirmEdit() {
    let body = document.getElementsByTagName('body')[0];
    body.style.backgroundImage = `url(${this.baseUrl + this.src})`;
    this.storage.setLocal("setting_background", this.baseUrl + this.src);
  }
}
