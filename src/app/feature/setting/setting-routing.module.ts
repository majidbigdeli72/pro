import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BackgroundComponent } from "./background/background.component";

import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
const routes: Routes = [
  {
    path: 'setting',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: BackgroundComponent,
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingRoutingModule { }
