import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingRoutingModule } from './setting-routing.module';
import { BackgroundComponent } from './background/background.component';
import { SharedModule } from '../../shared/shared.module'

@NgModule({
  imports: [
    CommonModule,
    SettingRoutingModule,
    SharedModule
  ],
  declarations: [BackgroundComponent]
})
export class SettingModule { }
