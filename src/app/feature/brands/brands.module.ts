import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { BrandsRoutingModule } from './brands-routing.module';
import { CreateComponent } from '../brands/create/create.component';
import { ListComponent } from './list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from '../../shared/shared.module'
@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    BrandsRoutingModule,
    HttpClientModule,
  ],
  declarations: [CreateComponent, ListComponent, EditComponent],
})
export class BrandsModule { }
