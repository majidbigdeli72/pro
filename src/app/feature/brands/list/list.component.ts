import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Ilabel, Ibrands } from '../ibrands';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';


@Component({
  selector: 'basketik-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less'],
})
export class ListComponent implements OnInit {
  public label: Ibrands;
  public url: string = 'brand/getlist';
  public columns: Array<any>;
  public refresh: boolean = true;
  public config: any = {};
  public body: any = {
    Title: "",
    OrderByCol: "RegDate",
    OrderByDir: "DESC",
    PageIndex: 0,
    PageSize: 10
  }
  constructor(
    private dataService:SetTitleNavbarService,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService) {
      dataService.updatedDataSelection('لیست برندها ');
    this.label = new Ilabel().getLabel();
    this.columns = [
      { title: this.label.Id, name: 'Id', sort: true },
      { title: this.label.Title, name: 'Title', sort: true },
      {
        title: this.label.Description,
        name: 'Description',
        sort: false
      },
      { title: this.label.PicName, name: 'PicName', sort: false, pic: true },
      { title: this.label.ProducerTitle, name: 'ProducerTitle', sort: true },
      { title: this.label.IsSpecial, name: 'IsSpecial', sort: true, icon: true },
      { title: 'عملیات', name: 'Action', sort: false },
    ];
  }
  ngOnInit() { }
  public onRemoveClicked(id) {
    this.toast.Wait();
    this.http.post('brand/remove', {
      Id: id
    }).subscribe(status => {
      this.toast.Success();
    }, errr => { }, () => this.toast.ClearWaiting());
    this.refresh = false;
    setTimeout(() => this.refresh = true, 1);
  }
  public onEditClicked(e) {
    this.router.navigate([e.id, 'edit'], { relativeTo: this.route });
  }
}




