import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { NgForm, NgModel } from "@angular/forms";
import { Ibrands, Ilabel, IKeys } from "../ibrands";
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ToastService } from '../../../core/toast/toast.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';


@Component({
  selector: 'basketik-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.less']
})
export class CreateComponent implements OnInit, AfterContentInit {

  @ViewChild('forms', { read: NgForm }) _forms: NgForm;
  public model: Ibrands = new Ibrands();
  public label: Ibrands = new Ibrands();
  public showTab: boolean = true;
  public stringFormTitle: string = "اطلاعات پایه";
  public Producers: Array<{ id: number, name: string }>;
  public CountDescription: number;
  public urlPic: string;
  constructor(
    private dataService:SetTitleNavbarService,
    private http: HttpServiceService,
    private authService: AuthService,
    private baseUrl: AppConfigService,
    private router: Router,
    private toast: ToastService) {
      dataService.updatedDataSelection('افزودن برند');
  }

  ngAfterContentInit(): void {
    this.CountDescription = 0;
  }

  ngOnInit() {
    this.label = new Ilabel().getLabel();
    this.http.post("producer/getlist", {
      Title: "",
      RegById: "",
      PageIndex: 0,
      PageSize: 1000000
    }).subscribe(x => {
      if (x.Code == 200) {
        let list = <Array<any>>x.Result.List;
        this.Producers = list.map(val => <IKeys>{
          id: val.Id,
          name: val.Title
        });
      }
    });
    this.urlPic = this.baseUrl.configuration.PicUrl;
  }

  public toggleShowTab() {
    this.showTab = !this.showTab;
  }

  public getProducer(e) {
    if (this.model.ProducerId) {
      return this.Producers.find(x => x.id == e).name;
    }
  }

  public onSubmit(e: NgForm) {
    this.toast.Wait();
    this.model.RegById = this.authService.getUserId();
    if (e.form.valid) {
      this.http.post("brand/Register", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/brand']);
          this.toast.Success();
        }
        else
          this.toast.Error();
      }, err => { }, () => this.toast.ClearWaiting());
    }
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
