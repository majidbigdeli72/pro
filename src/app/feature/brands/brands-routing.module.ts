import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from "../brands/create/create.component";
import { ListComponent } from "../brands/list/list.component";
import { EditComponent } from "../brands/edit/edit.component";

import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
const routes: Routes = [
  {
    path: 'brand',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'create',
        component: CreateComponent
      },
      {
        path: ':id/edit',
        component: EditComponent
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandsRoutingModule { }
