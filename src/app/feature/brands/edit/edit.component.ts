import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, NgModel } from "@angular/forms";
import { Ibrands, Ilabel, IKeys } from "../ibrands";
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit {

  private id: number;
  
  public label: Ibrands = new Ibrands();
  public model: Ibrands = new Ibrands();
  public proId: number;
  public showTab: boolean = true;
  public stringFormTitle: string = "اطلاعات پایه";
  public Producers: Array<{ id: number, name: string }>;
  public CountDescription: number;
  public urlPic: string;

  constructor(
    private dataService:SetTitleNavbarService,
    private toast: ToastService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private http: HttpServiceService,
    private router: Router,
    private baseUrl: AppConfigService) { }

  ngOnInit() {
    this.label = new Ilabel().getLabel();
    this.urlPic = this.baseUrl.configuration.PicUrl;
     this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.http.post("producer/getlist", {
        Title: "",
        RegById: "",
        PageIndex: 0,
        PageSize: 1000000
      }).subscribe(x => {
        if (x.Code == 200) {
          let list = <Array<any>>x.Result.List;
          this.Producers = list.map(val => <IKeys>{
            id: val.Id,
            name: val.Title
          });
          this.http.post('/brand/get', {
            Id: this.id
          }).subscribe(x => {
            this.model = x.Result;
            this.dataService.updatedDataSelection(`ویرایش ${this.model.Title}`);
            this.model.Description && (this.CountDescription = this.model.Description.length);
            this.model.PicName = this.model.PicName.replace(new RegExp(this.baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');
          });
        }
      });
    });
  }


  public toggleShowTab() {
    this.showTab = !this.showTab;
  }
  public getProducer(e) {
    if (this.model.ProducerId) {
      return this.Producers.find(x => x.id == e).name;
    }
  }
  public onSubmit(e: NgForm) {
    this.toast.Wait();
    if (e.form.valid) {
      this.http.post("brand/Update", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/brand']);
          this.toast.Success();
        }
        else
          this.toast.Error();
      }, err => { }, () => this.toast.ClearWaiting());
    }
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
