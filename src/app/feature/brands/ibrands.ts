export class Ibrands {
    constructor(
        public Id?: any,
        public Title?: string,
        public EnTitle?: string,
        public Description?: string,
        public PicName?: string,
        public RegById?: string,
        public IsSpecial?: any,
        public ProducerId?: number) { }
        public ProducerTitle:String;
}


export class Ilabel {
    public getLabel(): Ibrands {
        let label: Ibrands = new Ibrands();
        label.Id = "شماره";
        label.Title = "عنوان";
        label.PicName = "تصویر";
        label.EnTitle = "عنوان لاتین";
        label.Description = "توضیحات";
        label.ProducerTitle = "تولیدکننده";
        label.IsSpecial = "ویژه";
        return label;
    }
}

export interface IKeys { id: number, name: string };