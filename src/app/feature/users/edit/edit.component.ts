import { Component, OnInit } from '@angular/core';
import { Ilabel, Iusers, Genders, iGender } from "../Iusers";
import { selectModel, inputModel } from '../../../shared/tags/input-model';
import { text } from '@angular/core/src/render3/instructions';
import { Input } from '@angular/core/src/metadata/directives';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';

interface Iitems { Id: number, Name: string };

@Component({
  selector: 'basketik-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit {
  public RolesSelect: Array<any>
  public RoleIds: Array<any> = [];
  public showTab: Array<boolean> = [false, true, true];
  public disabled;
  public PicName: string
  public model: Iusers;
  public MeasurementCreteriaData: Array<inputModel>;
  public label: Iusers;
  public States: Array<{ Id: number, Name: string }>;
  public Citys: Array<{ Id: number, Name: string }>;
  public stringFormTitle = "اطلاعات پایه";
  public GenderTitle: string
  private sub: any;
  public pic64: any;
  public items: Array<Iitems>;
  public UserRole: any
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpServiceService,
    private auth: AuthService,
    private toast: ToastService) {
    this.MeasurementCreteriaData = new Genders().getGenders();
    this.label = new Ilabel().getLabel();
    this.model = new Iusers();
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.model.Id = params['id'];
      this.http.post("account/GetRoleList", {
        Title: "",
        OrderByCol: "Id",
        OrderByDir: "DESC",
        PageIndex: 0,
        PageSize: 10000
      }).subscribe(x => {
        if (x.Code === 200) {
          let list = <Array<any>>x.Result.List;
          console.log(list);
          this.items = list.map(val => <Iitems>{
            Id: val.Id,
            Name: val.Name
          });
          this.http.post("address/getstatelist", {
            Id: 1
          }).subscribe(x => {
            if (x.Code == 200) {
              let list = <Array<any>>x.Result;
              this.States = list.map(val => <Iitems>{
                Id: val.Id,
                Name: val.Title
              });
              this.http.post("account/getuserdetail", {
                Id: this.model.Id
              }).subscribe(x => {
                var data = x.Result;
                this.http.post("address/getcitylist", {
                  Id: x.Result.CityId
                }).subscribe(x => {
                  if (x.Code == 200) {
                    let list = <Array<any>>x.Result;
                    this.Citys = list.map(val => <Iitems>{
                      Id: val.Id,
                      Name: val.Title
                    });
                    this.model = data;
                    this.PicName = data.PicName;
                    this.GenderTitle = this.MeasurementCreteriaData.find(l => l.value === this.model.Gender).text;
                  }
                });
              });
              this.http.post("account/GetUserRoles", {
                Id: this.model.Id
              }).subscribe(x => {
                console.log(x.Result);
                for (let i = 0; i < this.items.length; i++) {
                  for (let j = 0; j < x.Result.length; j++) {
                    if (this.items[i].Name === x.Result[j]) {
                      this.RoleIds.push(this.items[i].Id)
                    }
                  }
                }
              })
            }
          });
        }
      });
    });
  };
  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
    if (index === 2) {
      this.RolesSelect = [];
      for (let i = 0; i < this.RoleIds.length; i++) {
        const RoleId = this.RoleIds[i];
        var RoleNames = this.items.find(x => x.Id === RoleId).Name;
        this.RolesSelect.push(RoleNames);
      }
    }
  };
  public changeMeasurementCreteria(value): void {
    this.model.Gender = value;
    this.GenderTitle = this.MeasurementCreteriaData.find(x => x.value == value).text;
  };
  public fromDateTimeChange(value): void {
    this.model.Birthdate = value;
  };
  public getCitys(cityId) {
    this.http.post("address/getcitylist", {
      Id: cityId
    }).subscribe(x => {
      if (x.Code == 200) {
        let list = <Array<any>>x.Result;
        this.Citys = list.map(val => <Iitems>{
          Id: val.Id,
          Name: val.Title
        });
      }
    });
  };
  public selectState(stateId) {
    if (this.model.StateId && this.model.StateId !== this.label.StateId) {
      return this.States.find(x => x.Id == stateId).Name;
    }
  };
  public selectCity(cityId) {
    
    if (this.model.CityId && this.model.CityId !== this.label.CityId) {
      return this.States.find(x => x.Id == cityId).Name;
    }
  };
  public modelChange(e) {
    this.RoleIds = [];
    this.RoleIds = e;
  };
  public picChanged(e: string) {
    if (e) {
      this.PicName = e;
      this.pic64 = e.replace('data:image/jpeg;base64,', '');
    }
  };
  public onSubmit(e) {
    this.toast.Wait();
    if (e.form.valid) {
      this.http.post("account/registeruser", this.model).subscribe(x => {
        if (x.Code === 200) {
          debugger
          this.http.post("account/SetUserRoles", {
            UserId: this.model.Id,
            RolesId: this.RoleIds
          }).subscribe(x => {
            if (x.Code === 200) {
              if (this.pic64) {
                this.http.post("account/UploadProfileImage", {
                  UserId: this.model.Id,
                  Base64Image: this.pic64
                }).subscribe(x => {
                  if (x.Code === 200) {
                    this.toast.Success();
                    this.router.navigate(['/users']);
                  } else {
                    this.toast.Error();
                  }
                }, err => { }, () => this.toast.ClearWaiting())
              } else {
                this.toast.Success();
                this.router.navigate(['/users']);
              }
            } else {
              this.toast.Error();
              // this.toast.Error({
              //   msg: x.Message,
              //   title: 'پیغام'
              // })
            }
          }, err => { }, () => this.toast.ClearWaiting())
        } else {
          if(x.Code === 103){
            this.toast.Error({
              msg: "یوزرنیم تکراری",
              title: 'پیغام'
            })
          }else{
            this.toast.Error({
              msg: "ایمیل تکراری",
              title: 'پیغام'
            })
          } 
        }
      }, err => { }, () => this.toast.ClearWaiting());
    }
  };
}


