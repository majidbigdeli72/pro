export class Iusers {

    constructor(
        Id?: number,
        Email?: string,
        NationalCode?: string,
        Gender?: string,
        ConfirmPassword?: string,
        LastName?: string,
        Password?: number,
        FirstName?: string,
        Birthdate?: string,
        UserName?: string,
        Description?: string,
        StateId?: number,
        CityId?: number,
        PhoneNumber?: string,
        Base64Image?: string,
        RoleId?: any
    ) {
        this.Id = Id;
        this.Email = Email;
        this.NationalCode = NationalCode;
        this.Gender = Gender;
        this.ConfirmPassword = ConfirmPassword;
        this.LastName = LastName;
        this.Password = Password;
        this.FirstName = FirstName;
        this.Birthdate = Birthdate;
        this.UserName = UserName;
        this.Description = Description;
        this.StateId = StateId;
        this.CityId = CityId;
        this.PhoneNumber = PhoneNumber;
        this.Base64Image = Base64Image;
        this.RoleId = RoleId

    }
    public Id: any;
    public Email: string;
    public NationalCode: string;
    public Gender: string;
    public ConfirmPassword: string;
    public LastName: string;
    public Password: any;
    public FirstName: string;
    public Birthdate: string;
    public UserName: string;
    public Description: string;
    public Base64Image: string;
    public StateId: any;
    public CityId: any;
    public PhoneNumber: string;
    public RoleId: any
}
export class Ilabel {
    public getLabel(): Iusers {
        let label: Iusers = new Iusers();
        label.Id = "شماره";
        label.Email = "ایمیل";
        label.NationalCode = "کد ملی";
        label.Description = "توضیحات";
        label.Gender = "جنسیت";
        label.ConfirmPassword = "تکرار پسورد";
        label.LastName = "نام خانوادگی";
        label.Password = "پسورد";
        label.FirstName = "نام";
        label.Birthdate = "تاریخ تولد";
        label.UserName = "نام کاربری";
        label.Description = "توضیحات";
        //label.StateId = "منطقه";
        label.CityId = "شهر";
        label.PhoneNumber = "شماره تلفن";
        label.CityId = "شهر";
        label.StateId = "استان";
        label.Base64Image = "تصویر";
        label.RoleId = "نقش ها"
        return label;
    }
}

export interface iGender {
    value?: string;
    text: string;
    name: string;
}

export class Genders {
    public getGenders(): Array<iGender> {
        return [{
            value: "M",
            name: "men",
            text: "مرد",
        }, {
            value: "F",
            name: "women",
            text: "زن",
        }];
    }
}