
import { Component, OnInit } from '@angular/core';
import { Iusers, Ilabel } from '../Iusers'
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { Subject } from 'rxjs/Subject';
import { debounceTime, distinctUntilChanged, flatMap, switchMap } from 'rxjs/operators';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';

@Component({
  selector: 'basketik-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {
  public label: Iusers;
  public fieldSearch: string = "UserName";
  public url: string = 'account/Getlist';
  public columns: Array<any>;
  public config: any = {};
  public body: any = {
    Email: "",
    Gender: "",
    LastName: "",
    FirstName: "",
    UserName: "",
    RegionId: 0,
    CityId: 0,
    PhoneNumberConfirmed: true,
    PageIndex: 0,
    PageSize: 10,
    OrderByCol: "RegDate",
    OrderByDir: "ASC",
  }

  constructor(
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService) {
    this.label = new Ilabel().getLabel();
    this.columns = [
      { title: this.label.UserName, name: 'UserName', sort: "", filtering: { filterString: '', placeholder: 'جستجو در عنوان' } },
      {
        title: this.label.FirstName,
        name: 'FirstName',
        sort: true
      },
      { title: this.label.LastName, name: 'LastName', sort: true },
      { title: "وضعیت", name: 'PhoneNumberConfirmed', sort: true , iconStatus:true },
      { title: 'عملیات', name: 'Action', sort: false },
    ];  }

  ngOnInit() { }

  public onRemoveClicked(id) {
    this.toast.Wait();
    this.http.post('account/remove', {
      Id: id
    }).subscribe(status => {
      this.toast.Success({
        msg: 'عملیات با موفقیت انجام شد',
        title: 'حذف'
      });
    }, errr => {  }, () => this.toast.ClearWaiting());
  }

  public edit(id) {
    this.router.navigate([id, 'edit'], { relativeTo: this.route });
  }

}
