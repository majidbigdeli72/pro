import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from "../users/add/add.component";
import { EditComponent } from "../users/edit/edit.component";
import { ListComponent } from "../users/list/list.component";
import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
const routes: Routes = [
  {
    path: 'users',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'add',
        component: AddComponent
      },
      {
        path: ':id/edit',
        component: EditComponent
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
