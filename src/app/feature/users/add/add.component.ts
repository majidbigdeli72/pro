import { Component, OnInit } from '@angular/core';
import { Ilabel, Iusers, Genders, iGender } from "../Iusers";
import { selectModel, inputModel } from '../../../shared/tags/input-model';
import { text, lifecycle } from '@angular/core/src/render3/instructions';
import { Input } from '@angular/core/src/metadata/directives';
import * as momentJalaali from "moment-jalaali";
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { ToastService } from '../../../core/toast/toast.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// interface IKeys { id: number, name: string };
interface Iitems { Id: number, Name: string };


@Component({
  selector: 'basketik-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.less']
})
export class AddComponent implements OnInit {
  public RolesSelect: Array<any>
  public RoleIds: Array<any>;
  public showTab: Array<boolean> = [false, true, true];
  public model: Iusers;
  public mobileValidation : "/(\+98|0)?9\d{9}$/"
  public MeasurementCreteriaData: Array<iGender>;
  public label: Iusers = new Iusers();
  public GenderTitle: string;
  public Birthdate: string;
  public States: Array<{ Id: number, Name: string }>;
  public items: Array<{ Id: number, Name: string }>
  public Citys: Array<{ Id: number, Name: string }>;
  public Base64Image: any;
  public MeasurementCreteriaId;
  public disabled;
  public pic64: any;
  public PicName: string
  public stringFormTitle = "اطلاعات پایه";
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastService,
    private http: HttpServiceService,
    private authService: AuthService,
    private baseUrl: AppConfigService) {
    this.MeasurementCreteriaData = new Genders().getGenders();
    this.label = new Ilabel().getLabel();
    this.model = new Iusers();
  }
  ngOnInit() {
    this.http.post("address/getstatelist", {
      Id: 1
    }).subscribe(x => {
      if (x.Code == 200) {
        let list = <Array<any>>x.Result;
        this.States = list.map(val => <Iitems>{
          Id: val.Id,
          Name: val.Title
        });
      }
    });
    this.http.post("account/GetRoleList", {
      Title: "",
      OrderByCol: "Id",
      OrderByDir: "DESC",
      PageIndex: 0,
      PageSize: 10000
    }).subscribe(x => {
      if (x.Code === 200) {
        let list = <Array<any>>x.Result.List;
        this.items = list.map(val => <Iitems>{
          Id: val.Id,
          Name: val.Name
        });
      }
    });
  };
  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
    if (index === 2) {
      this.RolesSelect = [];
      for (let i = 0; i < this.RoleIds.length; i++) {
        const RoleId = this.RoleIds[i];
        var RoleNames = this.items.find(x => x.Id === RoleId).Name;
        this.RolesSelect.push(RoleNames);
      }
    }
  };
  public changeMeasurementCreteria(value): void {
    this.model.Gender = value;
    this.GenderTitle = this.MeasurementCreteriaData.find(x => x.value == value).text;
  };
  public fromDateTimeChange(value): void {
    debugger
    this.model.Birthdate = value;
  };
  public getCitys(StateId) {
    this.http.post("address/getcitylist", {
      Id: StateId
    }).subscribe(x => {
      if (x.Code == 200) {
        let list = <Array<any>>x.Result;
        this.Citys = list.map(val => <Iitems>{
          Id: val.Id,
          Name: val.Title
        });
      }
    });
  };
  public selectState(stateId) {
    if (this.model.StateId && this.model.StateId !== this.label.StateId) {
      return this.States.find(x => x.Id == stateId).Name;
    }
  };
  public selectCity(cityId) {
    if (this.model.CityId && this.model.CityId !== this.label.CityId) {
      return this.States.find(x => x.Id == cityId).Name;
    }
  };
  public modelChange(e) {
    this.RoleIds = e;
  };
  public onSubmit(e) {
    this.toast.Wait();
    if (e.form.valid) {
      this.http.post("account/registeruser", this.model).subscribe(x => {
        if (x.Code === 200) {
          var UserID = x.Result.Id;
          this.http.post("account/SetUserRoles", {
            UserId: UserID,
            RolesId: this.RoleIds
          }).subscribe(x => {
            if (x.Code === 200) {
              if (this.pic64) {
                this.http.post("account/UploadProfileImage", {
                  UserId: UserID,
                  Base64Image: this.pic64
                }).subscribe(x => {
                  if (x.Code === 200) {
                    this.toast.Success();
                    this.router.navigate(['/users']);
                  } else {
                    this.toast.Error();
                  }
                }, err => { }, () => this.toast.ClearWaiting())
              } else {
                this.toast.Success();
                this.router.navigate(['/users']);
              }
            } else {
              this.toast.Error();
              // this.toast.Error({
              //   msg: x.Message,
              //   title: 'پیغام'
              // })             
            }
          }, err => { }, () => this.toast.ClearWaiting())
        } else {
          if(x.Code === 103){
            this.toast.Error({
              msg: "یوزرنیم تکراری",
              title: 'پیغام'
            })
          }else{
            this.toast.Error({
              msg: "ایمیل تکراری",
              title: 'پیغام'
            })
          }        
        }
      }, err => {

      }, () => this.toast.ClearWaiting());
    }
  };
  public picChanged(e) {
    this.PicName = e;
    this.pic64 = e.replace('data:image/jpeg;base64,', '');
  };
}