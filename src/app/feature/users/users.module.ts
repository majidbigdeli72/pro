import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { UsersRoutingModule } from './users-routing';
import { AddComponent } from '../users/add/add.component';
import { ListComponent } from './list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from '../../shared/shared.module';
import { Ng2TableModule } from '../../shared/table/ng-table-module';
import { PaginationModule } from '../../shared/pagination';
@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    UsersRoutingModule,
    HttpClientModule,
    Ng2TableModule,
    PaginationModule.forRoot(),
    FormsModule
  ],
  declarations: [AddComponent, ListComponent, EditComponent]
})
export class UsersModule { }
