import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from "../supplier/add/add.component";
import { ListComponent } from "../supplier/list/list.component";
import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
import { EditComponent } from "../supplier/edit/edit.component";
const routes: Routes = [
  {
    path: 'supplier',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'add',
        component: AddComponent,
      },
      {
        path: ':id/edit',
        component: EditComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplierRoutingModule { }
