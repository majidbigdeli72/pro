import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentFactory, ComponentRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ISupplierHolidays } from '../isupplier';
import { ValueDateComponent } from './value-date/value-date.component';

@Component({
  selector: 'basketik-days-holiday',
  templateUrl: './days-holiday.component.html',
  styleUrls: ['./days-holiday.component.less']
})
export class DaysHolidayComponent implements OnInit, OnChanges {
  ngOnChanges(changes: SimpleChanges): void {
    if (changes._model && changes._model.currentValue)
      this._model = changes._model.currentValue;
  }
  @Input('model') _model: Array<ISupplierHolidays>;

  public workHolidays: Array<ISupplierHolidays>;
  private components: Array<any>;

  @ViewChild('container', { read: ViewContainerRef }) public container: ViewContainerRef;

  constructor(private factoryResolver: ComponentFactoryResolver) {
    this.components = [];
  }

  ngOnInit() {
    this._model && this._model.forEach(x => this.holidayCreate(x));
  }

  public holidayCreate(value?) {
    const factory: ComponentFactory<ValueDateComponent> = this.factoryResolver.resolveComponentFactory(ValueDateComponent);
    let componentRef: ComponentRef<ValueDateComponent> = this.container.createComponent(factory);
    componentRef.instance.hashId = Math.random();
    componentRef.instance.DateHoliday = value ? value.Date : undefined;
    componentRef.instance.removeDate.subscribe(x => this.remove(componentRef.instance.hashId));
    this.components.push(componentRef);
  }

  public remove(e) {
    const component = this.components.find((component) => component.instance.hashId == e);
    const componentIndex = this.components.indexOf(component);

    if (componentIndex !== -1) {
      this.container.remove(this.container.indexOf(component));
      this.components.splice(componentIndex, 1);
    }
  }

  public GetHolidays(): Array<ISupplierHolidays> {
    let _: Array<ISupplierHolidays> = [];
    this.components.forEach(x => _.push({
      Date: x.instance.DateHoliday
    }));
    return _;
  }
}
