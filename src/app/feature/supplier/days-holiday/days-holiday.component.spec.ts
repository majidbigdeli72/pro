import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaysHolidayComponent } from './days-holiday.component';

describe('DaysHolidayComponent', () => {
  let component: DaysHolidayComponent;
  let fixture: ComponentFixture<DaysHolidayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaysHolidayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaysHolidayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
