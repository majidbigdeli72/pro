import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'basketik-value-date',
  templateUrl: './value-date.component.html',
  styleUrls: ['./value-date.component.less']
})
export class ValueDateComponent implements OnInit {
  public hashId: number;
  public DateHoliday: any;
  public disabled: boolean = false;
  
  @Output('removeDate') public removeDate: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() { }

  public fromDateTimeChange(e) {
    this.DateHoliday = e;
  }
}
