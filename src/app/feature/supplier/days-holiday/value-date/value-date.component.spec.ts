import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueDateComponent } from './value-date.component';

describe('ValueDateComponent', () => {
  let component: ValueDateComponent;
  let fixture: ComponentFixture<ValueDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValueDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
