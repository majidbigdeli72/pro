import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { Isuppliers, Ilabel, ISupplierHolidays, GetListWeek, IKeys, ISupplierSupervisors } from "../isupplier";
import { NgForm, NgModel } from "@angular/forms";
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { MouseEvent } from '@agm/core';
import { DaysWeekComponent } from '../days-week/days-week.component';
import { DaysHolidayComponent } from '../days-holiday/days-holiday.component';
import { ToastService } from '../../../core/toast/toast.service';
import { Router } from '@angular/router';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';
import { DeliveryManSupplierComponent } from '../delivery-man-supplier/delivery-man-supplier.component';
import { SuperVisorComponent } from '../super-visor/super-visor.component';

@Component({
  selector: 'basketik-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.less']
})

export class AddComponent implements OnInit, AfterContentInit {

  @ViewChild(DaysWeekComponent) private _serviceTimes: DaysWeekComponent;
  @ViewChild(DaysHolidayComponent) private _holidays: DaysHolidayComponent;
  @ViewChild(DeliveryManSupplierComponent) private _deliveryMen: DeliveryManSupplierComponent;
  @ViewChild(SuperVisorComponent) private _superVisor: SuperVisorComponent;

  public lat: number;
  public lng: number;
  public zoom: number;
  public DaysOfWeek: Array<string>;
  public stringBaseInformation: string = "اطلاعات پایه";
  public model: Isuppliers = new Isuppliers();
  public label: Isuppliers = new Isuppliers();
  public stringFormTitle: string = "اطلاعات پایه";
  public States: Array<{ id: number, name: string }>;
  public Cities: Array<{ id: number, name: string }>;
  public showTab: Array<boolean> = [false, true, true, true, true];
  public urlPic: string;
  public urlPicMap: string;
  public ListDeliveryMans: Array<any>;
  public ListSuperVisors: Array<any>;
  public CountDescription: number;
  constructor(
    private dataService: SetTitleNavbarService,
    private http: HttpServiceService,
    private toast: ToastService,
    private router: Router,
    private authService: AuthService,
    private baseUrl: AppConfigService) {
    this.ListDeliveryMans = [];
    this.ListSuperVisors = [];
    dataService.updatedDataSelection('افزودن تامین کننده جدید');
  }

  ngAfterContentInit(): void {
    this.CountDescription = 0;
  }
  ngOnInit() {
    this.DaysOfWeek = new GetListWeek().GetListWeek();
    this.lat = 35.689670;
    this.lng = 51.389151;
    this.zoom = 10
    this.model.RegById = this.authService.getUserId();
    this.label = new Ilabel().getLabel();
    this.urlPic = this.baseUrl.configuration.PicUrl;
    this.http.post("address/getstatelist", {
      Id: 1
    }).subscribe(x => {
      if (x.Code == 200) {
        let list = <Array<any>>x.Result;
        this.States = list.map(val => <IKeys>{
          id: val.Id,
          name: val.Title
        });
      }
    });
  }
  public getCitys(cityId) {
    this.http.post("address/getcitylist", {
      Id: cityId
    }).subscribe(x => {
      if (x.Code == 200) {
        let list = <Array<any>>x.Result;
        this.Cities = list.map(val => <IKeys>{
          id: val.Id,
          name: val.Title
        });
      }
    });
  }
  public selectCity() {
    this.model.CityTitle = this.Cities.find(x => x.id == this.model.CityId).name;
  }
  public selectState() {
    this.getCitys(this.model.StateId);
    this.model.StateTitle = this.States.find(x => x.id == this.model.StateId).name;
  }
  public mapClicked($event: MouseEvent) {
    this.model.Latitude = $event.coords.lat,
      this.model.Longitude = $event.coords.lng;
    this.http.get(`https://maps.googleapis.com/maps/api/staticmap?center=${$event.coords.lat},${$event.coords.lng}&zoom=13&size=300x200&maptype=roadmap&markers=color:red%7Clabel:H%7C35.6892,51.3890&key=AIzaSyBDD3xmueKEOFm81mVNU2Ty531IoHV-QGw`).subscribe(x => {
      this.urlPicMap = x;
    });
  }
  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
    if (index == 4)
      this.extendedModel();
  }

  public onSubmit(form: NgForm) {
    this.toast.Wait();
    if (form.valid) {
      this.http.post("supplier/Register", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/supplier']);
          this.toast.Success();
        }
        else { }
      }, err => { },
        () => this.toast.ClearWaiting());
    }
  }

  private extendedModel() {
    this.model.ServiceTimes = this._serviceTimes.GetServiceTime();
    this.model.SupplierHolidays = this._holidays.GetHolidays();
    this.model.SupplierDeliveryMen = this._deliveryMen.GetListDeliveryMen();
    this.model.SupplierSupervisors = this._superVisor.GetListSuperVisors();
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }

  public GetNameDelivery(e): string {
    const items = this.ListDeliveryMans.find(x => x.Id == e.DeliveryManId);
    return items ? `${items.FirstName} ${items.LastName}` : '';
  }

  public GetNameSuperVisor(e): string {
    const items = this.ListSuperVisors.find(x => x.Id == e.UserId);
    return items ? `${items.FirstName} ${items.LastName}` : '';
  }
}
