import { Component, OnInit, ViewChild } from '@angular/core';
import { DaysWeekComponent } from '../days-week/days-week.component';
import { DaysHolidayComponent } from '../days-holiday/days-holiday.component';
import { Isuppliers, GetListWeek, Ilabel, IKeys } from '../isupplier';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { ToastService } from '../../../core/toast/toast.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { MouseEvent } from '@agm/core';
import { NgForm, NgModel } from '@angular/forms';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';
import { DeliveryManSupplierComponent } from '../delivery-man-supplier/delivery-man-supplier.component';
import { SuperVisorComponent } from '../super-visor/super-visor.component';


@Component({
  selector: 'basketik-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit {
  @ViewChild(DaysWeekComponent) private _serviceTimes: DaysWeekComponent;
  @ViewChild(DaysHolidayComponent) private _holidays: DaysHolidayComponent;
  @ViewChild(DeliveryManSupplierComponent) private _deliveryMen: DeliveryManSupplierComponent;
  @ViewChild(SuperVisorComponent) private _superVisor: SuperVisorComponent;
  
  public lat: number;
  public lng: number;
  public zoom: number;
  public DaysOfWeek: Array<string>;
  public stringBaseInformation: string = "اطلاعات پایه";
  public model: Isuppliers = new Isuppliers();
  public label: Isuppliers;
  public stringFormTitle: string = "اطلاعات پایه";
  public States: Array<{ id: number, name: string }>;
  public Cities: Array<{ id: number, name: string }>;
  public showTab: Array<boolean> = [false, true, true, true, true];
  public urlPic: string;
  public urlPicMap: string;
  public CountDescription: number;
  public ListDeliveryMans: Array<any>;
  public ListSuperVisors: Array<any>;
  private id: number;

  constructor(
    private dataService: SetTitleNavbarService,
    private route: ActivatedRoute,
    private http: HttpServiceService,
    private toast: ToastService,
    private router: Router,
    private authService: AuthService,
    private baseUrl: AppConfigService) {
    this.ListDeliveryMans = [];
    this.ListSuperVisors = [];
  }

  ngAfterContentInit(): void {
    this.CountDescription = 0;
  }
  ngOnInit() {
    this.DaysOfWeek = new GetListWeek().GetListWeek();
    // this.lat = 35.689670;
    // this.lng = 51.389151;
    this.zoom = 10
    this.label = new Ilabel().getLabel();
    this.urlPic = this.baseUrl.configuration.PicUrl;

    this.urlPic = this.baseUrl.configuration.PicUrl;
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.http.post('supplier/get', {
        Id: this.id
      }).subscribe(x => {
        this.model = x.Result;
        this.dataService.updatedDataSelection(`ویرایش ${this.model.Title}`);
        this.CountDescription = this.model.Description ? this.model.Description.length : 0;
        this.model.PicName = this.model.PicName.replace(new RegExp(this.baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');

        this.lat = this.model.Latitude ? this.model.Latitude : 35.689670;
        this.lng = this.model.Longitude ? this.model.Longitude : 51.389151;
        console.log(this.model)
        this.http.post("address/getstatelist", {
          Id: 1
        }).subscribe(x => {
          if (x.Code == 200) {
            let list = <Array<any>>x.Result;
            this.States = list.map(val => <IKeys>{
              id: val.Id,
              name: val.Title
            });
            this.getCitys(this.model.CityId);
          }
        });
      })
    });
  }

  public getCitys(cityId) {
    this.http.post("address/getcitylist", {
      Id: cityId
    }).subscribe(x => {
      if (x.Code == 200) {
        let list = <Array<any>>x.Result;
        this.Cities = list.map(val => <IKeys>{
          id: val.Id,
          name: val.Title
        });
      }
    });
  }
  public selectCity() {
    this.model.CityTitle = this.Cities.find(x => x.id == this.model.CityId).name;
  }
  public selectState() {
    this.getCitys(this.model.StateId);
    this.model.StateTitle = this.States.find(x => x.id == this.model.StateId).name;
  }
  public mapClicked($event: MouseEvent) {
    this.model.Latitude = $event.coords.lat,
      this.model.Longitude = $event.coords.lng;
    this.http.get(`https://maps.googleapis.com/maps/api/staticmap?center=${$event.coords.lat},${$event.coords.lng}&zoom=13&size=300x200&maptype=roadmap&markers=color:red%7Clabel:H%7C35.6892,51.3890&key=AIzaSyBDD3xmueKEOFm81mVNU2Ty531IoHV-QGw`).subscribe(x => {
      this.urlPicMap = x;
    });
  }
  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
    if (index == 4)
      this.extendedModel();
  }

  public onSubmit(form: NgForm) {
    this.toast.Wait();
    if (form.valid) {
      this.http.post("supplier/Update", this.model).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/supplier']);
          this.toast.Success();
        }
        else { }
      }, err => { },
        () => this.toast.ClearWaiting());
    }
  }

  private extendedModel() {
    this.model.ServiceTimes = this._serviceTimes.GetServiceTime();
    this.model.SupplierHolidays = this._holidays.GetHolidays();
    this.model.SupplierDeliveryMen = this._deliveryMen.GetListDeliveryMen();
    this.model.SupplierSupervisors = this._superVisor.GetListSuperVisors();
  }

  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }

  public GetNameDelivery(e): string {
    const items = this.ListDeliveryMans.find(x => x.Id == e.DeliveryManId);
    return items ? `${items.FirstName} ${items.LastName}` : '';
  }
  public GetNameSuperVisor(e): string {
    const items = this.ListSuperVisors.find(x => x.Id == e.UserId);
    return items ? `${items.FirstName} ${items.LastName}` : '';
  }
}
