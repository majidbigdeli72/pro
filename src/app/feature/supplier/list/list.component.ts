import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { Isuppliers, Ilabel } from "../isupplier";
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {


  public label: Isuppliers;
  public url: string = 'supplier/getlist';
  public columns: Array<any>;
  public config: any = {};
  public body: any = {
    Title: "",
    OrderByCol: "Id",
    OrderByDir: "DESC",
    CityId: 0,
    StateId: 0,
    RegionId: 0,
    DistrictId: 0,
    PageIndex: 0,
    PageSize: 10
  }
  constructor(
    private dataService:SetTitleNavbarService,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService) {
      dataService.updatedDataSelection('لیست تامین کنندگان ');
    this.label = new Ilabel().getLabel();
    this.columns = [
      { title: this.label.Id, name: 'Id', sort: true },
      {
        title: this.label.Title,
        name: 'Title',
        sort: true,
      },
      { title: this.label.Description, name: 'Description', sort: false },
      { title: this.label.PicName, name: 'Image', sort: false, pic: true },
      { title: 'عملیات', name: 'Action', sort: false },
    ];
  }
  ngOnInit() { }
  public onRemoveClicked(id) {
    this.toast.Wait();
    this.http.post('supplier/remove', {
      Id: id
    }).subscribe(status => {
      this.toast.Success({
        msg: 'عملیات با موفقیت انجام شد',
        title: 'حذف'
      });
    }, errr => { }, () => this.toast.ClearWaiting());
  }
  public onEditClicked(e) {
    this.router.navigate([e.id, 'edit'], { relativeTo: this.route });
  }
}
