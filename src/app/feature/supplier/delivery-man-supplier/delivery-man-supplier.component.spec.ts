import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryManSupplierComponent } from './delivery-man-supplier.component';

describe('DeliveryManSupplierComponent', () => {
  let component: DeliveryManSupplierComponent;
  let fixture: ComponentFixture<DeliveryManSupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryManSupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryManSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
