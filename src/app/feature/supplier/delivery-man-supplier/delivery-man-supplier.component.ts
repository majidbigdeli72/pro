import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { ISupplierDeliveryMen, ISupplierHolidays } from '../isupplier';
interface IItem { Id: string, Name: string };

@Component({
  selector: 'basketik-delivery-man-supplier',
  templateUrl: './delivery-man-supplier.component.html',
  styleUrls: ['./delivery-man-supplier.component.less']
})

export class DeliveryManSupplierComponent implements OnInit {
  @Input('model') _model: Array<ISupplierDeliveryMen>;

  public items: Array<IItem> = [];
  public model: Array<any>;
  private _list: Array<string> = [];

  @Output('ListOfDeliveryMen') private _listItems: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpServiceService) {
    this.model = [];
  }

  ngOnInit() {
    this.http.post("account/GetUsersByRole", {
      Name: "DeliveryMan"
    }).subscribe(x => {
      this._listItems.emit(x.Result);
      this.items = x.Result.map(c => <IItem>{
        Id: c.Id,
        Name: `${c.FirstName} ${c.LastName}`
      });

      if (this._model) {
        this._model.forEach(x => this.model.push(x.DeliveryManId));
      }
    });
  }

  public modelChange(e) {
    this._list = e;
  }

  public GetListDeliveryMen() {
    let _model: Array<ISupplierDeliveryMen> = [];
    this._list.forEach(x => _model.push({ DeliveryManId: x }));
    return _model;
  }
}
