import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { ISupplierSupervisors } from '../isupplier';

interface IItem { Id: string, Name: string };

@Component({
  selector: 'basketik-super-visor',
  templateUrl: './super-visor.component.html',
  styleUrls: ['./super-visor.component.less']
})
export class SuperVisorComponent implements OnInit {
  @Input('model') _model: Array<ISupplierSupervisors>;

  public items: Array<IItem> = [];
  public model: any;
  private _list: Array<string> = [];

  @Output('ListOfSuperVisor') private _listItems: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpServiceService) {
    //this.model = [];
  }

  ngOnInit() {
    this.http.post("account/GetUsersByRole",
      {
        Name: "Supervisor"
      }).subscribe(x => {
        this._listItems.emit(x.Result);
        this.items = x.Result.map(c => <IItem>{
          Id: c.Id,
          Name: `${c.FirstName} ${c.LastName}`
        });
        if (this._model) {
          // this._model.forEach(x => this.model.push(x.UserId));
          this.model = this._model[0].UserId;
        }
      });
  }

  public modelChange(e) {
    // this._list = e;
    this._list = [];
    this._list.push(e);
  }

  public GetListSuperVisors() {
    let _model: Array<ISupplierSupervisors> = [];
    this._list.forEach(x => _model.push({ UserId: x }));
    return _model;
  }
}
