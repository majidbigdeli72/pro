import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { SupplierRoutingModule } from "./supplier-routing.module";
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { Ng2TableModule } from '../../shared/table/ng-table-module';
import { AgmCoreModule } from '@agm/core';
import { DaysWeekComponent } from './days-week/days-week.component';
import { DaysComponent } from './days-week/days/days.component';
import { DaysHolidayComponent } from './days-holiday/days-holiday.component';
import { ValueDateComponent } from './days-holiday/value-date/value-date.component';
import { DeliveryManSupplierComponent } from './delivery-man-supplier/delivery-man-supplier.component';
import { SuperVisorComponent } from './super-visor/super-visor.component';


@NgModule({
  imports: [
    CommonModule,
    SupplierRoutingModule,
    FormsModule,
    Ng2TableModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBDD3xmueKEOFm81mVNU2Ty531IoHV-QGw'
    }),

  ],
  declarations: [
    AddComponent, 
    ListComponent, 
    EditComponent, 
    DaysWeekComponent, 
    DaysComponent, 
    DaysHolidayComponent, 
    ValueDateComponent, DeliveryManSupplierComponent, SuperVisorComponent
  ],
  entryComponents:[
    ValueDateComponent
  ]
})
export class SupplierModule { }
