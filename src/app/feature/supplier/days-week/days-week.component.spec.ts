import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaysWeekComponent } from './days-week.component';

describe('DaysWeekComponent', () => {
  let component: DaysWeekComponent;
  let fixture: ComponentFixture<DaysWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaysWeekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaysWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
