import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IServcieTime } from '../../isupplier';

@Component({
  selector: 'basketik-days',
  templateUrl: './days.component.html',
  styleUrls: ['./days.component.less']
})
export class DaysComponent implements OnInit, OnChanges {
  ngOnChanges(changes: SimpleChanges): void {
    if (changes._model && changes._model.currentValue) {
      const _m = <Array<IServcieTime>>changes._model.currentValue;
      const _r = _m.find(x => x.DayOfWeek == this._index);
      this.model = _r ? _r : new IServcieTime();
    }

    if (changes._index && changes._index.currentValue) {
      this._index = changes._index.currentValue;
    }
  }

  @Input('index') public _index: number;
  @Input('title') public _title: string;
  @Input('model') public _model: string;

  public placeholder: string = '00:00';
  public model: IServcieTime;
  public removeTime: boolean = true;

  constructor() {
    this.model = new IServcieTime();
  }

  ngOnInit() {
    this.model.DayOfWeek = this._index;
  }

  public onChangeFromTime(e) {
    this.model.DayOfWeek = this._index;    
    this.model.FromTime = e;
  }

  public onChangeToTime(e) {
    this.model.DayOfWeek = this._index;
    this.model.ToTime = e;
  }
  public removeWorkTime() {
    this.removeTime = false;
    this.model.FromTime = undefined;
    this.model.ToTime = undefined;
    setTimeout(() => this.removeTime = true, 1);
  }
}
