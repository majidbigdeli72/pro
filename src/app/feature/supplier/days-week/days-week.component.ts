import { Component, OnInit, ViewChildren, Input, OnChanges, SimpleChanges } from '@angular/core';
import { GetListWeek, IServcieTime } from '../isupplier';
import { DaysComponent } from './days/days.component';

@Component({
  selector: 'basketik-days-week',
  templateUrl: './days-week.component.html',
  styleUrls: ['./days-week.component.less']
})
export class DaysWeekComponent implements OnInit, OnChanges {
  @Input('model') _model: Array<any>;
  public DaysOfWeek: Array<string>;
  @ViewChildren(DaysComponent) private _days: Array<DaysComponent>;
  private _serviceTime: Array<IServcieTime>;

  constructor() {
    this._serviceTime = [];
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes._model && changes._model.currentValue)
      this._model = changes._model.currentValue;
  }

  ngOnInit() {
    this.DaysOfWeek = new GetListWeek().GetListWeek();
  }

  public GetServiceTime(): Array<IServcieTime> {
    this._serviceTime = [];
    this._days.forEach(x => {
      if (x.model.FromTime && x.model.ToTime)
        this._serviceTime.push({
          DayOfWeek: x.model.DayOfWeek,
          ToTime: x.model.ToTime,
          FromTime: x.model.FromTime
        })
    });
    return this._serviceTime;
  }
}
