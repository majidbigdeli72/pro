export class Isuppliers {
    constructor(
        public Id?: any,
        public StateId?: number,
        public StateTitle?: string,
        public CityTitle?: string,
        public CityId?: number,
        public RegionId?: number,
        public RegionTitle?: string,
        public DistrictId?: number,
        public DistrictTitle?: string,
        public Title?: string,
        public EnTitle?: string,
        public Owner?: string,
        public Description?: string,
        public PicName?: string,
        public ServiceRadius?: any,
        public Latitude?: any,
        public Longitude?: any,
        public RegById?: string,
        public SupplierServerBaseIp?: string,
        public ServiceTimes?: Array<IServcieTime>,
        public SupplierHolidays?: Array<ISupplierHolidays>,
        public SupplierDeliveryMen?: Array<ISupplierDeliveryMen>,
        public SupplierSupervisors?: Array<ISupplierSupervisors>
    ) { }
}

export class Ilabel {
    public getLabel(): Isuppliers {
        let label: Isuppliers = new Isuppliers();
        label.Id = "شماره";
        label.Title = "عنوان";
        label.EnTitle = "عنوان لاتین";
        label.Description = "توضیحات";
        label.PicName = "تصویر";
        label.Owner = "مالک";
        label.ServiceRadius = "شعاع";
        label.SupplierServerBaseIp = "آی پی سرور";
        label.StateTitle = "استان";
        label.CityTitle = "شهر";
        label.DistrictTitle = "محله";
        label.RegionTitle = "ناحیه";
        return label;
    }
}

export class IServcieTime {
    constructor(
        public DayOfWeek?: any,
        public FromTime?: String,
        public ToTime?: String) { }
}

export class ISupplierHolidays {
    //1/27/2018
    constructor(public Date?: any) { }
}

export class GetListWeek {
    public GetListWeek(): Array<string> {
        let model: Array<string> = [
            '','شنبه', 'یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنج شنبه', 'جمعه'
        ];
        return model;
    }
}

export class ISupplierDeliveryMen {
    constructor(public DeliveryManId?: string) { }
}
export interface IKeys { id: number, name: string };

export class ISupplierSupervisors {
    constructor(public UserId?: string) { }
}
