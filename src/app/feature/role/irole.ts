export class Iroles {
    constructor(
        RoleId?: any,
        RoleName?: string,
        ControllerActionList?: Array<ApiAcsess>,
        ActionDescription?: string,
    ) {
        this.RoleName = RoleName;
        this.RoleId = RoleId;
        this.ControllerActionList = ControllerActionList;
        this.ActionDescription = ActionDescription;
    }
    public RoleId: any;
    public RoleName: string;
    public ControllerActionList: Array<ApiAcsess>;
    public ActionDescription: string;
}


export interface ApiAcsess {
    ControllerName?: string;
    ActionName?: string;
    ActionTitle?: string;
    ControllerTitle?: string;
    ActionDescription?: string
}

export class Ilabel {
    public getLabel(): Iroles {
        let label: Iroles = new Iroles();
        label.RoleName = "نام";
        return label;
    }
}