import { Component, OnInit } from '@angular/core';
import { Ilabel, Iroles, ApiAcsess } from '../irole';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { AuthService } from '../../../core/user-service/auth.service';



@Component({
  selector: 'basketik-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.less']
})
export class AddComponent implements OnInit {
  public model: Iroles = new Iroles();
  public label: Iroles = new Iroles();
  public stringFormTitle: string = "اطلاعات پایه";
  public showTab: Array<boolean> = [false, true, true];
  public Controllers: Array<any>;
  public IsSelected: boolean = false;
  constructor(private http: HttpServiceService,
    private authService: AuthService,
    private toast: ToastService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.label = new Ilabel().getLabel();
    this.model.ControllerActionList = [];
    this.http.post("account/GetAllActions", {}).subscribe(x => {
      if (x.Code === 200) {
        this.Controllers = x.Result;
      }
    })
  }

  public show(index: number): void {
    this.showTab.forEach((x, i) => this.showTab[i] = true);
    this.showTab[index] = false;
  };

  public getActionController(value: ApiAcsess, event, action) {
    action.IsSelected = event
    if (event) {
      this.model.ControllerActionList.push(value);
    } else {
      var i = this.model.ControllerActionList.findIndex(item => item.ControllerName === value.ControllerName && item.ActionName === value.ActionName);
      this.model.ControllerActionList.splice(i, 1)
    }
  };
  public getControllers() {
    var Controllers = [];
    if (this.model.ControllerActionList) {
      this.model.ControllerActionList.forEach(item => {
        if (Controllers.findIndex(co => co.controllerName === item.ControllerName) === -1) {
          Controllers.push({ controllerName: item.ControllerName, controllerTitle: item.ControllerTitle });
        }
      });
    }
    return Controllers;
  };
  public getActionInController(controller) {
    return this.model.ControllerActionList.filter((item) => {
      return item.ControllerName === controller;
    });
  };
  public onSubmit(e) {
    this.toast.Wait();
    if (e.form.valid) {
      this.http.post("account/SetRoleAccess", this.model).subscribe(x => {
        if (x.Code === 200) {
          this.router.navigate(['/role']);
          this.toast.Success();
        }
        else
          this.toast.Error();
      }, err => { }, () => this.toast.ClearWaiting());
    }
  };
  public selectAll() {
    this.model.ControllerActionList = [];
    this.Controllers.forEach(i => {
      i.Actions.forEach(j => {
        j.IsSelected = true;
        var item: ApiAcsess = {};
        item.ControllerName = i.ControllerName;
        item.ControllerTitle = i.ControllerTitle;
        item.ActionName = j.ActionName;
        item.ActionDescription = j.ActionDescription;
        item.ActionTitle = j.ActionTitle;
        this.model.ControllerActionList.push(item);
      });
    });
  };

  public unselectAll() {
    this.model.ControllerActionList = [];
    this.Controllers.forEach(i => {
      i.Actions.forEach(j => {
        j.IsSelected = false;
      });
    });
  }


}
