import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { RoleRoutingModule } from "./role-routing";
import { SharedModule } from '../../shared/shared.module'


@NgModule({
  imports: [
    CommonModule,
    RoleRoutingModule,
    HttpClientModule,
    FormsModule,
    SharedModule
  ],
  declarations: [ListComponent, AddComponent, EditComponent]
})
export class RoleModule { }
