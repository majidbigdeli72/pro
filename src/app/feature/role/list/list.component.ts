import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { Ilabel, Iroles } from "../irole";
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
@Component({
  selector: 'basketik-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {
  public label: Iroles;
  public url: string = 'account/GetRoleList';
  public columns: Array<any>;
  public config: any = {};
  public refresh: boolean = true;
  public fieldSearch: string = "Name";
  public body: any = {
    Name: "",
    OrderByCol: "Id",
    OrderByDir: "DESC",
    PageIndex: 0,
    PageSize: 10
  }
  constructor(private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService) {
    this.label = new Ilabel().getLabel();
    this.columns = [
      {
        title: this.label.RoleName,
        name: 'Name',
        sort: true
      },
      { title: 'عملیات', name: 'Action', sort: false },
    ];
  }
  ngOnInit() { }
  public onRemoveClicked(id) {
    this.toast.Wait();
    this.http.post('account/RemvoeRoleById', {
      Id: id
    }).subscribe(status => {
      this.toast.Success();
    }, errr => {
      debugger
      this.toast.Error({
        msg: 'این نقش کاربر وابسته دارد',
        title: 'خطا'
      });
     }, () => this.toast.ClearWaiting());
    this.refresh = false;
    setTimeout(() => this.refresh = true, 1);
  }
  public onEditClicked(e) {
    this.router.navigate([e.id, e.data.Name, 'edit'], { relativeTo: this.route });
  }
}
