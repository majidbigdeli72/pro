import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from "../role/add/add.component";
import { ListComponent } from "../role/list/list.component";
import { EditComponent } from "../role/edit/edit.component";

import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
const routes: Routes = [
    {
        path: 'role',
        canActivate: [
            UserIsLoginGuard
        ],
        children: [
            {
                path: '',
                component: ListComponent,
            },
            {
                path: 'add',
                component: AddComponent
            },
            {
                path: ':id/:name/edit',
                component: EditComponent
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RoleRoutingModule { }
