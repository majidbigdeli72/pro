export class Iadvertisements {
    constructor(
        public Id?: any,
        public Title?: string,
        public PicName?: string,
        public FromDateTime?: string,
        public ToDateTime?: string,
        public ClientType?: string,
        public PositionId?: any,
        public CategoryId?: any,
        public SupplierId?: any,
        public Url?: string,
        public RegById?: any
    ) { }
}

export class Ilabel {
    public getLabel(): Iadvertisements {
        let label: Iadvertisements = new Iadvertisements();
        label.CategoryId = "دسته بندی";
        label.ClientType = "نوع";
        label.FromDateTime = "تاریخ و زمان شروع";
        label.Id = "شماره";
        label.PicName = "تصویر";
        label.PositionId = "موقعیت";
        label.SupplierId = "تامین کننده";
        label.Title = "عنوان";
        label.ToDateTime = "تاریخ و زمان پایان";
        label.Url = "لینک";
        return label;
    }
}

export interface IClientType {
    Id?: number,
    Type?: string,
    Title?: string
}

export class ClientTypeList {
    public ListClientType(): Array<IClientType> {
        return [
            {
                Id: 1,
                Type: 'W',
                Title: 'وب'
            },
            {
                Id: 2,
                Type: 'M',
                Title: 'موبایل'
            },
            {
                Id: 3,
                Type: 'A',
                Title: 'Android'
            },
            {
                Id: 4,
                Type: 'I',
                Title: 'IOS'
            }
        ]
    }
}

export interface IPosition{
    Id?: number;
    Idp?: number;
    Position?: number,
    Title?: string;
}

export class PositionList{
    public GetListPosition() : Array<IPosition>{
        return [
            {
              Id: 1,
              Idp: 1,
              Position: 1,
              Title: 'صفحه اصلی - بالای صفحه'
            },
            {
              Id: 2,
              Idp: 1,
              Position: 2,
              Title: 'صفحه اصلی - کنار صفحه'
            },
            {
              Id: 3,
              Idp: 1,
              Position: 3,
              Title: 'دسته بندی - بالای صفحه'
            },
            {
              Id: 4,
              Idp: 1,
              Position: 4,
              Title: 'دسته بندی - کنار صفحه'
            },
      
      
            {
              Id: 5,
              Idp: 2,
              Position: 1,
              Title: 'صفحه اصلی - بالای صفحه'
            },
            {
              Id: 6,
              Idp: 2,
              Position: 2,
              Title: 'صفحه اصلی - پایین صفحه'
            },
            {
              Id: 7,
              Idp: 2,
              Position: 3,
              Title: 'دسته بندی - بالای صفحه'
            },
      
      
            {
              Id: 8,
              Idp: 3,
              Position: 1,
              Title: 'صفحه اصلی - بالای صفحه'
            },
            {
              Id: 9,
              Idp: 3,
              Position: 2,
              Title: 'صفحه اصلی - پایین صفحه'
            },
            {
              Id: 10,
              Idp: 3,
              Position: 3,
              Title: 'دسته بندی - بالای صفحه'
            },
      
      
            {
              Id: 11,
              Idp: 4,
              Position: 1,
              Title: 'صفحه اصلی - بالای صفحه'
            },
            {
              Id: 12,
              Idp: 4,
              Position: 2,
              Title: 'صفحه اصلی - پایین صفحه'
            },
            {
              Id: 13,
              Idp: 4,
              Position: 3,
              Title: 'دسته بندی - بالای صفحه'
            }
          ];
    }
}

export interface IKeys { Id: any, Name: string };
