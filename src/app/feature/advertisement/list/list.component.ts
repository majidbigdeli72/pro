import { Component, OnInit } from '@angular/core';
import { Ilabel, Iadvertisements } from '../iadvertisement';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../core/toast/toast.service';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {

  public label: Iadvertisements;
  public url: string = 'advlogo/GetList';
  public columns: Array<any>;
  public config: any = {};
  public refresh: boolean = true;
  public body: any = {
    SupplierId: 0,
    CategoryId: 0,
    ClientType: "",
    PositionId: 0,
    PageIndex: 0,
    PageSize: 10,
    OrderByCol: "RegDate",
    OrderByDir: "DESC"
  }
  constructor(
    private dataService: SetTitleNavbarService,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastService) {
    dataService.updatedDataSelection('لیست تبلیغات');
    this.label = new Ilabel().getLabel();
    this.columns = [
      { title: this.label.Id, name: 'Id', sort: true },
      { title: this.label.Title, name: 'Title', sort: true },
      { title: this.label.SupplierId, name: 'SupplierTitle', sort: true },
      { title: this.label.CategoryId, name: 'CategoryTitle', sort: true },
      { title: this.label.PicName, name: 'PicName', sort: false, pic: true },
      { title: this.label.Url, name: 'Url', sort: false },
      { title: 'عملیات', name: 'Action', sort: false },
    ];
  }
  ngOnInit() { }
  public onRemoveClicked(id) {
    this.toast.Wait();
    this.http.post('advlogo/Remove', {
      Id: id
    }).subscribe(status => {
      this.toast.Success({
        msg: 'عملیات با موفقیت انجام شد',
        title: 'حذف'
      });
      
      this.refresh = false;
      setTimeout(() => this.refresh = true, 1);
    }, errr => {
      this.toast.Error();
    }, () => this.toast.ClearWaiting());
  }
  public onEditClicked(e) {
    this.router.navigate([e.id, 'edit'], { relativeTo: this.route });
  }

}
