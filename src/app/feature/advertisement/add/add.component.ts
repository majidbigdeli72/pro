import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Iadvertisements, Ilabel, ClientTypeList, PositionList, IKeys, IClientType, IPosition } from "../iadvertisement";
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ToastService } from '../../../core/toast/toast.service';
import { Router } from '@angular/router';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.less']
})

export class AddComponent implements OnInit {
  public data: Iadvertisements = new Iadvertisements();
  public label: Iadvertisements = new Iadvertisements();
  public showTab: boolean = true;
  public stringFormTitle: string = "اطلاعات پایه";
  public urlPic: string = this.baseUrl.configuration.PicUrl;
  public clienttypes: Array<IClientType>;
  public positionids: Array<IPosition>;
  public suppliers: Array<any>;
  public categories: Array<any>;

  constructor(
    private dataService: SetTitleNavbarService,
    private toast: ToastService,
    private http: HttpServiceService,
    private router: Router,
    private authService: AuthService,
    private baseUrl: AppConfigService) {
    dataService.updatedDataSelection('افزودن تبلیغ جدید');
    this.clienttypes = new ClientTypeList().ListClientType();
  };

  ngOnInit() {
    this.label = new Ilabel().getLabel();
    this.http.post("supplier/getlist", {
      Title: "",
      OrderByCol: "RegDate",
      OrderByDir: "DESC",
      CityId: 0,
      StateId: 0,
      RegionId: 0,
      DistrictId: 0,
      PageIndex: 0,
      PageSize: 100000
    }).subscribe(x => {
      const result = <Array<any>>x.Result.List;
      this.suppliers = result.map(z => <IKeys>{
        Id: z.Id,
        Name: z.Title
      });
      let all: IKeys = {
        Id: 0,
        Name: "همه"
      };
      this.suppliers.push(all);
      this.suppliers.sort((a, b) => a.Id - b.Id);
    });

    this.http.post("category/getlist", {
      Title: "",
      PageIndex: 0,
      PageSize: 12
    }).subscribe(x => {
      const result = <Array<any>>x.Result.List;
      this.categories = result.map(z => <IKeys>{
        Id: z.Id,
        Name: z.Title
      });
      let all: IKeys = {
        Id: 0,
        Name: "همه"
      };
      this.categories.push(all);
      this.categories.sort((a, b) => a.Id - b.Id);
    });
  };

  public modelChangeSupplier(e) {
    this.data.SupplierId = e;
  }
  public modelChangeCategory(e) {
    this.data.CategoryId = e;
  }

  public SetClientType(e) {
    this.data.ClientType = e;
    this.positionids = new PositionList().GetListPosition().filter(x => x.Idp == this.clienttypes.find(z => z.Type == this.data.ClientType).Id);
    this.data.PositionId = this.positionids.find(x => x.Id == this.data.PositionId) ? this.data.PositionId : undefined;
  }

  public getPositionids(id) {
    let result = [];
    for (var i = 0; i < this.positionids.length; i++) {
      if (this.positionids[i].Idp == id) {
        result.push(this.positionids[i]);
      }
    }
    this.positionids = result;
  };
  public toDateTimeChange(e) {
    this.data.ToDateTime = e
  };
  public fromDateTimeChange(e) {
    this.data.FromDateTime = e
  };
  public toggleShowTab() {
    this.showTab = !this.showTab;
  };
  public getSupplier(e) {
    return this.suppliers.find(x => x.Id == e).Name;
  };
  public getCategory(e) {
    return this.categories.find(x => x.Id == e).Name;
  };
  public getClientType(e) {
    if (this.data.ClientType) {
      return this.clienttypes.find(x => x.Type == e).Title;
    }
  };
  public getPosition(e) {
    if (this.data.PositionId) {
      return this.positionids.find(x => x.Id == e).Title;
    }
  };
  public onSubmit(e) {
    this.data.RegById = this.authService.getUserId();
    this.toast.Wait();
    if (e.form.valid) {
      this.data.SupplierId = this.data.SupplierId == 0 ? null : this.data.SupplierId;
      this.data.CategoryId = this.data.CategoryId == 0 ? null : this.data.CategoryId;
      this.http.post("advlogo/Register", this.data).subscribe(x => {
        if (x.Code === 200) {
          this.router.navigate(['/advertisement']);
          this.toast.Success();
        } else if (x.Code == 101)
          this.toast.Error({
            msg: x.Message,
            title: 'اشکال در انجام عملیات'
          });
      }, err => { ; }, () => this.toast.ClearWaiting());
    };
  };
}