import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from "../advertisement/add/add.component";
import { ListComponent } from "../advertisement/list/list.component";
import { EditComponent } from "../advertisement/edit/edit.component";
import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
const routes: Routes = [
  {
    path: 'advertisement',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'add',
        component: AddComponent
      },
      {
        path: ':id/edit',
        component: EditComponent
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class advertisementRoutingModule { }
