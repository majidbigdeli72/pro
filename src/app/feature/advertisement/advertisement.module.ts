import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { SharedModule } from '../../shared/shared.module'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from "@angular/forms";
import { advertisementRoutingModule } from "./advertisement-routing";

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    advertisementRoutingModule
  ],
  declarations: [AddComponent, EditComponent, ListComponent]
})
export class AdvertisementModule { }
