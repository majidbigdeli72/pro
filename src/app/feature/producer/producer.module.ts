import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { ProducerRoutingModule } from "./producer-routing.module";
import { FormsModule } from '@angular/forms';
import { Ng2TableModule } from '../../shared/table/ng-table-module';
import { PaginationModule } from '../../shared/pagination';

@NgModule({
  imports: [
    CommonModule,
    ProducerRoutingModule,
    FormsModule,
    Ng2TableModule,
    PaginationModule
  ],
  declarations: [AddComponent, EditComponent, ListComponent]
})
export class ProducerModule { }
