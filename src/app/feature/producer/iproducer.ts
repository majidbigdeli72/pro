export class Iproducers {
    constructor(
        Id?: number,
        Title?: string,
        EnTitle?: string,
        Description?: string,
        PicName?: string,
        RegById?: string,
        ProducerId?: number) {
        this.Title = Title;
        this.Description = Description;
        this.PicName = PicName;
        this.ProducerId = ProducerId;
        this.RegById = RegById;
        this.EnTitle = EnTitle;
        this.Id = Id;
    }
    public Id: any;
    public EnTitle: string;
    public Title: string;
    public Description: string;
    public PicName: string;
    public RegById: string;
    public ProducerId: number;
    public ProducerTitle: string;
}

export class Ilabel {
    public getLabel(): Iproducers {
        let label: Iproducers = new Iproducers();
        label.Id = "شماره";
        label.Title = "عنوان";
        label.PicName = "تصویر";
        label.EnTitle = "عنوان لاتین";
        label.Description = "توضیحات";
        label.ProducerTitle = "تولیدکننده";
        return label;
    }
}