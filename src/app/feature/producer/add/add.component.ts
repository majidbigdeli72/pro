import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Iproducers, Ilabel } from "../iproducer";
import { NgForm, NgModel } from "@angular/forms";
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ToastService } from '../../../core/toast/toast.service';
import { Router } from '@angular/router';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.less']
})
export class AddComponent implements OnInit, AfterContentInit {
 
  public data: Iproducers = new Iproducers();
  public label: Iproducers = new Iproducers();
  public showTab: boolean = true;
  public urlPic: string = this.baseUrl.configuration.PicUrl;
  public stringFormTitle: string = "اطلاعات پایه";
  public Producers: Array<{ id: number, name: string }>;
  public CountDescription: number;

  constructor(
    private dataService:SetTitleNavbarService,
    private router: Router,
    private toast: ToastService,
    private http: HttpServiceService,
    private authService: AuthService,
    private baseUrl: AppConfigService) {
      dataService.updatedDataSelection('افزودن تولیدکننده جدید');
  }
  ngOnInit() {
    this.label = new Ilabel().getLabel();
  }

  ngAfterContentInit(): void {
    this.CountDescription = 0;
  }

  public toggleShowTab() {
    this.showTab = !this.showTab;
  }

  public onSubmit(e) {
    this.toast.Wait();
    this.data.RegById = this.authService.getUserId();
    if (e.form.valid) {
      this.http.post("producer/Register", this.data).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/producer']);
          this.toast.Success();
        }
        else
          this.toast.Error();
      }, err => { }, () => this.toast.ClearWaiting());
    }
  }
 public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
