import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from "../producer/add/add.component";
import { EditComponent } from "../producer/edit/edit.component";
import { ListComponent } from "../producer/list/list.component";

import { UserIsLoginGuard } from '../../core/user-service/user-is-login.guard';
const routes: Routes = [
  {
    path: 'producer',
    canActivate: [
      UserIsLoginGuard
    ],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'add',
        component: AddComponent
      },
      {
        path: ':id/edit',
        component: EditComponent
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProducerRoutingModule { }