import { Component, OnInit, AfterContentInit } from '@angular/core';
import { NgForm, NgModel } from "@angular/forms";
import { Iproducers, Ilabel } from "../iproducer";
import { IKeyedCollection, KeyedCollection } from '../../../core/dictionary';
import { HttpServiceService } from '../../../core/http-service/http-service.service';
import { AuthService } from '../../../core/user-service/auth.service';
import { AppConfigService } from '../../../core/app-config-setting-bootstrap/app-config-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
import { ToastService } from '../../../core/toast/toast.service';
import { SetTitleNavbarService } from '../../../core/navbar/set-title-navbar.service';

@Component({
  selector: 'basketik-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit, AfterContentInit {


  id: number;
  private sub: any;
  public label: Iproducers = new Iproducers();
  public data: Iproducers = new Iproducers();
  public proId: number;
  public showTab: boolean = true;
  public stringFormTitle: string = "اطلاعات پایه";
  public Producers: Array<{ id: number, name: string }>
  public CountDescription: number;
  public urlPic: string;
  constructor(
    private dataService:SetTitleNavbarService,
    private router: Router,
    private toast: ToastService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private http: HttpServiceService,
    private baseUrl: AppConfigService) { }

  ngOnInit() {
    this.label = new Ilabel().getLabel();
    interface IKeys { id: number, name: string };
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.http.post("producer/get", {
        "Id": this.id
      }).subscribe(x => {
        if (x.Code === 200) {
          this.data = x.Result;
          this.dataService.updatedDataSelection(`ویرایش ${this.data.Title}`);
          this.data.PicName = this.data.PicName.replace(new RegExp(this.baseUrl.configuration.RegexRemoveFromPic, 'gi'), '');
          this.data.Description && (this.CountDescription = this.data.Description.length);
        }
      });
    });
  }

  ngAfterContentInit(): void {
    this.urlPic = this.baseUrl.configuration.PicUrl;
    this.CountDescription = 0;
  }


  public toggleShowTab() {
    this.showTab = !this.showTab;
  }

  public onSubmit(e) {
    this.toast.Wait();
    this.data.RegById = this.authService.getUserId();
    if (e.form.valid) {
      this.http.post("producer/Update", this.data).subscribe(x => {
        if (x.Code == 200) {
          this.router.navigate(['/producer']);
          this.toast.Success();
        }
        else
          this.toast.Error();
      }, err => { }, () => this.toast.ClearWaiting());
    }
  }
  public CheckCount(e: NgModel) {
    this.CountDescription = e.value.length;
  }
}
