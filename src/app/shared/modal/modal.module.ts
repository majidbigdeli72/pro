import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import {PipesModule} from "../pipes/pipes.module"
@NgModule({
  imports: [
    CommonModule,
    PipesModule
  ],
  declarations: [
    ModalComponent
  ],
  exports: [
    ModalComponent
  ]
})
export class ModalModule {
}
