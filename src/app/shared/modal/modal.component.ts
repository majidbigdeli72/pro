import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'basketik-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less']
})
export class ModalComponent implements OnInit {
  @Input() isImage : boolean ; 
  @Input() public rmId: number;
  @Input('isModalRemove') public isRemove: boolean;
  @Input() public textModel: string;
  @Input() public imgModel: string;

  @Output() public close: EventEmitter<any> = new EventEmitter();
  @Output() public confirm: EventEmitter<any> = new EventEmitter<any>();

  newname = "";

  ngOnInit() { }

  ok() {
    this.confirm.emit();
  }

  cancel() {
    this.close.emit(null);
  }

}
