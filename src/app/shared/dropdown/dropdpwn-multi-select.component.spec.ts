import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdpwnMultiSelectComponent } from './dropdpwn-multi-select.component';

describe('DropdpwnMultiSelectComponent', () => {
  let component: DropdpwnMultiSelectComponent;
  let fixture: ComponentFixture<DropdpwnMultiSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdpwnMultiSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdpwnMultiSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
