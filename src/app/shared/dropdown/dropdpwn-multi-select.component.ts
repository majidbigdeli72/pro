import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'basketik-dropdpwn-multi-select',
  templateUrl: './dropdpwn-multi-select.component.html',
  styleUrls: ['./dropdpwn-multi-select.component.less']
})
export class DropdpwnMultiSelectComponent implements OnInit {

  @Input() items: Array<any>;
  @Input() model: any;
  @Output() modelCahnge: EventEmitter<any> = new EventEmitter<any>();
  @Input() isMulti: boolean = true;



  constructor() { }

  ngOnInit() {
  }

  emitmodel(e) {
    this.modelCahnge.emit(e);
  }

}
