import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultValueIfNull'
})
export class DefaultValueIfNullPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value ? value : '';
  }

}
