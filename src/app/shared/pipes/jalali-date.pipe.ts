import { Pipe, PipeTransform } from '@angular/core';
import * as momentJalaali from "moment-jalaali";

@Pipe({
  name: 'jalaliDate'
})
export class JalaliDatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return momentJalaali(value).format(args);
  }
/**
 *  <span dir="ltr">{{date | jalaliDate:'jYYYY/jMM/jDD hh:mm' }}</span>,
  <span dir="rtl">{{date | jalaliDate:'jD jMMMM jYYYY [ساعت] LT'}}</span>
 */
}
