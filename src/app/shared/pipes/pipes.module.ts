import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JalaliDatePipe } from './jalali-date.pipe';
import { Decode64Pipe } from "./decode64.pipe";
import { SafeHtmlPipe } from './safe-html.pipe';
import { DefaultValueIfNullPipe } from './default-value-if-null.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    JalaliDatePipe,
    Decode64Pipe,
    SafeHtmlPipe,
    DefaultValueIfNullPipe
  ],
  exports: [
    JalaliDatePipe,
    Decode64Pipe,
    SafeHtmlPipe,
    DefaultValueIfNullPipe
  ]
})
export class PipesModule {
}
