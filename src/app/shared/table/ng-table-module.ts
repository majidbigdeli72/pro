import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgTableComponent } from './ng-table.component';
import { NgTableFilteringDirective } from './ng-table-filtering.directive';
import { NgTablePagingDirective } from './ng-table-paging.directive';
import { NgTableSortingDirective } from './ng-table-sorting.directive';
import { PaginationModule } from '../pagination/pagination.module';
import { PipesModule } from '../pipes/pipes.module';
import { ModalModule } from '../modal/modal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PaginationModule,
    ModalModule,
    PipesModule
  ],
  declarations: [
    NgTableComponent,
    NgTableFilteringDirective,
    NgTablePagingDirective,
    NgTableSortingDirective
  ],
  exports: [
    NgTableComponent,
    NgTableFilteringDirective,
    NgTablePagingDirective,
    NgTableSortingDirective
  ]
})
export class Ng2TableModule {
}
