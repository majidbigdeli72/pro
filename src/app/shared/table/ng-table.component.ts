//refrence: https://valor-software.com/ng2-table/
import { Component, EventEmitter, Input, Output, OnChanges, SimpleChanges, ViewChild, TemplateRef, OnInit, ContentChild, AfterContentChecked, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpServiceService } from '../../core/http-service/http-service.service';
import { debounceTime, distinctUntilChanged, switchMap, takeUntil, takeWhile, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { ToastService } from '../../core/toast/toast.service';

@Component({
  selector: 'ng-table',
  templateUrl: "./ng-table.component.html",
  styleUrls: ['./ng-table.component.less']
})
export class NgTableComponent implements OnChanges, OnInit, AfterContentChecked, OnDestroy {
  ngAfterContentChecked(): void {
    this.pageNumber = this.body.PageIndex;
  }

  public constructor(
    private toast: ToastService,
    private http: HttpServiceService) {   }

  @ViewChild('operation', { read: TemplateRef }) operation: TemplateRef<any>;
  @ViewChild('standard', { read: TemplateRef }) standard: TemplateRef<any>;
  @ViewChild('image', { read: TemplateRef }) image: TemplateRef<any>;
  @ViewChild('icon', { read: TemplateRef }) icon: TemplateRef<any>;
  @ViewChild('iconStatus', { read: TemplateRef }) iconStatus: TemplateRef<any>;
  @ViewChild('dateTime', { read: TemplateRef }) dateTime: TemplateRef<any>;
  @ContentChild(TemplateRef) itemTemplate: TemplateRef<any>;


  @Output() public cellClicked: EventEmitter<any> = new EventEmitter();
  @Output() public removeClicked: EventEmitter<any> = new EventEmitter();
  @Output() public editClicked: EventEmitter<any> = new EventEmitter();


  // @Input() public starClicked: EventEmitter<any> = new EventEmitter();
  @Input() public UrlSpecial: string;
  @Input() public rows: Array<any>;
  @Input() public Default: boolean = true;
  @Input('pagination') _pagination;
  @Input('Url') _url;
  @Input('UrlRemove') _urlRemove;
  @Input('FieldForSerach') private _fieldForSearch: string = "Title";

  @Input('Body')
  public set body(body: any) {
    this._body = Object.assign(body, {
      PageIndex: 1
    });
  }
  public get body() {
    return this._body;
  }

  @Input()
  public set config(conf: any) {
    const _obj = Object.assign(this._config, conf);
    if (!_obj.className) {
      _obj.className = 'table-striped table-bordered';
    }
    if (_obj.className instanceof Array) {
      _obj.className = _obj.className.join(' ');
    }
    this._config = _obj;
  }
  public get config(): any {
    return this._config;
  }
  @Input()
  public set columns(values: Array<any>) {
    values.forEach((value: any) => {
      if (value.filtering) {
        this.showFilterRow = true;
      }
      if (value.className && value.className instanceof Array) {
        value.className = value.className.join(' ');
      }
      let column = this._columns.find((col: any) => col.name === value.name);
      if (column) {
        Object.assign(column, value);
      }
      if (!column) {
        this._columns.push(value);
      }
    });
  }
  public get columns(): Array<any> {
    return this._columns;
  }

  public get configColumns(): any {
    let sortColumns: Array<any> = [];

    this.columns.forEach((column: any) => {
      if (column.sort) {
        sortColumns.push(column);
      }
    });

    return { columns: sortColumns };
  }

  private pendingRequest: Subscription;
  private _body: any;
  private model1Changed: Subject<any> = new Subject<any>();
  private dueTime = 500;
  private _columns: Array<any> = [];
  private _config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  public isImage: boolean;
  public showFilterRow: Boolean = false;
  public enabledPagination: boolean = true;
  public RemoveId: number;
  public pageNumber: number;
  public FilteredCount: number;

  //count badge number page in page
  public MaxSize: number = 10;
  public NumPage: number = 1;
  public totalItems: number = 0;

  public searchItem: string;
  public showModal: boolean = false;
  public isModalRemove: boolean = true;
  public textModel: string;
  public imgModel: string;
  public showIndicator: boolean = false;
  ngOnInit(): void {
    this.Init();
    this.search();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes._fieldForSearch && !changes._fieldForSearch.firstChange) {
      this.pendingRequest.unsubscribe();
      this.body[changes._fieldForSearch.previousValue] = "";
    }
  }

  public InitPagination(e) {
    this.body.PageIndex = e.page;
    this.Init();
  }
  public Init() {
    if (this.config.filtering) {
      Object.assign(this.config.filtering, this.config.filtering);
    }
    if (this.config.sorting) {
      Object.assign(this.config.sorting, this.config.sorting);
    }
    --this.body.PageIndex;
    this.pendingRequest = this._init(this.searchItem).pipe(
      takeUntil(this.model1Changed),
      tap(x => {
        setTimeout(() => {
          if (this.pendingRequest)
            this.pendingRequest.unsubscribe();
        }, 5000);
      })
    ).subscribe(x => {
      if (x.Code == 200) {
        this.FilteredCount = x.Result.FilteredCount;
        this.NumPage = Math.ceil(x.Result.FilteredCount / this.body.PageSize);
        this.rows = x.Result.List;
        this.totalItems = x.Result.FilteredCount;
        this.pendingRequest.unsubscribe();
        this.showIndicator = false
      }
    }, (err) => { }, () => this.showIndicator = false);
  }

  public onSearch() {
    this.model1Changed.next(this.searchItem);
  }

  private search(page: any = { page: this.body.PageIndex, itemsPerPage: this.body.PageSize }) {
    this.model1Changed
      .pipe(
        debounceTime(this.dueTime),
        distinctUntilChanged(),
        switchMap(inputValue => {
          this.body.PageIndex = 0;
          return this._init(inputValue);
        })
      )
      .subscribe((x) => {
        if (x.Code == 200) {
          this.FilteredCount = x.Result.FilteredCount;
          this.NumPage = Math.ceil(x.Result.FilteredCount / this.body.PageSize);
          this.rows = x.Result.List;
          this.totalItems = x.Result.FilteredCount;
          this.showIndicator = false
        }
      }, (err) => { }, () => this.showIndicator = false);
  }

  private _init(input?: any) {
    this.showIndicator = true;
    const _field = this._fieldForSearch;
    let _obj = {
      PageIndex: this.body.PageIndex,
      PageSize: this.body.PageSize
    };
    _obj[_field] = input ? input : '';
    let obj: object = Object.assign(this.body, _obj);
    return this.http.post(this._url, this.body);
  }

  public onChangeTable(column: any): void {
    this.body.PageIndex = 1;
    this._columns.forEach((col: any) => {
      if (col.name !== column.name && col.sort !== false) {
        col.sort = '';
      }
    });
    const _cfg = this.configColumns.columns[0];
    let _name = '';
    let _order = 'DESC';
    if (_cfg) {
      _name = _cfg['name'];
      _order = _cfg['sort'];
    }
    Object.assign(this.body, {
      OrderByCol: _name,
      OrderByDir: _order.toUpperCase(),
    });
    this.Init();
  }

  public getData(row: any, propertyName: string, pic: boolean, icon: boolean, iconStatus: boolean): TemplateRef<any> {
    if (propertyName == 'Action')
      return this.operation;
    else if (propertyName == 'RegDate' || propertyName == 'StrDate')
      return this.dateTime;
    else {
      if (pic) {
        return this.image;
      } else if (icon) {
        return this.icon;
      } else if (iconStatus) {
        return this.iconStatus;
      }
      else {
        return this.standard;
      }
    }
  }

  public cellClick(row: any, column: any, event: any, pic?: boolean, icon?: boolean): void {
    if (icon) {
      this.SpecialItem(row, column, event);
      return;
    }
    this.showModal = true;
    this.isModalRemove = false;
    if (pic) {
      this.isImage = true;
      this.imgModel = column;
    }
    else {
      this.textModel = column;
      this.isImage = false;
    }
  }
  //#region Star
  public SpecialItem(row: any, isSpecial: any, event: any) {
    let _event =  event.srcElement || event.target;
    this.toast.Wait();
    const state: boolean = _event.getAttribute('state') == 'true' ? false : true;
    this.http.post(this.UrlSpecial, {
      id: row["Id"],
      Value: state
    }).subscribe(x => {
      if (x.Code == 200) {
        this.toast.Success();
        if (state)
        _event.className = 'fa fa-star background';
        else
        _event.className = "fa fa-star-o";
        _event.setAttribute('state', `${state}`);
      }
    }, (err) => { }, () => this.toast.ClearWaiting());
  }
  //#endregion Star

  public edit(id, index, data) {
    this.editClicked.emit({ id, index, data });
  }

  public remove(id, index) {
    this.showModal = true;
    this.isModalRemove = true;
    this.RemoveId = id;
  }

  public confirmRemove() {
    this.removeClicked.emit(this.RemoveId);
  }
  ngOnDestroy(): void { }
}
