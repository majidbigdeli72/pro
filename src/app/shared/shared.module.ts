import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedRoutingModule } from './shared-routing.module';
import { InputCheckBoxComponent } from './tags/input-check-box/input-check-box.component';
import { InputRadioComponent } from './tags/input-radio/input-radio.component';
import { InputSelectComponent } from './tags/input-select/input-select.component';
import { FormsModule } from '@angular/forms';
import { InputDateComponent } from './tags/input-date/input-date.component';
import { DpDatePickerModule } from 'ng2-jalali-date-picker';
import { InputTimeComponent } from './tags/input-time/input-time.component';
import { DateTimeDirective } from './directive/date-time.directive';
import { ModalModule } from './modal/modal.module';
import { PaginationModule } from './pagination';
import { Ng2TableModule } from './table/ng-table-module';
import { PipesModule } from "./pipes/pipes.module";
import { NumberOnlyDirective } from './directive/number-only.directive';
import { InputDateJustComponent } from './tags/input-date-just/input-date-just.component';
import { FileUploadComponent } from './tags/file-upload/file-upload.component';
import { ProvideParentForm } from "./directive/provide-parent-form.directive";
import { EqualValidator } from "./directive/equal-validator.directive";
import { CustomValidationModule } from './custom-validation/custom-validation.module';
import { ModalDynamicModule } from "./modal-dynamic/modal-dynamic.module";
import { DropdpwnMultiSelectComponent } from "./dropdown/dropdpwn-multi-select.component";
import { NgSelectModule } from '@ng-select/ng-select';
import { DropdownDynamicComponent } from './dropdown-dynamic/dropdown-dynamic.component';

@NgModule({
  imports: [
    CommonModule,
    SharedRoutingModule,
    FormsModule,
    DpDatePickerModule,
    PaginationModule.forRoot(),
    Ng2TableModule,
    ModalModule,
    CustomValidationModule,
    PipesModule,
    ModalDynamicModule,
    NgSelectModule

  ],
  declarations: [
    InputCheckBoxComponent,
    InputRadioComponent,
    InputSelectComponent,
    InputDateComponent,
    InputTimeComponent,
    DateTimeDirective,
    NumberOnlyDirective,
    EqualValidator,
    InputDateJustComponent,
    FileUploadComponent,
    ProvideParentForm,
    DropdpwnMultiSelectComponent,
    DropdownDynamicComponent
  ],
  exports: [
    CommonModule,
    SharedRoutingModule,
    InputCheckBoxComponent,
    InputRadioComponent,
    InputSelectComponent,
    InputDateComponent,
    InputTimeComponent,
    DateTimeDirective,
    PaginationModule,
    EqualValidator,
    Ng2TableModule,
    PipesModule,
    ModalModule,
    CustomValidationModule,
    NumberOnlyDirective,
    InputDateJustComponent,
    FileUploadComponent,
    ProvideParentForm,
    ModalDynamicModule,
    DropdpwnMultiSelectComponent,
    DropdownDynamicComponent

  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [

      ]
    }
  }
}
