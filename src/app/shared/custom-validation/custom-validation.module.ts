import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidatManualCodeDirective } from './validat-manual-code.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ValidatManualCodeDirective
  ],
  exports:[
    ValidatManualCodeDirective
  ]
})
export class CustomValidationModule { }
