import { Directive, Input, forwardRef } from "@angular/core";
import { AbstractControl, NG_ASYNC_VALIDATORS, AsyncValidator } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { takeUntil, take, debounceTime, distinctUntilChanged, mergeMap, switchMap, tap, catchError } from 'rxjs/operators';
import { HttpServiceService } from "../../core/http-service/http-service.service";
@Directive({
  selector:
    "[uniqueValidator][formControlName],[uniqueValidator][formControl],[uniqueValidator][ngModel]",
  providers: [{
    provide: NG_ASYNC_VALIDATORS,
    useExisting: forwardRef(() => ValidatManualCodeDirective),
    multi: true
  }],
  host: {
    '[attr.required]': 'required' ? "" : null,
    '(blur)': 'onBlur($event)'
  }
})
export class ValidatManualCodeDirective implements AsyncValidator {
  @Input("remote-length") _length: number;
  @Input("remote-url") _url: string;
  @Input("remote-oldValue") _oldValue: string;
  private _required: boolean = true;
  private changed$: Subject<any> = new Subject<any>();
  constructor(private http: HttpServiceService) { }

  validate(control: AbstractControl): Promise<{ [key: string]: any; }> | Observable<{ [key: string]: any; }> {
    if (this._oldValue)
      if (this._oldValue == control.value)
        return new Promise((obs: any) => {
          obs(null);
        })
    this.changed$.next(null);
    if (control.value && (control.value.length == this._length)) {
      const _debounceTime = 300;
      return new Observable((obs: any) => {
        control.valueChanges
          .pipe(
            takeUntil(this.changed$),
            take(1),
            debounceTime(_debounceTime),
            distinctUntilChanged(),
            switchMap(term => {
              return this.doRemoteValidation(control.value);
            })
          ).subscribe(
            (result: IRemoteValidationResult) => {
              if (result.Result) {
                console.log('glkjhlkfhlk')
                obs.next({
                  remoteValidation: {
                    result: true,
                    message: 'کد قبلا ثبت گردیده است'
                  }
                });
              } else {
                obs.next(null);
              }
              obs.complete();
            },
            error => {
              obs.next({
                remoteValidation: {
                  result: true,
                  message: 'مشکلی رخ داده است، یکبار دیگرتلاش کنید'
                }
              });
              obs.complete();
            }
          );
      });
    }

    return new Promise((obs: any) => {
      obs({
        remoteValidation: {
          result: true,
          message: `کد میبایست ${this._length} عدد باشد`
        }
      });
    })
  }

  registerOnValidatorChange?(fn: () => void): void {
  }

  private doRemoteValidation(code: string): Observable<IRemoteValidationResult> {
    return this.http
      .post(this._url, {
        Value: code.trim()
      }).pipe(
        tap(result => console.log("remoteValidation result: ", result)),
        catchError(this.handleError));

  }

  private handleError(error: Response): Observable<any> {
    return Observable.throw(error.statusText);
  }

  private onBlur(e: Event): Promise<{ [key: string]: any; }> | Observable<{ [key: string]: any; }> {
    return new Promise((obs: any) => {
      obs({
        remoteValidation: {
          result: true,
          message: `کد میبایست ${this._length} عدد باشد`
        }
      });
    })
  }
}

export interface IRemoteValidationResult {
  Message: string,
  Result: boolean
}