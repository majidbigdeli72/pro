import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { DatePickerComponent } from 'ng2-jalali-date-picker';
import * as moment from 'jalali-moment';

@Component({
  selector: 'basketik-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.less']
})
export class InputDateComponent implements OnInit, OnChanges {
  @Input() model: any;
  @Input() placeholder: string;
  @Output() modelCahnge: EventEmitter<any> = new EventEmitter<any>();
  @Input() disabled: boolean;
  @Input() ShowTime: boolean = true;

  public dateObject: any;

  @ViewChild('dayPicker') datePicker: DatePickerComponent;

  public datePickerConfig = {
    format: 'HH:mm YYYY/M/DD',
    closeOnSelect: true,
    openOnClick: true,
    openOnFocus: true,
  }
  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {

    if (changes.disabled)
      this.disabled = changes.disabled.currentValue;
    if (changes.ShowTime) {
      this.ShowTime = changes.ShowTime.currentValue == 'true';
      !this.ShowTime && Object.assign(this.datePickerConfig, {
        format: "YYYY/M/DD"
      });
    }
    if (changes.model && changes.model.currentValue)
      this.model = changes.model.currentValue;
  }

  ngOnInit() {
    if (this.model) {
      //this.model: 2018-03-27 17:03:38
      //_date: 1397-10-21
      const _d = moment(this.model);
      const _date = _d.locale('fa').format('YYYY-M-D');
      this.dateObject = moment(_date, 'jYYYY,jMM,jDD');
    }
  }

  public change(e) {
    //2/7/2018 18:00:00
    if (this.ShowTime)
      this.modelCahnge.emit(e.locale('en').format("MM/DD/YYYY HH:mm:ss"));
    else
      this.modelCahnge.emit(e.locale('en').format("MM/DD/YYYY"));
  }
}
