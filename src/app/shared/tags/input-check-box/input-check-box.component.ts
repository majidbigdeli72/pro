import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, ViewChildren, ElementRef, SimpleChange } from '@angular/core';
import { inputModel } from '../input-model';
@Component({
  selector: 'basketik-input-check-box',
  templateUrl: './input-check-box.component.html',
  styleUrls: ['./input-check-box.component.less']
})
export class InputCheckBoxComponent implements OnInit, OnChanges {
  @Input() data: Array<inputModel>;
  @Input() prefix: string;
  @Output() TypeDataChange: EventEmitter<any> = new EventEmitter<any>();
  @Input() TypeDataId: any;
  @ViewChildren('type', { read: ElementRef }) element: ElementRef[];
  ngOnChanges(changes: SimpleChanges): void { }

  constructor() { }
  ngOnInit() { }

  public change(value, type) {
    let _ids: Array<{ ProductTypeId: number }> = [];
    let _elements = this.element.filter(x => {
      if (x.nativeElement.checked) {
        _ids.push({
          ProductTypeId: x.nativeElement.getAttribute('value')
        })
      }
    })
    this.TypeDataId = _ids;
    this.TypeDataChange.emit(_ids);
  }

  public checked(i): boolean {
    let status: boolean = false;
    if (this.TypeDataId) {
      let _arr = Array.from(this.TypeDataId);
      _arr.forEach(x => {
        if (x['ProductTypeId'] == i)
          status = true;
      })
    }

    return status;
  }
}
