import { Component, OnInit, Input, EventEmitter, Output, ViewChild, SimpleChanges } from '@angular/core';
import { DatePickerComponent } from 'ng2-jalali-date-picker';

@Component({
  selector: 'basketik-input-time',
  templateUrl: './input-time.component.html',
  styleUrls: ['./input-time.component.less']
})
export class InputTimeComponent implements OnInit {

  @Input() model: any;
  @Input() placeholder: string;
  @Output() modelCahnge: EventEmitter<any> = new EventEmitter<any>();
  @Input() disabled: boolean;
  public dateObject: any;

  @ViewChild('dayPicker') datePicker: DatePickerComponent;
  public datePickerConfig = {
    format: 'HH:mm',
    closeOnSelect: true,
    openOnClick: true,
    openOnFocus: true,
  }
  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.disabled)
      this.disabled = changes.disabled.currentValue;
    if (changes.model && changes.model.currentValue)
      this.dateObject = changes.model.currentValue;
  }

  ngOnInit() { }

  public change(e) {
    if (typeof e === 'string')
      this.modelCahnge.emit(e)
    else
      this.modelCahnge.emit(e.locale('en').format("HH:mm"));
  }

}
