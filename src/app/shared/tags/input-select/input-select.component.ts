import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { inputModel } from '../input-model';

@Component({
  selector: 'basketik-input-select',
  templateUrl: './input-select.component.html',
  styleUrls: ['./input-select.component.less']
})
export class InputSelectComponent implements OnInit {
  @Input() data: Array<{ value: number, text: string }>
  @Input() multiSelect: any;
  @Input() productSkuField: any;
  @Output() selectedValue: EventEmitter<any> = new EventEmitter<any>();
  public oldValue: any;
  constructor() { }

  ngOnChanges(changes: SimpleChanges): void { }

  ngOnInit() { }

  public change(value) {
    if (this.oldValue != value) {
      const _value: any = this.multiSelect == 'true' ? value : parseInt(value[0]);
      this.productSkuField = _value;
      this.selectedValue.emit(_value);
    }
    this.oldValue = value;
  }
}
