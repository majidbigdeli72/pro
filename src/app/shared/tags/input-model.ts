export interface inputModel {
    value?: string;
    text: string;
    name: string;
}

export interface selectModel{
    value:number;
    text:string;
}