import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { inputModel } from '../input-model';

@Component({
  selector: 'basketik-input-radio',
  templateUrl: './input-radio.component.html',
  styleUrls: ['./input-radio.component.less']
})
export class InputRadioComponent implements OnInit, OnChanges{
  @Input() data: Array<inputModel> = [];
  @Input() public prefixHtml: string = '';
  @Input() public MeasurementCreteriaId: any;
  @Output() MeasurementCreteriaIdChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() private onChangeEv: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
  
  }

  public change(Value) {
    this.MeasurementCreteriaId = Value;
    this.MeasurementCreteriaIdChange.emit(Value);
    this.onChangeEv.emit(Value);
  }

  ngOnInit() {

  }
}
