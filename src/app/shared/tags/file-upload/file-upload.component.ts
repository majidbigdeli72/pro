import { Component, OnInit, forwardRef, Input, Output, EventEmitter, } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'basketik-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FileUploadComponent),
      multi: true
    }
  ]
})
export class FileUploadComponent implements ControlValueAccessor {
  selectedFileName: string = null;
  @Input() showFileNameInput: boolean;
  @Input() uploadButtonText: string;
  @Output() picCahnge: EventEmitter<any> = new EventEmitter<any>();

  writeValue(value: any) {
    //Handle write value
  }
  propagateChange = (_: any) => { };
  registerOnChange(fn) {
    debugger
    this.propagateChange = fn;
  }
  registerOnTouched() { }

  changeListener($event): void {
    // debugger; // uncomment this for debugging purposes
    this.readThis($event.target);
  }
  readThis(inputValue: any): void {
    // debugger; // uncomment this for debugging purposes
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();

    myReader.readAsDataURL(file);

    myReader.onloadend = (e) => {
      //this.propagateChange(myReader.result);
      this.picCahnge.emit(myReader.result)
      this.selectedFileName = file.name;
    }
    myReader.onerror = function (error) {
      console.log('Erro ao ler a imagem : ', error);
    };
  }

}
