import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { DatePickerComponent } from 'ng2-jalali-date-picker';
import * as moment from 'jalali-moment';

@Component({
  selector: 'basketik-input-date-just',
  templateUrl: './input-date-just.component.html',
  styleUrls: ['./input-date-just.component.less']
})
export class InputDateJustComponent implements OnInit {

  @Input() model: any;
  @Input() placeholder: string;
  @Input() disabled: boolean;
  @Input() completeDate: boolean = false;
  @Output() modelCahnge: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('dayPicker') datePicker: DatePickerComponent;

  public dateObject: any;
  public datePickerConfig = {
    format: 'YYYY/MM/DD',
    closeOnSelect: true,
    openOnClick: true,
    openOnFocus: true,
  }
  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.disabled)
      this.disabled = changes.disabled.currentValue;
  }

  ngOnInit() {
    if (this.model) {
      //this.model: 2018-03-27 17:03:38
      //_date: 1397-10-21
      const _d = moment(this.model);
      const _date = _d.locale('fa').format('YYYY/M/D');
      this.dateObject = moment(_date, 'jYYYY,jMM,jDD');
    }
  }


  public change(e) {
    if (e && this.completeDate)
      this.modelCahnge.emit(e._d);
    else if (e)
      //2/7/2018
      this.modelCahnge.emit(e.locale('en').format("MM/DD/YYYY"));
  }
}
