import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputDateJustComponent } from './input-date-just.component';

describe('InputDateJustComponent', () => {
  let component: InputDateJustComponent;
  let fixture: ComponentFixture<InputDateJustComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputDateJustComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDateJustComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
