import { Component, NgModule, VERSION, Input, ViewEncapsulation, Directive } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, NgForm, ControlContainer } from '@angular/forms';

@Directive({
    selector: '[provide-parent-form]',
    providers: [
        {
            provide: ControlContainer,
            useFactory: function (form: NgForm) {
                return form;
            },
            deps: [NgForm]
        }
    ]
})
export class ProvideParentForm { }