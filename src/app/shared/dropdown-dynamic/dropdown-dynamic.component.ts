import { Component, OnInit, Input, Output, TemplateRef, ContentChild, EventEmitter, ViewEncapsulation, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'basketik-dropdown-dynamic',
  templateUrl: './dropdown-dynamic.component.html',
  styleUrls: ['./dropdown-dynamic.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class DropdownDynamicComponent implements OnInit, OnChanges {
  @Input('placeholder') _placeholder: string = "انتخاب کنید";
  @Input() items: Array<any>;
  @Input() model: string[];
  @Input('multiple') _multiple: boolean = false;
  @Input('closeOnSelect') closeOnSelect: boolean = true;
  @Input('bindLabel') _bindLabel: string = 'Name';
  @Input('bindValue') _bindValue: string = 'Id';

  @ContentChild(TemplateRef) itemTemplate: TemplateRef<any>;

  @Output('modelCahnge') _modelChange: EventEmitter<any> = new EventEmitter<any>();
  @Output('onOpen') _onOpen: EventEmitter<any> = new EventEmitter<any>();
  @Output('onClose') _onClose: EventEmitter<any> = new EventEmitter<any>();
  @Output('onFocus') _onFocus: EventEmitter<any> = new EventEmitter<any>();
  @Output('onChange') _onChange: EventEmitter<any> = new EventEmitter<any>();
  @Output('onBlur') _onBlur: EventEmitter<any> = new EventEmitter<any>();
  @Output('onClear') _onClear: EventEmitter<any> = new EventEmitter<any>();
  @Output('onAdd') _onAdd: EventEmitter<any> = new EventEmitter<any>();
  @Output('onScrollToEnd') _onScrollToEnd: EventEmitter<any> = new EventEmitter<any>();
  @Output('onRemove') _onRemove: EventEmitter<any> = new EventEmitter<any>();

  public refresh: boolean = true;
  public clearAllText: string = "Clear";
  public _clearAllText: Array<any> = [];
  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.model && changes.model.currentValue) {
      if (changes.items && changes.items.currentValue) {
        this.refresh = false;
        this.model = changes.model.currentValue;
        this._modelChange.emit(this.model);
        setTimeout(() => {
          this.refresh = true;
        }, 1000);
      }
    }
    if (changes._multiple && changes._multiple.currentValue) {
      this._multiple = changes._multiple.currentValue == 'true';
    }
  }

  ngOnInit() { }

  public modelChange(e) {
    this._modelChange.emit(e);
  }

  public onOpen() {
    this._onOpen.emit();
  }
  public onClose() {
    this._onClose.emit();
  }
  public onFocus(e) {
    this._onFocus.emit(e);
  }
  public onBlur(e) {
    this._onBlur.emit(e);
  }
  public onClear() {
    this._onClear.emit();
  }
  public onAdd(e) {
    this._onAdd.emit(e);
  }
  public onScrollToEnd(e) {
    this._onScrollToEnd.emit(e);
  }
  public onRemove(e) {
    this._onRemove.emit(e);
  }
  public onChange(e) {
    this._onChange.emit();
  }
}
