import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalDynamicComponent } from './modal/modal.component';
import {PipesModule} from "../pipes/pipes.module"


@NgModule({
  imports: [
    CommonModule,
    PipesModule,

  ],
  declarations: [ModalDynamicComponent],
  exports: [
    ModalDynamicComponent
  ]
})
export class ModalDynamicModule { }
