import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'basketik-modalDynamic',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less']
})
export class ModalDynamicComponent implements OnInit, OnChanges {

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.backgroundModal.currentValue)
      this.backgroundModal = changes.backgroundModal.currentValue === 'true';
  }

  @Input() public backgroundModal: boolean;
  @Input() public caption: string;
  @Input() public body: string;

  @Output() public close: EventEmitter<any> = new EventEmitter();
  @Output() public confirm: EventEmitter<any> = new EventEmitter<any>();

  newname = "";

  ngOnInit() { }

  ok() {
    this.confirm.emit();
  }

  cancel() {
    this.close.emit(null);
  }

}
