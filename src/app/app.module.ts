import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, APP_INITIALIZER } from "@angular/core";
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './home/home.module';
import { BrandsModule } from './feature/brands/brands.module';
import { AppConfigService } from './core/app-config-setting-bootstrap/app-config-service.service';
import { FileManagerModule } from './file-manager/file-manager.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptorService } from './core/interceptors/loader-interceptor.service';
import { InterceptorService } from './core/interceptors/InterceptorService';
import { CategoryModule } from './feature/category/category.module';
import { ProducerModule } from "./feature/producer/producer.module";
import { SubCategoryModule } from './feature/sub-category/sub-category.module';
import { SubBrandsModule } from './feature/sub-brands/sub-brands.module';
import { BrandsRoutingModule } from './feature/brands/brands-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ProductSkuModule } from './feature/product-sku/product-sku.module';
import { NG_ASYNC_VALIDATORS } from '@angular/forms';
import { ProductModule } from './feature/product/product.module';
import { PaginationModule } from './shared/pagination';
import { FormsModule } from '@angular/forms';
import { AdvertisementModule } from "./feature/advertisement/advertisement.module";
import { SettingModule } from "./feature/setting/setting.module";
import { OrderModule } from "./feature/order/order.module"
import { SupplierModule } from './feature/supplier/supplier.module';
import { UsersModule } from './feature/users/users.module';
import { RoleModule } from './feature/role/role.module'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    RoleModule,
    OrderModule,
    SettingModule,
    AdvertisementModule,
    SupplierModule,
    ProducerModule,
    HttpClientModule,
    BrowserAnimationsModule ,
    BrowserModule,
    CategoryModule,
    SubCategoryModule,
    CoreModule,
    SubBrandsModule,
    SharedModule.forRoot(),
    BrandsModule,
    FormsModule,
    AuthenticationModule,
    FileManagerModule,
    SlimLoadingBarModule.forRoot(),
    ProductSkuModule,
    ProductModule,
    HomeModule,
    UsersModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    }, 
    {
      provide: APP_INITIALIZER,
      useFactory: (config: AppConfigService) => () => config.loadClientConfig(),
      deps: [AppConfigService],
      multi: true
    }],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
