import { Component, OnInit, ErrorHandler } from '@angular/core';
import { Icredentials } from '../icredentials';
import { AuthService } from '../../core/user-service/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'basketik-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute) { }

  public model: Icredentials = { username: "", password: "", rememberMe: false };
  public error = "";
  public returnUrl: string;
  public loadinglogin: boolean = false;
  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"];
    this.authService.logout(true);
  }

  public submitForm(form: NgForm) {
    this.loadinglogin = true;
    this.authService.login(form.value)
      .subscribe(isLoggedIn => {
        if (isLoggedIn) {
          if (this.returnUrl)
            this.router.navigate([this.returnUrl]);
          else if (this.authService.redirectUrl)
            this.router.navigateByUrl(this.authService.redirectUrl);
          else this.router.navigate(["/home"]);
        }
      },
        (error: HttpErrorResponse) => {
          console.log("Login error", error);
          this.error = "نام کاربری یا رمز عبور اشتباه است";
          this.loadinglogin = false;
        }, () => this.loadinglogin = false);
  }
}
