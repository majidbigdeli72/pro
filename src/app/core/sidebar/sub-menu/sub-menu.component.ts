import { Component, OnInit, Input } from '@angular/core';
import { SlideInOutAnimation } from '../../../animation/slideInOut';

@Component({
  selector: 'basketik-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.less'],
  animations: [
    SlideInOutAnimation
  ]
})
export class SubMenuComponent implements OnInit {
  @Input('model') _model;
  constructor() { }
  public isDisabled: boolean = false;

  ngOnInit() { }

  public animationState = 'out';
  toggleShowDiv(ev: Event) {
    this.animationState = this.animationState === 'out' ? 'in' : 'out';
  }

  public animationStarted(e) { }
  public animationDone(e) { }
}
