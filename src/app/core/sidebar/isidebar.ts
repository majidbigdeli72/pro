export interface Isidebar {
    id: Number;
    link: string;
    name: string;
    subset?: {
        link: string;
        name: string;
    }
}
