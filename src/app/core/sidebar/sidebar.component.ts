import { Component, OnInit, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { Isidebar } from './isidebar';
import { Observable, Subscribable } from 'rxjs/observable';
import { HttpServiceService } from '../http-service/http-service.service';
import { SlideInOutSlider, RotateArrow } from '../../animation/SlideInOutSlider';

@Component({
  selector: 'basketik-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less'],
  animations: [
    SlideInOutSlider,
    RotateArrow
  ]
})
export class SidebarComponent implements OnInit {
  constructor(private http: HttpServiceService) { }
  public sidebars: Isidebar[] = [];
  public open: string = 'in';
  public name: string;
  @Output() public showSidebar: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit() {
    this.http.get<Isidebar[]>('assets/json/nav.json').subscribe((res) => {
      this.sidebars = res;
    });
  }

  sideNavHandle() {
    this.open = this.open == 'out' ? 'in' : 'out';
    this.showSidebar.emit(this.open == 'out');
  }
}
