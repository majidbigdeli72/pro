import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { CoreRoutingModule } from './core-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FilterPipe } from './sidebar/filter.pipe';
import { SubMenuComponent } from './sidebar/sub-menu/sub-menu.component';
import { HttpServiceService } from './http-service/http-service.service';
import { AppConfigService } from './app-config-setting-bootstrap/app-config-service.service';
import { FormsModule } from '@angular/forms';
import { BrowserStorageService } from './browser-storage/browser-storage-service.service';
import { InterceptorService } from './interceptors/InterceptorService';
import { AuthService } from './user-service/auth.service';
import { APP_CONFIG, AppConfig } from './user-service/app.config';
import { UserIsLoginGuard } from './user-service/user-is-login.guard';
import {ToastyModule} from 'ng2-toasty';
import { ToastService } from './toast/toast.service';
import { SetTitleNavbarService } from './navbar/set-title-navbar.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    CoreRoutingModule,
    ToastyModule.forRoot()
  ],
  exports: [
    NavbarComponent,
    SidebarComponent,
    FilterPipe,
    SubMenuComponent,
    ToastyModule
  ],
  declarations: [
    NavbarComponent,
    SidebarComponent,
    FilterPipe,
    SubMenuComponent
  ],
  providers: [
    HttpServiceService,
    AppConfigService,
    BrowserStorageService,
    InterceptorService,
    AuthService,
    { provide: APP_CONFIG, useValue: AppConfig },
    UserIsLoginGuard,
    ToastService,
    SetTitleNavbarService
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core)
      throw new Error("CORE INITIALIZED");
  }
}
