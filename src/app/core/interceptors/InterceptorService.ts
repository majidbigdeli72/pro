import { Injectable } from '@angular/core';
import { AuthService } from '../user-service/auth.service';
import { Observable } from 'rxjs/Observable';
import { HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse, HttpInterceptor } from '@angular/common/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ToastService } from '../toast/toast.service';

@Injectable()
export class InterceptorService implements HttpInterceptor {
  constructor(
    private userService: AuthService, 
    private router: Router, 
    private toast: ToastService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const JWT = `Bearer ${this.userService.getToken()}`;
    req = req.clone({
      setHeaders: {
        Authorization: JWT
      }
    });
    return next.handle(req).map((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        return event;
      }
    }).catch((err: any) => {
      if (err instanceof HttpErrorResponse) {
        switch (err.status) {
          case 400:
            this.toast.Error({
              title: 'درخواست اشتباه',
              msg: ''
            });
            break;
          case 404:
            this.toast.Error({
              title: 'پیدا نشد',
              msg: 'محتوی مورد نظر پیدا نشد'
            });
            break;
          case 500:
            this.toast.Error();
            break;
          case 401:
            this.toast.Error({
              title: 'عدم دسترسی',
              msg: 'شما مجوز انجام این کار را ندارید'
            });
            break;
          case 403:
            this.toast.Error({
              title: 'عدم دسترسی',
              msg: 'شما دسترسی به انجام این کار را ندارید'
            });
            break;
          default:

        }
        return Observable.throw(err);
      }
    })
  }
}
