import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { AppConfigService } from '../app-config-setting-bootstrap/app-config-service.service';
import { Icredentials } from '../../authentication/icredentials';

import { retry, catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

@Injectable()
export class HttpServiceService {

  constructor(private http: HttpClient, private baseUrl: AppConfigService) { }

  public get<T>(url: string): Observable<any> {
    return this.http.get<T>(url)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  public post(url: string, body: any, version: any = '1.0'): Observable<any> {
    let fullUrl: string = this.baseUrl.configuration.host + url;
    let header: HttpHeaders = new HttpHeaders({
      'api-version': version,
      'Content-Type': "application/json"
    });

    return this.http.post(fullUrl, body, { headers: header, })
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
  }

  public FileManagerExec(url: string, mode: string, path: string, body: any, name?: string, version: any = "2.0"): Observable<any> {
    let fullUrl: string = this.baseUrl.configuration.host + url;

    let param = new HttpParams()
      .set('name', name)
      .set('mode', mode)
      .set('path', path);

    let formData: FormData = new FormData();
    formData.append('', body);

    let header: HttpHeaders = new HttpHeaders({
      'api-version': version
    });

    return this.http.post(fullUrl, formData, {
      headers: header,
      params: param,
    })
      .pipe(
        retry(3),
        catchError(this.handleError));
  }


  public postLogin(body: Icredentials): Observable<any> {
    let fullUrl: string = this.baseUrl.configuration.authUrl;

    let frmdata = new URLSearchParams();
    frmdata.append('username', body.username);
    frmdata.append('client_id', this.baseUrl.configuration.clientid);
    frmdata.append('grant_type', 'password');
    frmdata.append('password', body.password);

    let header: HttpHeaders = new HttpHeaders({
      'Accept': '*/*',
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    });
    return this.http.post(fullUrl, frmdata.toString(), { headers: header })
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${JSON.stringify(error.status)}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };
}
