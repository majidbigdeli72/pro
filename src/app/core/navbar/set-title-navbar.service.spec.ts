import { TestBed, inject } from '@angular/core/testing';

import { SetTitleNavbarService } from './set-title-navbar.service';

describe('SetTitleNavbarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SetTitleNavbarService]
    });
  });

  it('should be created', inject([SetTitleNavbarService], (service: SetTitleNavbarService) => {
    expect(service).toBeTruthy();
  }));
});
