import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Title } from '@angular/platform-browser';

@Injectable()
export class SetTitleNavbarService {
  constructor(private titleService: Title) { }

  private dataSource = new BehaviorSubject<string>(this.titleService.getTitle());
  data = this.dataSource.asObservable();

  updatedDataSelection(data: string) {
    this.dataSource.next(data);
  }
}
