import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Inotificatin } from "./inotification";
import { HttpServiceService } from '../http-service/http-service.service';
import { Title } from '@angular/platform-browser';
import { SetTitleNavbarService } from './set-title-navbar.service';
@Component({
  selector: 'basketik-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  public events: Array<Inotificatin>;
  public eventContentEnabled: boolean = false;
  public notiCount: Number;
  public Title: string;

  constructor(
    private dataService: SetTitleNavbarService,
    private http: HttpServiceService,
    private titleService: Title) { }

  ngOnInit() {
    this.http.get<Inotificatin[]>("assets/json/event.json").subscribe(x => {
      this.events = x.sort((a, b) => +b.id - +a.id);
      this.notiCount = x.length;
    });
    this.dataService.data.subscribe(data => {
      this.titleService.setTitle(data);
      this.Title = this.titleService.getTitle();
    })
  }

}
