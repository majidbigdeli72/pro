import { trigger, state, style, transition, group, animate } from "@angular/animations";

export const SlideInOutSlider = [
    trigger('SlideInOutSlider', [
        state('in', style({
            transform: 'translate3d(0%,0px,0px)'
        })),
        state('out', style({
            transform: 'translate3d(96%,0px,0px)'
        })),
        transition('in => out',  animate('300ms ease-in')),
        transition('out => in',  animate('300ms ease-in'))
    ]),
]

export const RotateArrow = [
    trigger('RotateArrow', [
        state('in', style({
            transform: 'rotate(0deg)'
        })),
        state('out', style({
            transform: 'rotate(180deg)'
        })),
        transition('in => out',  animate('300ms ease-in')),
        transition('out => in',  animate('300ms ease-in'))
    ]),
]
