import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/user-service/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { interval } from 'rxjs/observable/interval';
import { Observable } from 'rxjs/Observable';
import { HttpServiceService } from '../../core/http-service/http-service.service';

@Component({
  selector: 'basketik-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.less']
})
export class IndexComponent implements OnInit {
  constructor(private AuthService: AuthService,
    private http: HttpServiceService) { }
  private intervalSubscriptionProductSku: Subscription | null = null;
  private intervalSubscriptionProduct: Subscription | null = null;
  private intervalSubscriptionProducer: Subscription | null = null;
  private intervalSubscriptionBrands: Subscription | null = null;

  public ProductSku: number;
  public Product: number;
  public Producer: number;
  public Brands: number;

  ngOnInit() {
    let _countProductSku: number;
    let _countProduct: number;
    let _countProducer: number;
    let _counbrands: number;
    this.http.post('productSku/getCount',{}).subscribe(x=>{
      if(x.Code == 200)
      _countProductSku = x.Result;
    })
    this.http.post('product/getCount',{}).subscribe(x=>{
      if(x.Code == 200)
      _countProduct = x.Result;
    })
    this.http.post('producer/getCount',{}).subscribe(x=>{
      if(x.Code == 200)
      _countProducer = x.Result;
    })
    this.http.post('brand/getCount',{}).subscribe(x=>{
      if(x.Code == 200)
      _counbrands = x.Result;
    })
    this.startInterval(_countProductSku).subscribe(i => {
      if (_countProductSku >= i)
        this.ProductSku = i
      else if (this.intervalSubscriptionProductSku)
        this.intervalSubscriptionProductSku.unsubscribe()
    });
    this.startInterval(_countProduct).subscribe(i => {
      if (_countProduct >= i)
        this.Product = i
      else if (this.intervalSubscriptionProduct)
        this.intervalSubscriptionProduct.unsubscribe()
    });
    this.startInterval(_countProducer).subscribe(i => {
      if (_countProducer >= i)
        this.Producer = i
      else if (this.intervalSubscriptionProducer)
        this.intervalSubscriptionProducer.unsubscribe()
    });
    this.startInterval(_counbrands).subscribe(i => {
      if (_counbrands >= i)
        this.Brands = i
      else if (this.intervalSubscriptionBrands)
        this.intervalSubscriptionBrands.unsubscribe()
    });
  }

  startInterval(count: number): Observable<any> {
    const _time = this.calculateTime(count);
    const interval$ = interval(_time);
    return interval$
      .finally(() => console.log("All done!"))
  }

  private calculateTime(count): number {
    const time = Math.sqrt((3 / count) * 1000);
    return time;
  }
}
