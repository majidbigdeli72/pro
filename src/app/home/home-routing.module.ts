import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserIsLoginGuard } from '../core/user-service/user-is-login.guard';

const routes: Routes = [
  {
    path: 'home',
    component: IndexComponent,
    canActivate: [
      UserIsLoginGuard
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
