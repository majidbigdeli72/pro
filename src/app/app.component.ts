import { Component, OnInit, OnChanges, SimpleChanges, OnDestroy, ChangeDetectorRef, AfterContentInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from './core/user-service/auth.service';
import { Router } from '@angular/router';
import { BrowserStorageService } from './core/browser-storage/browser-storage-service.service';


@Component({
  selector: 'basketik-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy, AfterContentInit {
 
  public login: boolean;
  public slideBar: boolean;
  public navBar: boolean;
  public isLoggedIn: boolean;
  subscription: Subscription;
  public displayName: string;
  public name: any;
  public fullScreen: boolean = this.isLoggedIn;
  constructor(
    private authService: AuthService,
    private storage: BrowserStorageService,
    private chd: ChangeDetectorRef) { }

  ngOnInit(): void {  }

  ngAfterContentInit(): void {
    if (this.storage.getLocal('setting_background')) {
      let body = document.getElementsByTagName('body')[0];
      body.style.backgroundImage = `url(${this.storage.getLocal('setting_background')})`;
    }
    this.subscription = this.authService.authService$.subscribe(status => {
      this.isLoggedIn = status;
      if (status)
        this.displayName = this.authService.getDisplayName();
      this.slideBar = status;
      this.navBar = status;
      this.chd.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public logout() {
    this.authService.logout(true);
  }

  public showSidebarclicked(e) {
    this.fullScreen = e;
  }
}
