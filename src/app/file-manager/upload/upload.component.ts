import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { HttpServiceService } from '../../core/http-service/http-service.service';
import { ListComponent } from '../list/list.component';
import { ToastService } from '../../core/toast/toast.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'basketik-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.less']
})
export class UploadComponent implements OnInit {
  public previews: Array<{ name: string, path: string, file: any }> = [];
  public backToFolderVal: String;
  public disabled: boolean = true;

  @Output('goToFolder') goToFolder: EventEmitter<any> = new EventEmitter<any>();
  @Input("pathUrl") pathUrl: string[];

  constructor(
    private _DomSanitizationService: DomSanitizer,
    private toast: ToastService,
    private http: HttpServiceService) { }

  @ViewChild('fileUploadInput', { read: ElementRef }) fileUploadInput: ElementRef;

  ngOnInit() { }

  public openInput() {
    this.fileUploadInput.nativeElement.click();
  }

  public showPreviewImage(event) {
    if (event.target.files.length > 10) {
      this.toast.Error({
        msg: 'شما مجاز به بارگزاری 10 عکس بیشتر نیستید',
        title: 'تعداد مجاز عکس 10'
      });
      return;
    }
    if (this.previews.length >= 10) {
      this.toast.Error({
        msg: 'شما مجاز به بارگزاری 10 عکس بیشتر نیستید',
        title: 'تعداد مجاز عکس 10'
      });
      return;
    }
    this.toast.Wait()
    if (event.target.files && event.target.files.length >= 0) {
      this.disabled = false;
      try {
        for (let i = 0; i < event.target.files.length; i++) {
          debugger;
          if(event.target.files[i].size >= 1e+6){
            this.toast.Error({
              msg: '.حجم فایل بیشتر از حد مجاز است',
              title: 'خطا در پیش نمایش عکس'
            });
            return;
          }
          try {
            const element = event.target.files[i];
            let reader = new FileReader();
            reader.onload = (x: any) => {
              this.previews.push({ name: element.name, path: x.target.result, file: element });
            }
            reader.readAsDataURL(element);
          } catch (error) {
            this.toast.Error({
              msg: 'خطا',
              title: 'خطا در پیش نمایش عکس'
            });
          }
        }
      } catch (error) {
        this.toast.Error({
          msg: 'حجم فایل بیشتر از حد مجاز است',
          title: 'خطا در پیش نمایش عکس'
        });
      } finally {
        this.toast.ClearWaiting();
      }
    }
  }

  public removePreview(e: string) {
    this.previews.splice(this.previews.findIndex(x => x.name.includes(e)), 1);
  }

  public saveFile(ev: Event) {
    this.toast.Wait();
    this.disabled = true;
    let folder: string = this.pathUrl == undefined || this.pathUrl.length == 0 ? '/' : this.pathUrl[this.pathUrl.length - 1].replace('//UploadedFiles', '');
    this.previews.forEach(z => {
      this.http.FileManagerExec('filemanager/exec', 'upload', folder, z.file, '2.0').subscribe(x => {
        this.previews = [];
        this.goToFolder.emit(folder);
        this.toast.Success({
          msg: `بارگزاری عکس "${z.file.name}" انجام شد.`,
          title: 'بارگزاری عکس'
        })
      }, err => {
        this.toast.Error({
          msg: `بارگزاری عکس "${z.file.name}" انجام نشد.`,
          title: 'بارگزاری عکس'
        });
      }, () => {
        this.disabled = false;
        this.toast.ClearWaiting();
      })
    })
  }
}
