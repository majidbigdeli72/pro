import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpServiceService } from '../../core/http-service/http-service.service';
import { ListComponent } from '../list/list.component';
import { UploadComponent } from '../upload/upload.component'
import { ToastService } from '../../core/toast/toast.service';

@Component({
  selector: 'basketik-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.less']
})
export class FileComponent implements OnInit {
  constructor(
    private toast: ToastService,
    private http: HttpServiceService) { }

  @ViewChild('spanCopy', { read: ElementRef }) spanCopy: ElementRef;
  @ViewChild('frmCreateFolder', { read: ElementRef }) frmCreateFolder: ElementRef;
  @ViewChild('selectCopy', { read: ElementRef }) selectCopy: ElementRef;
  @ViewChild(ListComponent) listComponent: ListComponent;
  @ViewChild(UploadComponent) uploadComponent: UploadComponent;

  public backToFolderVal: Array<string> = [];
  public showCreateFolder: boolean = false;
  public reloadListFolder: boolean = true;
  public selectedValue: string;
  public NameFolder: string;

  ngOnInit() { }

  public backToFolder() {
    this.listComponent.backToFolder();
  }

  public createFolder() {
    this.toast.Wait();
    let folder: string = this.backToFolderVal == undefined || this.backToFolderVal.length == 0 ? '/' : this.backToFolderVal[this.backToFolderVal.length - 1];

    this.http.FileManagerExec("filemanager/exec", 'addfolder', folder, {}, this.NameFolder).subscribe(x => {
      console.log(x)
      if (x.Data.Errors && x.Data.Errors["0"].Message == 'DIRECTORY_ALREADY_EXISTS') {
        setTimeout(() => this.goToFolder(folder), 10);
        this.toast.Error({
          title: 'خطا در عملیات',
          msg: `پوشه ای با نام "${this.NameFolder}" قبلا ثبت گردیده است`
        });
      }
      else {
        this.listComponent.AddFolder(x.Data);
        setTimeout(() => this.goToFolder(folder), 10);
        this.toast.Success({
          title: 'پوشه جدید',
          msg: `پوشه جدید با نام "${this.NameFolder}" ایجاد گردید`
        })
      }
    }, (err) => { ; }, () => {
      this.toast.ClearWaiting();
      this.showCreateFolder = false;
    });
  }

  public openInputFile() {
    this.uploadComponent.openInput();
  }

  public setPathUrl(path) {
    this.backToFolderVal = path;
  }

  public goToFolder(path: any) {
    this.listComponent.goTOFolder(path);
  }

  public CopyToClipboard() {
    if (this.selectCopy.nativeElement.value) {
      this.selectCopy.nativeElement.select();
      document.execCommand("Copy");
      this.spanCopy.nativeElement.innerText = "کپی شد";
    }
  }

  public setToinput(e) {
    this.spanCopy.nativeElement.innerText = "کپی به حافظه";
    let url = e.replace('/UploadedFiles/', '');
    this.selectedValue = '/UploadedFiles/' + url.trim();
  }
}