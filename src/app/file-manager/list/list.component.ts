import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ViewContainerRef, ComponentFactory, ComponentFactoryResolver, ComponentRef } from '@angular/core';
import { HttpServiceService } from '../../core/http-service/http-service.service';
import { AppConfigService } from '../../core/app-config-setting-bootstrap/app-config-service.service';
import { ContextMenuFolderComponent } from '../context-menu-folder/context-menu-folder.component';
import { ToastService } from '../../core/toast/toast.service';
import { IModel } from '../ifile-manager';
import { ContextMenuFileComponent } from '../context-menu-file/context-menu-file.component';

@Component({
  selector: 'basketik-file-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {
  constructor(
    private toast: ToastService,
    private resolver: ComponentFactoryResolver,
    private http: HttpServiceService,
    private baseUrl: AppConfigService) { }

  @ViewChild('contextmenu', { read: ViewContainerRef }) private contextmenu: ViewContainerRef;

  public list: Array<IModel> = [];
  public showContainer: boolean = false;
  public urlPic: string = this.baseUrl.configuration.PicUrl;
  public selectedFile: any;

  private backUrls: Array<string> = [];
  @Output('backKeyVal') backKeyVal: EventEmitter<any> = new EventEmitter<any>();
  @Output('goTOFolder') _goTOFolder: EventEmitter<any> = new EventEmitter<any>();
  @Output('SelectFile') _selectFile: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit() {
    this.http.FileManagerExec('filemanager/exec','getfolder','', {}).subscribe(x => {
      this.list = x.Data;
      this.showContainer = true;
    });
  }

  public isFolder(i): Boolean {
    return i == 'folder';
  }

  public goTOFolder(e) {
    this.backKeyVal.emit(this.backUrls);
    this.showContainer = false;
    this.http.FileManagerExec('filemanager/exec','getfolder',e, {}).subscribe(x => {
      this.backUrls.push(e);
      this.list = x.Data;
      this.showContainer = true;
    })
  }

  public backToFolder() {
    this.backKeyVal.emit(this.backUrls);
    if (this.backUrls.length > 0) {
      this.showContainer = false;
      this.http.post("filemanager/exec", {
        "mode": "getfolder",
        "path": this.backUrls[this.backUrls.length - 2],
        "thumbnail": false,
      }).subscribe(x => {
        this.backUrls.pop();
        this.list = x.Data.Data;
        this.showContainer = true;
      })
    } else return;
  }

  public contextFolder(e: MouseEvent, idFolder) {
    // e.preventDefault();
    // this.contextmenu.clear();
    // const factory: ComponentFactory<ContextMenuFolderComponent> = this.resolver.resolveComponentFactory(ContextMenuFolderComponent);
    // let componentRef: ComponentRef<ContextMenuFolderComponent> = this.contextmenu.createComponent(factory);
    // componentRef.instance.x = e.pageX ;
    // componentRef.instance.y = e.pageY;
    // componentRef.instance._removeFolder.subscribe(event => this.RemoveFolder(event, idFolder));
  }

  public contextFile(e: MouseEvent, idFile) {
    // e.preventDefault();
    // this.contextmenu.clear();
    // const factory: ComponentFactory<ContextMenuFileComponent> = this.resolver.resolveComponentFactory(ContextMenuFileComponent);
    // let componentRef: ComponentRef<ContextMenuFileComponent> = this.contextmenu.createComponent(factory);

    // componentRef.instance.x = e.pageX ;
    // componentRef.instance.y = e.pageY;
    // componentRef.instance._removeFile.subscribe(event => this.RemoveFile(event, idFile));
  }

  public SelectFile(e) {
    this.selectedFile = e;
    this._selectFile.emit(e.Id);
  }

  //disables the menu
  public disableContextMenu() {
    if (this.contextmenu.length > 0)
      this.contextmenu.clear();
  }

  public RemoveFolder(e, idFolder) {
    this.toast.Wait();
    this.http.FileManagerExec('filemanager/exec', 'delete', idFolder, {}).subscribe(status => {
      this.toast.Success({
        msg: 'عملیات با موفقیت انجام شد',
        title: 'حذف'
      });
      this.list.splice(this.list.findIndex(x => x.Id == idFolder), 1);
    }, errr => { ; }, () => this.toast.ClearWaiting());
    this.disableContextMenu();
  }

  public RemoveFile(e, idFile) {
    this.toast.Wait();
    this.http.FileManagerExec('filemanager/exec', 'delete', idFile, {}).subscribe(status => {
      //this.showContainer = false;
      this.toast.Success({
        msg: 'عملیات با موفقیت انجام شد',
        title: 'حذف'
      });
      this.list.splice(this.list.findIndex(x => x.Id == idFile), 1);
    }, errr => { ; }, () => this.toast.ClearWaiting());
    this.disableContextMenu();
  }

  public AddFolder(e:IModel){
    this.list.push(e);
  }
}