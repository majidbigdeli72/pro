import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'basketik-context-menu-folder',
  templateUrl: './context-menu-folder.component.html',
  styleUrls: ['./context-menu-folder.component.less']
})
export class ContextMenuFolderComponent implements OnInit {

  @Output('RemoveFolder') public _removeFolder: EventEmitter<any> = new EventEmitter<any>();

  @Input() x = 0;
  @Input() y = 0;

  constructor() { }

  ngOnInit() {
  }

  public RemoveFolder(e) {
    this._removeFolder.emit(e)
  }
}
