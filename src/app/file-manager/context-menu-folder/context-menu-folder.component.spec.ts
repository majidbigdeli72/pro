import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextMenuFolderComponent } from './context-menu-folder.component';

describe('ContextMenuFolderComponent', () => {
  let component: ContextMenuFolderComponent;
  let fixture: ComponentFixture<ContextMenuFolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextMenuFolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextMenuFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
