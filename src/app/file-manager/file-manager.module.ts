import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FileManagerRoutingModule } from './file-manager-routing.module';
import { FileComponent } from './file/file.component';
import { ListComponent } from './list/list.component';
import { UploadComponent } from './upload/upload.component';
import { FormsModule } from '@angular/forms';
import { ContextMenuFolderComponent } from './context-menu-folder/context-menu-folder.component';
import { ContextMenuFileComponent } from './context-menu-file/context-menu-file.component';

@NgModule({
  imports: [
    CommonModule,
    FileManagerRoutingModule,
    FormsModule
  ],
  declarations: [
    FileComponent, 
    ListComponent, 
    UploadComponent, 
    ContextMenuFolderComponent, 
    ContextMenuFileComponent
  ],
  entryComponents:[
    ContextMenuFileComponent,
    ContextMenuFolderComponent
  ]
})
export class FileManagerModule { }
