import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'basketik-context-menu-file',
  templateUrl: './context-menu-file.component.html',
  styleUrls: ['./context-menu-file.component.less']
})
export class ContextMenuFileComponent implements OnInit {
  @Output('RemoveFile') public _removeFile: EventEmitter<any> = new EventEmitter<any>();

  @Input() x = 0;
  @Input() y = 0;
  constructor() { }

  ngOnInit() {
  }

  public RemoveFile(e){
    this._removeFile.emit(e);
  }
}
