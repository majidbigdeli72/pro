export class IModel {
    constructor(
        public Id?: string,
        public Type?: typeManager,
        public Attributes?: Attributes) { }
}

export class Attributes {
    constructor(public Name?: string,
        public Path?: string,
        public Readable?: number,
        public Writable?: number,
        public Created?: Date,
        public Modified?: Date,
        public Timestamp?: number,
        public Extension?: string,
        public Size?: number) { }
}

export enum typeManager {
    file,
    folder
}

